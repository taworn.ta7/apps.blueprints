import { createRouter, createWebHistory } from 'vue-router'
import HomePage from '../pages/HomePage.vue'

const router = createRouter({
	history: createWebHistory(import.meta.env.BASE_URL),
	routes: [
		{
			path: '/',
			name: 'home',
			component: HomePage,
		},
		{
			path: '/test-box',
			name: 'test-box',
			component: () => import('../pages/TestBoxesPage.vue'),
		},
		{
			path: '/test-box-shared',
			name: 'test-box-shared',
			component: () => import('../pages/TestBoxesSharedPage.vue'),
		},

		// external sign-in pages
		{
			path: '/authenx/google',
			name: 'signInGoogle',
			component: async () => await import('../pages/authenx/SignInGooglePage.vue'),
		},
		{
			path: '/authenx/line',
			name: 'signInLine',
			component: async () => await import('../pages/authenx/SignInLinePage.vue'),
		},

		// mail shell pages
		{
			path: '/mail_shells/member_signup',
			name: 'member_signup',
			component: async () => await import('../pages/mail_shells/member_signup.vue'),
		},
		{
			path: '/mail_shells/member_reset',
			name: 'member_reset',
			component: async () => await import('../pages/mail_shells/member_reset.vue'),
		},
		{
			path: '/mail_shells/member_change_mail',
			name: 'member_change_mail',
			component: async () => await import('../pages/mail_shells/member_change_mail.vue'),
		}
	]
})

export default router
