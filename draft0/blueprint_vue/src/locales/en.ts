export const messages = {
	"app": "Blueprint",
	"path": {
		"home": "Home",
		"test-box": "Test Boxes",
		"test-box-shared": "Test Boxes Shared",
	},
	"validator": {
		"isSamePasswords": "Password and confirm password must be equal."
	},
	"error": {
		"unknown": "HTTP error, status code={statusCode}!"
	},
}
