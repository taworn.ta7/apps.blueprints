export const messages = {
	"app": "พิมพ์เขียว",
	"path": {
		"home": "หน้าแรก",
		"test-box": "ทดสอบกล่อง",
		"test-box-shared": "ทดสอบกล่องใช้ร่วมกัน",
	},
	"validator": {
		"isSamePasswords": "รหัสผ่าน และ ยืนยันรหัสผ่าน ต้องเหมือนกัน"
	},
	"error": {
		"unknown": "เกิดข้อผิดพลาดจาก HTTP, สถานะ {statusCode}!"
	},
}
