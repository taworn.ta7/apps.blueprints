export const Consts = {

	/**
	 * Local base URL.
	 */
	localUrl: 'http://localhost:8088',
	localIpUrl: 'http://127.0.0.1:8088',

	/**
	 * Server base URL.
	 */
	baseUrl: 'http://localhost:8080/api',

	/**
	 * Static server base URL.
	 */
	baseStaticUrl: 'http://localhost:8080',

	/**
	 * Google client.
	 */
	googleClientId: '563014250625-8gb3cave705l5nss41snu1ruuf823cet.apps.googleusercontent.com',
	googleAuthUrl: 'https://accounts.google.com/o/oauth2/v2/auth',
	googleSignIn: () => {
		const scope = [
			'openid',
			'profile',
			'email',
			'https://www.googleapis.com/auth/userinfo.profile',
			'https://www.googleapis.com/auth/userinfo.email',
		]
		return Consts.googleAuthUrl
			+ `?redirect_uri=${Consts.localUrl}/authenx/google`
			+ `&client_id=${Consts.googleClientId}`
			+ `&access_type=offline&response_type=code&prompt=consent`
			+ `&scope=${scope.join(' ')}`
	},

	/**
	 * LINE client.
	 */
	lineClientId: '2005456140',
	lineAuthUrl: 'https://access.line.me/oauth2/v2.1/authorize',
	lineSignIn: () => {
		return Consts.lineAuthUrl
			+ `?redirect_uri=${Consts.localUrl}/authenx/line`
			+ `&client_id=${Consts.lineClientId}`
			+ `&response_type=code&state=${`C0L@4acf483c311c5f2ef8bf2450c849218d`}`
			+ `&scope=profile%20openid%20email`
	},

}

export default Consts
