import { defineStore } from 'pinia'
import { CaretakerProfile } from '../models/caretaker_profile.entity'
import { AxiosHelper } from '../helpers/axios'
import { LoadPageStateType } from '../helpers/loading'
import Consts from '../Consts'

/**
 * Caretaker singleton store class.
 */
export const useCaretakerStore = defineStore({

	id: 'CaretakerStore',

	state: () => ({

		/**
		 * Client REST connection.
		 */
		client: new AxiosHelper(Consts.baseUrl),

		/**
		 * Profile data.
		 */
		profile: <CaretakerProfile | null>null,

		/**
		 * Sign-in token.
		 */
		token: '',

		/**
		 * Loads page state.
		 */
		loading: LoadPageStateType.Wait,

	}),

	actions: {

		/**
		 * Begins current session.
		 */
		async setup(): Promise<void> {
			const token = localStorage.getItem('token') || ''
			if (!token) {
				this.setProfileState()
				return
			}

			// try to load profile
			console.log(`try to load caretaker profile with sign-in token: ${token}`)
			const result = await this.client.call(`caretaker/profile/me`, {
				method: 'GET',
				headers: this.defaultHeaders(token),
			})
			if (!result.ok) {
				this.setProfileState()
				return
			}

			// success, retrieves REST data
			this.setProfileState(new CaretakerProfile(result.json.profile), token)
			console.log(`caretaker sign-in via token:`, this.profile)
		},

		// ----------------------------------------------------------------------

		/**
		 * Gets default HTTP headers.
		 */
		defaultHeaders(token?: string): any {
			const t = token || this.token
			return {
				'Content-Type': 'application/json;charset=utf-8',
				'Authorization': `Bearer ${t}`,
			}
		},

		/**
		 * Gets multi-part HTTP headers.
		 */
		formDataHeaders(token?: string): any {
			const t = token || this.token
			return {
				'Content-Type': 'multipart/form-data',
				'Authorization': `Bearer ${t}`,
			}
		},

		// ----------------------------------------------------------------------

		isWait(): boolean {
			return this.loading === LoadPageStateType.Wait
		},

		isFinish(): boolean {
			return this.loading === LoadPageStateType.Finish
		},

		isFinishWithData(): boolean {
			return this.loading === LoadPageStateType.FinishWithData
		},

		setProfileState(profile?: CaretakerProfile, token?: string): void {
			this.profile = profile ?? null
			this.token = token ?? ''
			localStorage.setItem('token', this.token)
			if (!this.profile)
				this.loading = LoadPageStateType.Finish
			else
				this.loading = LoadPageStateType.FinishWithData
		},

	},

})
