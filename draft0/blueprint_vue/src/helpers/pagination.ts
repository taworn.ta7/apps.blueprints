/**
 * Pagination class.
 */
export class Pagination {

	/**
	 * Number of rows per one page.
	 */
	public readonly rowsPerPage: number = 0;

	/**
	 * Number of count of all rows.
	 */
	public readonly count: number = 0;

	/**
	 * Number of page, one page has 'rowsPerPage' rows.
	 */
	public readonly page: number = 0;

	/**
	 * Number of rows start, a 'rowsPerPage' * 'page'.
	 */
	public readonly offset: number = 0;

	/**
	 * Number of page, same as 'page'.
	 */
	public readonly pageIndex: number = 0;

	/**
	 * Number of page count, formula: Math.ceil(count / rowsPerPage).
	 */
	public readonly pageCount: number = 0;

	/**
	 * Number of rows start, same as 'offset'.
	 */
	public readonly pageStart: number = 0;

	/**
	 * Number of rows in page.  If not the last page, it will same as 'rowsPerPage', 
	 * otherwise, it will 'count' - 'offset', the remainder.
	 */
	public readonly pageSize: number = 0;

	/**
	 * Number of last rows in page.  It's just 'offset' + 'pageSize'.
	 */
	public readonly pageStop: number = 0;

	/**
	 * Constructs pagination from page, rows per page and count.
	 * 
	 * Rows per page must have value of 1 or more.
	 * Page will start with zero.
	 */
	constructor(rowsPerPage: number, count: number, page: number) {
		this.rowsPerPage = rowsPerPage <= 0 ? 1 : rowsPerPage;
		this.count = count;
		this.pageCount = Math.ceil(this.count / this.rowsPerPage);
		if (this.pageCount <= 0) {
			// no data
			this.page = 0;
			this.offset = 0;
			this.pageIndex = 0;
			this.pageStart = 0;
			this.pageSize = 0;
			this.pageStop = 0;
		}
		else {
			// have data
			this.page = page < 0 ? 0 : (page >= this.pageCount ? this.pageCount - 1 : page);
			this.offset = this.rowsPerPage * this.page;
			this.pageIndex = this.page;
			this.pageStart = this.offset;
			this.pageSize = this.page < this.pageCount - 1
				? this.rowsPerPage
				: (this.page >= this.pageCount ? 0 : this.count - this.offset);
			this.pageStop = this.offset + this.pageSize;
		}
	}

}
