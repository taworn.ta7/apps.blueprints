import axios, { AxiosError } from 'axios'

/**
 * The returns type from AxiosHelper.call().
 */
export type AxiosResult = {
	ok: boolean
	status: number
	json: any
	res: any
}


/**
 * An axios helper for easier used.
 */
export class AxiosHelper {

	/**
	 * Server base URL.
	 */
	baseUrl: string = ''


	/**
	 * Constructor.
	 */
	constructor(baseUrl: string) {
		this.baseUrl = baseUrl
		if (!this.baseUrl.endsWith('/'))
			this.baseUrl += '/'
	}


	/**
	 * Calls HTTP.
	 */
	async call(url: string, options?: any): Promise<AxiosResult> {
		// log before send
		let method
		if (options && options.method)
			method = options.method
		else
			method = 'GET'
		console.log(`${method} ${this.baseUrl + url}`)

		// fetches
		try {
			const res = await axios(this.baseUrl + url, options)
			console.log(`call result: ${res.status} ${res.statusText} %o`, res.data)
			return {
				ok: res.status === 200 || res.status === 201,
				status: res.status,
				json: res.data,
				res,
			}
		}
		catch (ex: any) {
			const err = ex as AxiosError
			if (err) {
				if (err.response) {
					const res = err.response
					const ret = {
						ok: false,
						status: res.status,
						json: res.data,
						res,
					}
					console.log(`call error: ${res.status} ${res.statusText} %o`, res.data)
					return ret
				}
				else {
					console.log(`call error: ${JSON.stringify(err, null, 2)}`)
					return {
						ok: false,
						status: -1,
						json: null,
						res: null,
					}
				}
			}
			console.log(`call error: ${ex}`)
			return {
				ok: false,
				status: -1,
				json: null,
				res: null,
			}
		}
	}

}
