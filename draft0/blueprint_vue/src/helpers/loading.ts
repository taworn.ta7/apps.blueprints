export enum LoadPageStateType {
	Wait = 0,
	Finish = 1,
	FinishWithData = 2,
}
