/**
 * Exception classes packed with error messages and constants to known more reason.
 */
export class Errors {

	/////////////////////////////////////////////////////////////////////////
	// Generic 400 Bad Request
	/////////////////////////////////////////////////////////////////////////

	static readonly uploadFail = 4000101;
	static readonly uploadIsNotType = 4000111;
	static readonly uploadIsNotTypeImage = 4000112;
	static readonly uploadIsTooBig = 4000121;


	/////////////////////////////////////////////////////////////////////////
	// Generic 403 Forbidden
	/////////////////////////////////////////////////////////////////////////

	static readonly dataExists = 4030001;
	static readonly dataInvalid = 4030002;
	static readonly dataExpire = 4030003;
	static readonly mailExists = 4030011;
	static readonly addressExists = 4030012;


	/////////////////////////////////////////////////////////////////////////
	// Generic 404 Not Found
	/////////////////////////////////////////////////////////////////////////

	static readonly dataNotFound = 4040001;
	static readonly mailNotFound = 4040011;
	static readonly addressNotFound = 4040012;


	/////////////////////////////////////////////////////////////////////////
	// Precinct
	/////////////////////////////////////////////////////////////////////////

	static readonly precinctCantonNotFound = 10001;
	static readonly precinctDistrictNotFound = 10002;
	static readonly precinctProvinceNotFound = 10003;
	static readonly precinctSectorNotFound = 10004;
	static readonly precinctZipNotFound = 10005;
	static readonly precinctZipOrCantonInvalid = 10011;


	/////////////////////////////////////////////////////////////////////////
	// Member
	/////////////////////////////////////////////////////////////////////////

	static readonly memberNotFound = 20001;
	static readonly memberMailOrPasswordInvalid = 20011;
	static readonly memberIsDisabledByAdmin = 20012;
	static readonly memberIsResigned = 20013;
	static readonly memberNeedSignIn = 20014;
	static readonly memberSignInTimeout = 20015;
	static readonly memberAddressIsLimited = 20021;


	/////////////////////////////////////////////////////////////////////////
	// Caretaker
	/////////////////////////////////////////////////////////////////////////

	static readonly caretakerNotFound = 30001;
	static readonly caretakerMailOrPasswordInvalid = 30011;
	static readonly caretakerIsDisabledByAdmin = 30012;
	static readonly caretakerIsResigned = 30013;
	static readonly caretakerNeedSignIn = 30014;
	static readonly caretakerSignInTimeout = 30015;
	static readonly caretakerAddressIsLimited = 30021;
	static readonly caretakerRootRequired = 30031;
	static readonly caretakerRootCannotBeDisabled = 30032;

}
