export interface WaitBoxOptions {
	barrierBackground?: string
	edgeBackground?: string
	iconColor?: string
	rightIcon?: boolean
}
