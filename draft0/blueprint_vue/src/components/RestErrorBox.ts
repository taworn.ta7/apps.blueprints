export interface RestErrorBoxOptions {
	content: string
	alternateFocus?: boolean
	barrierDismissible?: boolean
	barrierBackground?: string
	titleBackground?: string
	titleColor?: string
	icon?: string
	iconColor?: string
	rightIcon?: boolean
	reportText?: string | null
	callback?: (result: boolean) => void
}
