import { CaretakerProfile } from './caretaker_profile.entity';


/**
 * Caretaker's credential data.
 */
export class CaretakerCredential {

	id: string | undefined;

	/**
	 * Link to [CaretakerProfile]{@link CaretakerProfile.html}.
	 */
	profile: CaretakerProfile | undefined;

	// ----------------------------------------------------------------------

	// Password

	salt: string | undefined;

	hash: string | undefined;

	/**
	 * Sign-in token.
	 */
	token: string | null | undefined;

	/**
	 * Last sign-in date/time.
	 */
	begin: Date | null | undefined;

	/**
	 * Date/time to update expiry time.
	 */
	refresh: Date | null | undefined;

	/**
	 * Session expiry date/time.
	 */
	expire: Date | null | undefined;

	/**
	 * Last sign-out date/time.
	 */
	end: Date | null | undefined;

	// ----------------------------------------------------------------------

	created: Date | undefined;

	updated: Date | undefined;

	constructor(o?: Partial<CaretakerCredential>) {
		Object.assign(this, o)
	}

	// ----------------------------------------------------------------------

	print() {
		if (!this.profile)
			return `[${this.id}]`;
		else
			return `[owner ${this.profile.id}/${this.profile.mail}, ${this.id}]`;
	}

}
