/**
 * Member's entities constants.
 */
export enum MemberEntityConsts {
	PasswordMin = 4,
	PasswordMax = 20,

	Salt = 255,
	Hash = 1024,
	ConfirmToken = 64,
	Token = 128,

	Mail = 250,

	Abode = 200,
}
