import { MemberProfile } from './member_profile.entity';


/**
 * Member profile extra entity.
 */
export class MemberProfileExtra {

	id: string | undefined;

	/**
	 * Link to [MemberProfile]{@link MemberProfile.html}.
	 */
	profile: MemberProfile | undefined;

	// ----------------------------------------------------------------------

	/**
	 * Icon MIME.
	 */
	mime: string | null | undefined;

	/**
	 * Icon stored.
	 */
	icon: Buffer | null | undefined;

	/**
	 * Settings in JSON form.
	 */
	settings: string | undefined;

	// ----------------------------------------------------------------------

	created: Date | undefined;

	updated: Date | undefined;

	constructor(o?: Partial<MemberProfileExtra>) {
		Object.assign(this, o)
	}

	// ----------------------------------------------------------------------

	print() {
		if (!this.profile)
			return `[${this.id}]`;
		else
			return `[owner ${this.profile.id}/${this.profile.mail}, ${this.id}]`;
	}

}
