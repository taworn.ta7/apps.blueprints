import { MemberProfileExtra } from './member_profile_extra.entity';
import { MemberCredential } from './member_credential.entity';


/**
 * Member profile entity.
 */
export class MemberProfile {

	id: string | undefined;

	// ----------------------------------------------------------------------

	/**
	 * Your personal mail.
	 */
	mail: string | undefined;

	/**
	 * Created date/time for sign-up with our site.
	 */
	begin: Date | undefined;

	/**
	 * Disabled by admin.
	 */
	disabled: Date | null | undefined;

	/**
	 * Resigned by this member.
	 */
	resigned: Date | null | undefined;

	// ----------------------------------------------------------------------

	/**
	 * Google sign-in mail.
	 */
	google_mail: string | undefined;

	/**
	 * Google sign-in timestamp.
	 */
	google_signin: Date | undefined;

	// ----------------------------------------------------------------------

	/**
	 * Line sign-in mail.
	 */
	line_mail: string | undefined;

	/**
	 * Line sign-in timestamp.
	 */
	line_signin: Date | undefined;

	// ----------------------------------------------------------------------

	/**
	 * Link to [MemberProfileExtra]{@link MemberProfileExtra.html}.
	 */
	extra: MemberProfileExtra | undefined;

	/**
	 * Link to [MemberCredential]{@link MemberCredential.html}.
	 */
	credential: MemberCredential | undefined;

	// ----------------------------------------------------------------------

	created: Date | undefined;

	updated: Date | undefined;

	constructor(o?: Partial<MemberProfile>) {
		Object.assign(this, o)
	}

	// ----------------------------------------------------------------------

	print() {
		return `[${this.id + '/' + this.mail}]`;
	}

}
