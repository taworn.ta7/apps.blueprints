import { CaretakerProfile } from './caretaker_profile.entity';


/**
 * Caretaker profile extra entity.
 */
export class CaretakerProfileExtra {

	id: string | undefined;

	/**
	 * Link to [CaretakerProfile]{@link CaretakerProfile.html}.
	 */
	profile: CaretakerProfile | undefined;

	// ----------------------------------------------------------------------

	/**
	 * Icon MIME.
	 */
	mime: string | undefined;

	/**
	 * Icon stored.
	 */
	icon: Buffer | undefined;

	/**
	 * Settings in JSON form.
	 */
	settings: string | undefined;

	// ----------------------------------------------------------------------

	created: Date | undefined;

	updated: Date | undefined;

	constructor(o?: Partial<CaretakerProfileExtra>) {
		Object.assign(this, o)
	}

	// ----------------------------------------------------------------------

	print() {
		if (!this.profile)
			return `[${this.id}]`;
		else
			return `[owner ${this.profile.id}/${this.profile.mail}, ${this.id}]`;
	}

}
