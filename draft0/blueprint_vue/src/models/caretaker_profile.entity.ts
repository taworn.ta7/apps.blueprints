import { CaretakerProfileExtra } from './caretaker_profile_extra.entity';
import { CaretakerCredential } from './caretaker_credential.entity';


/**
 * Caretaker profile entity.
 */
export class CaretakerProfile {

	id: string | undefined;

	// ----------------------------------------------------------------------

	/**
	 * Your personal mail.
	 */
	mail: string | undefined;

	/**
	 * Created date/time for sign-up with our site.
	 */
	begin: Date | undefined;

	/**
	 * Disabled by admin.
	 */
	disabled: Date | null | undefined;

	/**
	 * Resigned by this caretaker.
	 */
	resigned: Date | null | undefined;

	/**
	 * Root privilege in our system.
	 */
	root: boolean = false;

	// ----------------------------------------------------------------------

	/**
	 * Link to [CaretakerProfileExtra]{@link CaretakerProfileExtra.html}.
	 */
	extra: CaretakerProfileExtra | undefined;

	/**
	 * Link to [CaretakerCredential]{@link CaretakerCredential.html}.
	 */
	credential: CaretakerCredential | undefined;

	// ----------------------------------------------------------------------

	created: Date | undefined;

	updated: Date | undefined;

	constructor(o?: Partial<CaretakerProfile>) {
		Object.assign(this, o)
	}

	// ----------------------------------------------------------------------

	print() {
		return `[${this.id + '/' + this.mail}]`;
	}

}
