import {
	Logger,
	Controller,
	Req,
	Body,
	Post,
} from '@nestjs/common';
import { Request } from 'express';
import { SharedService } from '../shared/shared.service';
import { SetupDto } from './setup.dto';
import { SetupService } from './setup.service';


/**
 * This class provides how to setup overall site.
 */
@Controller(`/api/setup`)
export class SetupController {

	private readonly logger = new Logger(SetupController.name);
	private readonly config = this.sharedService.config;

	constructor(
		private readonly sharedService: SharedService,
		private readonly service: SetupService,
	) { }

	// ----------------------------------------------------------------------

	@Post()
	async setup(
		@Req() req: Request,
		@Body('setup') dto: SetupDto,
	): Promise<{
		ok: string,
	}> {
		const firstRun = await this.service.setup(dto);
		if (!firstRun)
			return { ok: 'already setup :)' }

		return { ok: 'setup ok :)' }
	}

}
