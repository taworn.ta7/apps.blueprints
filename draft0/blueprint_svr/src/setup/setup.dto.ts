import {
	IsNotEmpty,
	Matches,
	MinLength,
	MaxLength,
} from 'class-validator';
import { IsEqualTo } from '../decorators/IsEqualTo';
import { regexMail } from '../helpers/string_ex';
import { CaretakerEntityConsts } from '../caretaker/caretaker_entity_consts';


export class SetupDto {
	@IsNotEmpty()
	//@IsEmail()
	@Matches(regexMail)
	@MinLength(5)
	@MaxLength(CaretakerEntityConsts.Mail)
	mail: string;

	@IsNotEmpty()
	@MinLength(CaretakerEntityConsts.PasswordMin)
	@MaxLength(CaretakerEntityConsts.PasswordMax)
	password: string;

	@IsNotEmpty()
	@IsEqualTo('password')
	confirmPassword: string;
}
