import { Module } from '@nestjs/common';
import { SharedService } from '../shared/shared.service';
import { SharedModule } from '../shared/shared.module';
import { CaretakerModule } from '../caretaker/caretaker.module';
import { SetupService } from './setup.service';
import { SetupController } from './setup.controller';


@Module({
	imports: [
		SharedModule,
		CaretakerModule,
	],
	providers: [
		SharedService,
		SetupService,
	],
	controllers: [
		SetupController,
	],
})
export class SetupModule { }
