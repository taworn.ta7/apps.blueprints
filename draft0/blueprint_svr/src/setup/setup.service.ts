import {
	Logger,
	Injectable,
} from '@nestjs/common';
import { InjectDataSource, InjectRepository } from '@nestjs/typeorm';
import { DataSource, Repository } from 'typeorm';
import * as path from 'node:path';
import * as fs from 'node:fs';
import { setPassword } from '../helpers/crypto_ex';
import { SharedService } from '../shared/shared.service';
import { CaretakerProfile, CaretakerPrivilege } from '../caretaker/caretaker_profile.entity';
import { CaretakerProfileExtra } from '../caretaker/caretaker_profile_extra.entity';
import { CaretakerCredential } from '../caretaker/caretaker_credential.entity';
import { SetupDto } from './setup.dto';


/**
 * This class provides how to setup overall site in business layer.
 */
@Injectable()
export class SetupService {

	private readonly logger = new Logger(SetupService.name);
	private readonly config = this.sharedService.config;

	constructor(
		@InjectDataSource()
		private readonly dataSource: DataSource,

		@InjectRepository(CaretakerProfile)
		private readonly profileRepo: Repository<CaretakerProfile>,

		private readonly sharedService: SharedService,
	) { }

	// ----------------------------------------------------------------------

	async setup(dto: SetupDto): Promise<boolean> {
		const alreadySetup = await this.sharedService.get('setup');
		if (alreadySetup)
			return false;

		// to put precinct data, executes queries 
		// don't forget to set multipleStatements=true in TypeOrmModule.forRootAsync, file app.module.ts
		const queries = await fs.promises.readFile(path.join(__dirname, 'precinct.sql'), { encoding: 'utf8' });
		await this.dataSource.query(queries);

		// creates 'root' caretaker
		const pass = setPassword(dto.password);
		const profile = new CaretakerProfile();
		profile.mail = dto.mail;
		profile.privilege = CaretakerPrivilege.Root;
		profile.extra = new CaretakerProfileExtra();
		profile.extra.settings = '{}';
		profile.credential = new CaretakerCredential();
		profile.credential.salt = pass.salt;
		profile.credential.hash = pass.hash;
		profile.credential.token = null;
		await this.profileRepo.save(profile);

		// finished :)
		await this.sharedService.set('setup', '1');
		return true;
	}

}
