import {
	Logger,
	ExceptionFilter,
	Catch,
	ArgumentsHost,
	HttpException,
} from '@nestjs/common';
import { Request, Response } from 'express';


@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {

	private readonly logger = new Logger(HttpExceptionFilter.name);

	catch(exception: HttpException, host: ArgumentsHost) {
		const ctx = host.switchToHttp();
		const req = ctx.getRequest<Request>();
		const res = ctx.getResponse<Response>();
		const status = exception.getStatus();
		const response = exception.getResponse();

		// custom extra code
		const code: number = response && response['code']
			? response['code']
			: null;

		// message
		const message: string = response && response['message']
			? response['message']
			: null;

		// locales, if provided
		const locales: any = response && response['locales']
			? response['locales']
			: null;

		// logging
		const prefix = req.id
			? `${req.id}; `
			: '';
		const suffix = locales && locales.en
			? ` ${locales.en}`
			: (message
				? ` ${message}`
				: '');
		this.logger.warn(`${prefix}HTTP error: ${status}${suffix}`);

		res.status(status).json({
			statusCode: status,
			extraCode: code,
			message,
			locales,
			path: req.url,
			timestamp: new Date().toISOString(),
			requestId: req.id,
			profileId: req.profile ? req.profile.id : null,
			profileMail: req.profile ? req.profile.mail : null,
		});
	}

}
