import { Module } from '@nestjs/common';
import { AxiosService } from '../shared/axios.service';
import { SharedService } from '../shared/shared.service';
import { SharedModule } from '../shared/shared.module';
import { TestSharedController } from './test_shared.controller';


@Module({
	imports: [
		SharedModule,
	],
	providers: [
		AxiosService,
		SharedService,
	],
	controllers: [
		TestSharedController,
	],
})
export class TestSharedModule { }
