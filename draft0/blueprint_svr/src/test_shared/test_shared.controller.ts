import {
	Logger,
	Controller,
	Req,
	Get,
} from '@nestjs/common';
import { Request } from 'express';
import { LoggingAction } from '../shared/logging.entity';
import { SharedService } from '../shared/shared.service';


@Controller(`/api/shared`)
export class TestSharedController {

	private readonly logger = new Logger(TestSharedController.name);
	private readonly config = this.service.config;

	constructor(
		private readonly service: SharedService,
	) { }

	// ----------------------------------------------------------------------

	/**
	 * Prints configuration as simple variables.
	 */
	@Get('config')
	async printConfig(): Promise<any> {
		return {
			config: this.config,
		}
	}

	// ----------------------------------------------------------------------

	@Get()
	async testPrintingLog(
		@Req() req: Request,
	): Promise<{
		note: string,
	}> {
		this.service.print('A simple description.', {
			action: LoggingAction.Read,
			requestId: req.id,
		});

		this.service.print('A next description.', {
			action: LoggingAction.Read,
			requestId: req.id,
		});

		return {
			note: "Please see logging table.",
		}
	}

}
