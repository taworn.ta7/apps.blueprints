import { describe, expect, test } from '@jest/globals';
import * as string_ex from './string_ex';

describe("string, customized for this project", () => {

	test("queryStringToBool()", () => {
		expect(string_ex.queryStringToBool("")).toBe(false);
		expect(string_ex.queryStringToBool(" ")).toBe(false);
		expect(string_ex.queryStringToBool("  ")).toBe(false);
		expect(string_ex.queryStringToBool(" 0 ")).toBe(false);
		expect(string_ex.queryStringToBool("  00  ")).toBe(false);
		expect(string_ex.queryStringToBool("   000   ")).toBe(false);
		expect(string_ex.queryStringToBool("FALSE")).toBe(false);
		expect(string_ex.queryStringToBool(" false ")).toBe(false);
		expect(string_ex.queryStringToBool("FaLsE")).toBe(false);
		expect(string_ex.queryStringToBool(" fAlSe ")).toBe(false);

		expect(string_ex.queryStringToBool("1")).toBe(true);
		expect(string_ex.queryStringToBool(" 2 ")).toBe(true);
		expect(string_ex.queryStringToBool("  3  ")).toBe(true);
		expect(string_ex.queryStringToBool("x")).toBe(true);
		expect(string_ex.queryStringToBool(" xx ")).toBe(true);
		expect(string_ex.queryStringToBool("  xxx  ")).toBe(true);
	});

	test("test regexMail", () => {
		const re = string_ex.regexMail;
		expect(re.test("a@b.co")).toBe(true);
		expect(re.test(" a@b.cc ")).toBe(true);
		expect(re.test("  aaa@bbb.cccc")).toBe(true);
		expect(re.test(" 1@2.34")).toBe(true);

		expect(re.test("a@b")).toBe(false);
		expect(re.test("a@b.c")).toBe(false);
		expect(re.test("aaa@.co")).toBe(false);
		expect(re.test("@bbb.co")).toBe(false);
		expect(re.test("a a@b c")).toBe(false);
		expect(re.test("a a@b.c")).toBe(false);
	});

});
