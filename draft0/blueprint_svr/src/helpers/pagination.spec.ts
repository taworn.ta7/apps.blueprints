import { describe, expect, test } from '@jest/globals';
import { Pagination } from './pagination';
import { queryStringToBool } from './string_ex';

describe("Pagination test", () => {

	test("constructors, 0 to 1", () => {
		let pagi: Pagination;

		// Parameters: rowsPerPage, count and page

		// no data, with rows per page = 0
		pagi = new Pagination(0, 0, 0)
		expect(
			pagi.rowsPerPage === 1 &&
			pagi.count === 0 &&
			pagi.pageCount === 0 &&
			pagi.page === 0
		).toBe(true);

		// no data, simple
		pagi = new Pagination(1, 0, 0);
		expect(
			pagi.rowsPerPage === 1 &&
			pagi.count === 0 &&
			pagi.pageCount === 0 &&
			pagi.page === 0
		).toBe(true);

		// one item
		pagi = new Pagination(1, 1, 0);
		expect(
			pagi.rowsPerPage === 1 &&
			pagi.count === 1 &&
			pagi.pageCount === 1 &&
			pagi.page === 0
		).toBe(true);
	});

	test("constructors, generic cases", () => {
		let pagi: Pagination;

		// Parameters: rowsPerPage, count and page

		// first page
		pagi = new Pagination(5, 10, 0);
		expect(
			pagi.rowsPerPage === 5 &&
			pagi.count === 10 &&
			pagi.pageCount === 2 &&
			pagi.pageIndex === 0 &&
			pagi.pageStart === 0 &&
			pagi.pageSize === 5 &&
			pagi.pageStop === 5 &&
			pagi.page === 0
		).toBe(true);

		// less than first page, revert page to 0
		pagi = new Pagination(5, 10, -1);
		expect(
			pagi.rowsPerPage === 5 &&
			pagi.count === 10 &&
			pagi.pageCount === 2 &&
			pagi.pageIndex === 0 &&
			pagi.pageStart === 0 &&
			pagi.pageSize === 5 &&
			pagi.pageStop === 5 &&
			pagi.page === 0
		).toBe(true);

		// last page
		pagi = new Pagination(5, 10, 1);
		expect(
			pagi.rowsPerPage === 5 &&
			pagi.count === 10 &&
			pagi.pageCount === 2 &&
			pagi.pageIndex === 1 &&
			pagi.pageStart === 5 &&
			pagi.pageSize === 5 &&
			pagi.pageStop === 10 &&
			pagi.page === 1
		).toBe(true);

		// last with remainder, last page
		pagi = new Pagination(5, 12, 2);
		expect(
			pagi.rowsPerPage === 5 &&
			pagi.count === 12 &&
			pagi.pageCount === 3 &&
			pagi.pageIndex === 2 &&
			pagi.pageStart === 10 &&
			pagi.pageSize === 2 &&
			pagi.pageStop === 12 &&
			pagi.page === 2
		).toBe(true);

		// last with remainder, exceed last page, revert to last page
		pagi = new Pagination(5, 12, 1000);
		expect(
			pagi.rowsPerPage === 5 &&
			pagi.count === 12 &&
			pagi.pageCount === 3 &&
			pagi.pageIndex === 2 &&
			pagi.pageStart === 10 &&
			pagi.pageSize === 2 &&
			pagi.pageStop === 12 &&
			pagi.page === 2
		).toBe(true);

		//console.log(`rowsPerPage: ${pagi.rowsPerPage}, count: ${pagi.count}, pageCount: ${pagi.pageCount}, page: ${pagi.page}, pageIndex: ${pagi.pageIndex}, pageStart: ${pagi.pageStart}, pageSize: ${pagi.pageSize}, pageStop: ${pagi.pageStop}`)
	});

});
