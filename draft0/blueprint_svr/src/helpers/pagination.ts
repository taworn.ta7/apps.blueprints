import { Request } from 'express';
import { queryStringToBool } from './string_ex';

/**
 * Pagination class.
 */
export class Pagination {

	/**
	 * Number of rows per one page.
	 */
	public readonly rowsPerPage: number = 0;

	/**
	 * Number of count of all rows.
	 */
	public readonly count: number = 0;

	/**
	 * Number of page, one page has 'rowsPerPage' rows.
	 */
	public readonly page: number = 0;

	/**
	 * Number of rows start, a 'rowsPerPage' * 'page'.
	 */
	public readonly offset: number = 0;

	/**
	 * Number of page, same as 'page'.
	 */
	public readonly pageIndex: number = 0;

	/**
	 * Number of page count, formula: Math.ceil(count / rowsPerPage).
	 */
	public readonly pageCount: number = 0;

	/**
	 * Number of rows start, same as 'offset'.
	 */
	public readonly pageStart: number = 0;

	/**
	 * Number of rows in page.  If not the last page, it will same as 'rowsPerPage', 
	 * otherwise, it will 'count' - 'offset', the remainder.
	 */
	public readonly pageSize: number = 0;

	/**
	 * Number of last rows in page.  It's just 'offset' + 'pageSize'.
	 */
	public readonly pageStop: number = 0;

	/**
	 * Constructs pagination from page, rows per page and count.
	 * 
	 * Rows per page must have value of 1 or more.
	 * Page will start with zero.
	 */
	constructor(rowsPerPage: number, count: number, page: number) {
		this.rowsPerPage = rowsPerPage <= 0 ? 1 : rowsPerPage;
		this.count = count;
		this.pageCount = Math.ceil(this.count / this.rowsPerPage);
		if (this.pageCount <= 0) {
			// no data
			this.page = 0;
			this.offset = 0;
			this.pageIndex = 0;
			this.pageStart = 0;
			this.pageSize = 0;
			this.pageStop = 0;
		}
		else {
			// have data
			this.page = page < 0 ? 0 : (page >= this.pageCount ? this.pageCount - 1 : page);
			this.offset = this.rowsPerPage * this.page;
			this.pageIndex = this.page;
			this.pageStart = this.offset;
			this.pageSize = this.page < this.pageCount - 1
				? this.rowsPerPage
				: (this.page >= this.pageCount ? 0 : this.count - this.offset);
			this.pageStop = this.offset + this.pageSize;
		}
	}

}


/**
 * Finds options from pagination.
 */
export type PaginationFindOptions = {
	/**
	 * Parameter 'page' must be zero to page count - 1.
	 */
	page: number,

	/**
	 * Parameter 'size' must more than zero.  Many rows of one page.
	 */
	size: number,

	/**
	 * Parameter 'search' is search string.  You should see entity code, again.
	 */
	search: string,

	/**
	 * Parameter 'trash' is boolean.  Just flags that record must 'removed' or not.
	 */
	trash: boolean,

	/**
	 * Parameter 'order' is ordering.  Should you have sort dictionary from 'convertOrderingData()' below.
	 */
	order: any,
}


/**
 * This function receives query string key 'order', like 'mail+,created-', and
 * converts into partial SQL structure like { mail: 'ASC', created: 'DESC' }.
 */
function convertOrderingData(sortDict: Object, order: string): Object {
	const result = {};
	if (typeof order === 'string') {
		const parts = order.split(/[,;]+/);
		const regex = /([a-zA-Z0-9_]+)([\+\-]?)/;
		for (let i = 0; i < parts.length; i++) {
			const item = parts[i].trim();
			if (item.length > 0) {
				const matches = regex.exec(item);
				const field = matches[1];
				const reverse = matches[2];
				if (field in sortDict) {
					result[sortDict[field]] = reverse === '-' ? 'DESC' : 'ASC';
				}
			}
		}
	}
	return result;
}


/**
 * Extracts parameters page, size, search, trash and order from Request.
 * 
 * @example
 * const { page, size, search, trash, order } = queryPagination(req, 10, {
 *   // ordering dictionary
 *   mail: 'mail',
 *   name: 'name',
 *   created: 'created',
 *   updated: 'updated',
 * });
 * 
 * @param {number} defaultSize  Default size of page, more than zero.
 */
export function queryPagination(req: Request, defaultSize: number, sortDict: Object): PaginationFindOptions {
	let page = Number(req.query.page);
	if (!page || page < 0)
		page = 0;

	let size = Number(req.query.size);
	if (!size || size <= 0)
		size = defaultSize;

	const search = req.query.search
		? req.query.search.toString().trim()
		: '';

	const trash = queryStringToBool(req.query.trash?.toString());

	const order = sortDict ? convertOrderingData(sortDict, req.query.order?.toString()) : {};

	return {
		page,
		size,
		search,
		trash,
		order,
	};
}
