import * as crypto from 'crypto';

/**
 * Sets password and returns structure salt and hash.
 *
 * @param {string} password  String to generate.
 * 
 * @returns Returns the structure { salt, hash }.
 */
export function setPassword(password: string): { salt: string, hash: string } {
	const salt = crypto.randomBytes(16).toString('hex');
	const hash = crypto.pbkdf2Sync(password, salt, 10000, 512, 'sha512').toString('hex');
	return { salt, hash };
}


/**
 * Validates password with salt and hash.
 *
 * @param {string} password  Password string to compare.
 * @param {string} salt      Combines with hash, it is a value compare with password.
 * @param {string} hash      Combines with salt, it is a value compare with password.
 * 
 * @returns Returns the comparison result.
 */
export function validatePassword(password: string, salt: string, hash: string): boolean {
	const computeHash = crypto.pbkdf2Sync(password, salt, 10000, 512, 'sha512').toString('hex');
	return hash === computeHash;
}


/**
 * Generates token string.
 *
 * @param {number} n  Number of length string return.
 * 
 * @returns Returns the token string.
 */
export function generateToken(n: number): string {
	const seed = crypto.randomBytes(n).toString('hex');
	const code = Buffer.from(seed).toString('base64');
	return code.substring(0, n);
}
