import {
	Logger,
	Injectable,
} from '@nestjs/common';
import { inspect } from 'node:util';
import axios, { AxiosError } from 'axios';


/**
 * The returns type from AxiosHelper.call().
 */
export class AxiosResult {

	/**
	 * This result is success or not.
	 */
	ok: boolean;

	/**
	 * Status HTTP code.
	 */
	status: number;

	/**
	 * Status HTTP code message.
	 */
	statusText: string;

	/**
	 * A return as JSON.
	 */
	json: any;

	/**
	 * A return as full response.
	 */
	res: any;

	// ----------------------------------------------------------------------

	constructor(o?: Partial<AxiosResult>) {
		Object.assign(this, o)
	}

	// ----------------------------------------------------------------------

	print() {
		return `${this.status} ${this.statusText}`;
	}

	fullPrint(depth?: number) {
		if (this.ok)
			return this.print() + ' ' + JSON.stringify(this.json, null, 2);
		else
			return inspect(this, { depth });
	}

};


/**
 * An axios helper for easier used.
 */
@Injectable()
export class AxiosService {

	private readonly logger = new Logger(AxiosService.name);

	constructor(
	) { }

	// ----------------------------------------------------------------------

	/**
	 * Calls HTTP.
	 */
	async call(url: string, options?: any): Promise<AxiosResult> {
		// log before send
		let method: string;
		if (options && options.method)
			method = options.method;
		else
			method = 'GET';
		this.logger.log(`${method} ${url}`);

		// fetches
		try {
			const res = await axios(url, options);
			const ret = new AxiosResult({
				ok: res.status === 200 || res.status === 201,
				status: res.status,
				statusText: res.statusText,
				json: res.data,
				res,
			});
			this.logger.verbose(`call result: ${ret.fullPrint()}`);
			return ret;
		}
		catch (ex: any) {
			const err = ex as AxiosError;
			if (err) {
				if (err.response) {
					const res = err.response;
					const ret = new AxiosResult({
						ok: false,
						status: res.status,
						statusText: res.statusText,
						json: res.data,
						res,
					});
					this.logger.warn(`call response error: ${ret.print()}`);
					return ret;
				}
				else {
					const ret = new AxiosResult({
						ok: false,
						status: -1,
						statusText: '',
						json: null,
						res: null,
					});
					this.logger.warn(`call error: ${ret.print()}`);
					return ret;
				}
			}
			const ret = new AxiosResult({
				ok: false,
				status: -1,
				statusText: ex,
				json: null,
				res: null,
			});
			this.logger.warn(`call error: ${ret.print()}`);
			return ret;
		}
	}

}
