import {
	Entity,
	Column,
	PrimaryGeneratedColumn,
	CreateDateColumn,
	UpdateDateColumn,
} from 'typeorm';
import { inspect } from 'node:util';


/**
 * System settings.
 */
@Entity({
	name: 'settings',
})
export class Settings {

	@PrimaryGeneratedColumn({
		type: 'bigint',
	})
	id: string;

	// ----------------------------------------------------------------------

	/**
	 * Key.
	 */
	@Column({
		length: 50,
	})
	key: string;

	/**
	 * Value as string.
	 */
	@Column({
		length: 200,
		nullable: true,
	})
	value: string | null;

	// ----------------------------------------------------------------------

	@CreateDateColumn({})
	created: Date;

	@UpdateDateColumn({})
	updated: Date;

	constructor(o?: Partial<Settings>) {
		Object.assign(this, o)
	}

	// ----------------------------------------------------------------------

	print() {
		return `[${this.id}]`;
	}

	fullPrint(depth?: number) {
		return inspect(this, { depth });
	}

}
