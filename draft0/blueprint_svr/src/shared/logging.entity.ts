import {
	Entity,
	Column,
	PrimaryGeneratedColumn,
	CreateDateColumn,
	UpdateDateColumn,
} from 'typeorm';
import { inspect } from 'node:util';


/**
 * CRUD: create, read, update or delete.
 */
export enum LoggingAction {
	Create = 'create',
	Read = 'read',
	Update = 'update',
	Delete = 'delete',
}


/**
 * Generic logging.
 */
@Entity({
	name: 'logging',
})
export class Logging {

	@PrimaryGeneratedColumn({
		type: 'bigint',
	})
	id: string;

	// ----------------------------------------------------------------------

	/**
	 * Description text.
	 */
	@Column({
		type: 'text',
	})
	description: string;

	/**
	 * CRUD: create, read, update or delete.
	 */
	@Column({
		type: 'enum',
		enum: LoggingAction,
	})
	action: LoggingAction;

	/**
	 * Which table(s)?
	 */
	@Column({
		length: 200,
	})
	table: string;

	/** 
	 * Request id, can get from Request object.
	 */
	@Column({
		type: 'int',
		nullable: true,
	})
	requestId: string;

	/** 
	 * Who do this action?
	 */
	@Column({
		type: 'bigint',
		nullable: true,
	})
	profileId: string;

	// ----------------------------------------------------------------------

	@CreateDateColumn({})
	created: Date;

	@UpdateDateColumn({})
	updated: Date;

	constructor(o?: Partial<Logging>) {
		Object.assign(this, o)
	}

	// ----------------------------------------------------------------------

	print() {
		return `[${this.id}]`;
	}

	fullPrint(depth?: number) {
		return inspect(this, { depth });
	}

}
