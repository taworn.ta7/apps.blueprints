import {
	Logger,
	Injectable,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Logging, LoggingAction } from './logging.entity';
import { Settings } from './settings.entity';


const env = process.env;

type SharedPrintOptions = {
	action: LoggingAction,
	table?: string | null,
	requestId?: string | null,
	profileId?: string | null,
}


/**
 * Portal to entrance to be more shared services.
 */
@Injectable()
export class SharedService {

	private readonly logger = new Logger(SharedService.name);

	constructor(
		@InjectRepository(Logging)
		private readonly loggingRepo: Repository<Logging>,

		@InjectRepository(Settings)
		private readonly settingsRepo: Repository<Settings>,
	) { }

	// ----------------------------------------------------------------------

	public readonly config = {
		client: {
			host: env.CLIENT_HOST,
		},

		google: {
			// Google
			clientId: env.GOOGLE_CLIENT_ID,
			clientSecret: env.GOOGLE_CLIENT_SECRET,
			redirectUrl: env.GOOGLE_REDIRECT_URL,
		},

		line: {
			// LINE
			clientId: env.LINE_CLIENT_ID,
			clientSecret: env.LINE_CLIENT_SECRET,
			redirectUrl: env.LINE_REDIRECT_URL,
		},

		member: {
			// member config
			iconFileLimit: +env.MEMBER_ICON_FILE_LIMIT,
			addressLimit: +env.MEMBER_ADDRESS_LIMIT,

			// member change property must have expire in days
			// must keep this for DAYS_TO_KEEP_*, too
			signUpExpireAsDays: +env.MEMBER_SIGNUP_EXPIRE,
			signUpExpire: +env.MEMBER_SIGNUP_EXPIRE * 24 * 60 * 60 * 1000,
			resetExpireAsDays: +env.MEMBER_RESET_EXPIRE,
			resetExpire: +env.MEMBER_RESET_EXPIRE * 24 * 60 * 60 * 1000,
			changeMailExpireAsDays: +env.MEMBER_CHANGE_MAIL_EXPIRE,
			changeMailExpire: +env.MEMBER_CHANGE_MAIL_EXPIRE * 24 * 60 * 60 * 1000,

			// member authen, timeout and refresh unit are days
			authenSecret: env.MEMBER_AUTHEN_SECRET,
			authenTimeOutAsDays: +env.MEMBER_AUTHEN_TIMEOUT,
			authenTimeOut: +env.MEMBER_AUTHEN_TIMEOUT * 24 * 60 * 60 * 1000,
			authenRefreshAsDays: +env.MEMBER_AUTHEN_REFRESH,
			authenRefresh: +env.MEMBER_AUTHEN_REFRESH * 24 * 60 * 60 * 1000,
		},

		caretaker: {
			// caretaker config
			iconFileLimit: +env.CARETAKER_ICON_FILE_LIMIT,
			addressLimit: +env.CARETAKER_ADDRESS_LIMIT,

			// caretaker change property must have expire in days
			// must keep this for DAYS_TO_KEEP_*, too
			resetExpireAsDays: +env.CARETAKER_RESET_EXPIRE,
			resetExpire: +env.CARETAKER_RESET_EXPIRE * 24 * 60 * 60 * 1000,
			changeMailExpireAsDays: +env.CARETAKER_CHANGE_MAIL_EXPIRE,
			changeMailExpire: +env.CARETAKER_CHANGE_MAIL_EXPIRE * 24 * 60 * 60 * 1000,

			// caretaker authen, timeout and refresh unit are days
			authenSecret: env.CARETAKER_AUTHEN_SECRET,
			authenTimeOutAsDays: +env.CARETAKER_AUTHEN_TIMEOUT,
			authenTimeOut: +env.CARETAKER_AUTHEN_TIMEOUT * 24 * 60 * 60 * 1000,
			authenRefreshAsDays: +env.CARETAKER_AUTHEN_REFRESH,
			authenRefresh: +env.CARETAKER_AUTHEN_REFRESH * 24 * 60 * 60 * 1000,
		},
	}

	// ----------------------------------------------------------------------

	/**
	 * Prints output to log.
	 */
	async print(description: string, options: SharedPrintOptions): Promise<void> {
		await this.loggingRepo.insert({
			description,
			action: options.action,
			table: options.table ?? '',
			requestId: options.requestId,
			profileId: options.profileId,
		});
	}

	// ----------------------------------------------------------------------

	/**
	 * Gets value from key in settings table.
	 */
	async get(key: string, defaultValue?: string): Promise<string | null> {
		const row = await this.settingsRepo.findOne({ where: { key } });
		if (!row)
			return defaultValue;
		else
			return row.value;
	}


	/**
	 * Sets value to key in settings table.
	 */
	async set(key: string, value: string | null): Promise<void> {
		let row = await this.settingsRepo.findOne({ where: { key } });
		if (!row) {
			row = new Settings();
			row.key = key;
		}
		row.value = value;
		await this.settingsRepo.save(row);
	}

}
