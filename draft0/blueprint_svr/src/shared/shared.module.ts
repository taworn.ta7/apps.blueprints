import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Logging } from './logging.entity';
import { Settings } from './settings.entity';
import { AxiosService } from './axios.service';
import { SharedService } from './shared.service';


@Module({
	imports: [
		TypeOrmModule.forFeature([
			Logging,
			Settings,
		]),
	],
	exports: [
		TypeOrmModule,
	],
	providers: [
		AxiosService,
		SharedService,
	],
	controllers: [
	],
})
export class SharedModule { }
