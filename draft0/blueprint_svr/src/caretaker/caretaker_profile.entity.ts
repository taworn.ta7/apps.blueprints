import {
	Entity,
	Column,
	PrimaryGeneratedColumn,
	CreateDateColumn,
	UpdateDateColumn,
	OneToOne,
	Index,
} from 'typeorm';
import { inspect } from 'node:util';
import { CaretakerEntityConsts } from './caretaker_entity_consts';
import { CaretakerProfileExtra } from './caretaker_profile_extra.entity';
import { CaretakerCredential } from './caretaker_credential.entity';


/**
 * Privilege system.
 * Less number, more level.
 */
export enum CaretakerPrivilege {
	Root = -1,
	Administrator = 0,
	Supervisor = 1,
}


/**
 * Caretaker profile entity.
 */
@Entity({
	name: 'caretaker_profile',
})
export class CaretakerProfile {

	@PrimaryGeneratedColumn({
		type: 'bigint',
	})
	id: string;

	// ----------------------------------------------------------------------

	/**
	 * Your personal mail.
	 */
	@Index({
		unique: true,
	})
	@Column({
		length: CaretakerEntityConsts.Mail,
	})
	mail: string;

	/**
	 * Created date/time for sign-up with our site.
	 */
	@Column({
		default: () => 'CURRENT_TIMESTAMP',
	})
	begin: Date;

	/**
	 * Disabled by admin.
	 */
	@Column({
		nullable: true,
	})
	disabled: Date | null;

	/**
	 * Resigned by this caretaker.
	 */
	@Column({
		nullable: true,
	})
	resigned: Date | null;

	/**
	 * Privilege in our system.
	 */
	@Column({
		type: 'enum',
		enum: CaretakerPrivilege,
	})
	privilege: CaretakerPrivilege;

	// ----------------------------------------------------------------------

	/**
	 * Link to [CaretakerProfileExtra]{@link CaretakerProfileExtra.html}.
	 */
	@OneToOne(() => CaretakerProfileExtra, (o) => o.profile, {
		cascade: true,
		onDelete: 'CASCADE',
		orphanedRowAction: 'delete',
	})
	extra: CaretakerProfileExtra;

	/**
	 * Link to [CaretakerCredential]{@link CaretakerCredential.html}.
	 */
	@OneToOne(() => CaretakerCredential, (o) => o.profile, {
		cascade: true,
		onDelete: 'CASCADE',
		orphanedRowAction: 'delete',
	})
	credential: CaretakerCredential;

	// ----------------------------------------------------------------------

	@CreateDateColumn({})
	created: Date;

	@UpdateDateColumn({})
	updated: Date;

	constructor(o?: Partial<CaretakerProfile>) {
		Object.assign(this, o)
	}

	// ----------------------------------------------------------------------

	print() {
		return `[${this.id + '/' + this.mail}]`;
	}

	fullPrint(depth?: number) {
		return inspect(this, { depth });
	}

}
