import {
	Entity,
	Column,
	PrimaryGeneratedColumn,
	CreateDateColumn,
	UpdateDateColumn,
	OneToOne,
	JoinColumn,
} from 'typeorm';
import { inspect } from 'node:util';
import { CaretakerProfile } from './caretaker_profile.entity';


/**
 * Caretaker profile extra entity.
 */
@Entity({
	name: 'caretaker_profile_extra',
})
export class CaretakerProfileExtra {

	@PrimaryGeneratedColumn({
		type: 'bigint',
	})
	id: string;

	/**
	 * Link to [CaretakerProfile]{@link CaretakerProfile.html}.
	 */
	@OneToOne(() => CaretakerProfile, (o) => o.extra)
	@JoinColumn({
		name: 'profile_id',
	})
	profile: CaretakerProfile;

	// ----------------------------------------------------------------------

	/**
	 * Icon MIME.
	 */
	@Column({
		length: 50,
		nullable: true,
	})
	mime: string | null;

	/**
	 * Icon stored.
	 */
	@Column({
		type: 'mediumblob',
		nullable: true,
	})
	icon: Buffer | null;

	/**
	 * Settings in JSON form.
	 */
	@Column({
		type: 'mediumtext',
	})
	settings: string;

	// ----------------------------------------------------------------------

	@CreateDateColumn({})
	created: Date;

	@UpdateDateColumn({})
	updated: Date;

	constructor(o?: Partial<CaretakerProfileExtra>) {
		Object.assign(this, o)
	}

	// ----------------------------------------------------------------------

	print() {
		if (!this.profile)
			return `[${this.id}]`;
		else
			return `[owner ${this.profile.id}/${this.profile.mail}, ${this.id}]`;
	}

	fullPrint(depth?: number) {
		return inspect(this, { depth });
	}

}
