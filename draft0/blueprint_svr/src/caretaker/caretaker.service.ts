import {
	Logger,
	Injectable,
} from '@nestjs/common';
import { InjectDataSource, InjectRepository } from '@nestjs/typeorm';
import { DataSource, Repository } from 'typeorm';
import { Request } from 'express';
import * as jwt from 'jsonwebtoken';
import { Pagination, PaginationFindOptions } from '../helpers/pagination';
import { Errors } from '../helpers/errors';
import { SharedService } from '../shared/shared.service';
import { CaretakerProfile } from './caretaker_profile.entity';
import { CaretakerCredential } from './caretaker_credential.entity';


/**
 * This class provides generic functions in business layer.
 */
@Injectable()
export class CaretakerService {

	private readonly logger = new Logger(CaretakerService.name);
	private readonly config = this.sharedService.config;

	constructor(
		@InjectDataSource()
		private readonly dataSource: DataSource,

		@InjectRepository(CaretakerProfile)
		private readonly profileRepo: Repository<CaretakerProfile>,

		@InjectRepository(CaretakerCredential)
		private readonly credentialRepo: Repository<CaretakerCredential>,

		private readonly sharedService: SharedService,
	) { }

	// ----------------------------------------------------------------------

	/**
	 * Finds caretaker profile by Id.
	 *
	 * @returns The 'CaretakerProfile' if founds, otherwise, exception 'E404CaretakerNotFound' raises.
	 */
	async findById(id: string, more?: boolean): Promise<CaretakerProfile> {
		const profile = await this.profileRepo.findOne({
			where: { id },
			relations: more ? ['extra'] : [],
		});
		if (!profile)
			throw Errors.E404CaretakerNotFound();
		return profile;
	}


	/**
	 * Finds caretaker profile by mail.
	 *
	 * @returns The 'CaretakerProfile' if founds, otherwise, exception 'E404CaretakerNotFound' raises.
	 */
	async findByMail(mail: string, more?: boolean): Promise<CaretakerProfile> {
		const profile = await this.profileRepo.findOne({
			where: { mail },
			relations: more ? ['extra'] : [],
		});
		if (!profile)
			throw Errors.E404CaretakerNotFound();
		return profile;
	}


	/**
	 * Finds caretakers with conditions, ordering and trashing.
	 *
	 * @returns The array of 'CaretakerProfile' if founds, otherwise, is empty array.
	 */
	async find(req: Request, options?: PaginationFindOptions): Promise<{
		profiles: CaretakerProfile[],
		pagination: Pagination,
	}> {
		// validates options
		const o = options ?? {
			page: 0,
			size: 1,
			search: '',
			trash: false,
			order: {},
		}

		// conditions
		let query = "1=1";
		if (o.search && o.search.length > 0)
			query += " AND (profile.mail LIKE :search)";
		if (!o.trash)
			query += " AND (profile.disabled IS NULL AND profile.resigned IS NULL)";
		else
			query += " AND (profile.disabled IS NOT NULL OR profile.resigned IS NOT NULL)";
		this.logger.debug(`${req.id}; query: ${query}`);

		// counts for conditions
		const count = await this.dataSource.createQueryBuilder()
			.select('profile')
			.from(CaretakerProfile, 'profile')
			.where(query, { search: `%${o.search}%` })
			.getCount();

		// builds pagination
		const pagination = new Pagination(o.size, count, o.page);

		// finds for conditions
		const rows = await this.dataSource.createQueryBuilder()
			.select('profile')
			.from(CaretakerProfile, 'profile')
			.where(query, { search: `%${o.search}%` })
			.orderBy(o.order)
			.skip(pagination.pageStart)
			.take(pagination.pageSize)
			.getMany();

		// reduces and returns
		return {
			profiles: rows,
			pagination,
		};
	}

	// ----------------------------------------------------------------------

	/**
	 * Validates this profile can sign-in or not.
	 *
	 * @returns Always true.  Otherwise, it's errors, raise exception.
	 */
	checkProfileCanSignIn(profile: CaretakerProfile): boolean {
		// checks if this caretaker disabled or resigned
		if (profile.disabled)
			throw Errors.E403CaretakerIsDisabledByAdmin();
		if (profile.resigned)
			throw Errors.E403CaretakerIsResigned();
		return true;
	}


	/**
	 * Profile to sign-in.
	 * 
	 * @returns Always true.
	 */
	async profileSignIn(profile: CaretakerProfile): Promise<boolean> {
		// signs token
		const now = new Date();
		const expire = now.getTime() + this.config.caretaker.authenTimeOut;
		const payload = {
			id: profile.id,
			iat: Math.floor(Date.now() / 1000) - 30,
		};
		const token = await jwt.sign(payload, this.config.caretaker.authenSecret, {});

		// updates credential
		profile.credential.begin = now;
		profile.credential.refresh = now;
		profile.credential.expire = new Date(expire);
		profile.credential.end = null;
		profile.credential.token = token;
		await this.credentialRepo.save(profile.credential);

		return true;
	}

}
