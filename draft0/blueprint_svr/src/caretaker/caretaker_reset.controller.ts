import {
	Logger,
	Controller,
	Req,
	Body,
	Get,
	Post,
	Query,
} from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';
import { Request } from 'express';
import { LoggingAction } from '../shared/logging.entity';
import { SharedService } from '../shared/shared.service';
import { CaretakerProfile } from './caretaker_profile.entity';
import { CaretakerChangeProperty } from './caretaker_change_property.entity';
import { CaretakerResetDto } from './caretaker_reset.dto';
import { CaretakerResetService } from './caretaker_reset.service';


/**
 * This class provides how to reset password.
 * 
 * The process is:
 * * receives request reset form
 * * get mail which has a link to URL
 * * user click link, which will reset password and send mail new password, too.
 */
@Controller(`/api/caretaker/reset`)
export class CaretakerResetController {

	private readonly logger = new Logger(CaretakerResetController.name);
	private readonly config = this.sharedService.config;

	constructor(
		private readonly mailerService: MailerService,
		private readonly sharedService: SharedService,
		private readonly service: CaretakerResetService,
	) { }

	// ----------------------------------------------------------------------

	@Post()
	async reset(
		@Req() req: Request,
		@Body('reset') dto: CaretakerResetDto,
	): Promise<{
		reset: CaretakerChangeProperty,
		url: string,
	}> {
		// creates reset record
		const reset = await this.service.reset(req, dto);

		// creates URL
		const url = `${req.protocol}://${req.get('host')}/api/caretaker/reset/confirm?code=${reset.confirmToken}`;

		// sends mail
		await this.mailerService.sendMail({
			to: reset.mail,
			subject: `Reset Password Confirmation`,
			template: 'caretaker_reset',
			context: {
				mail: reset.mail,
				token: reset.confirmToken,
				url,
			},
		});

		return { reset, url }
	}


	@Get('confirm')
	async confirm(
		@Req() req: Request,
		@Query('code') code: string,
	): Promise<{
		profile: CaretakerProfile,
	}> {
		// reset caretaker password
		const { profile, password } = await this.service.confirm(req, code);

		// sends mail
		await this.mailerService.sendMail({
			to: profile.mail,
			subject: "Reset Password Success",
			template: 'caretaker_reset_new_password',
			context: {
				mail: profile.mail,
				password,
			}
		});

		// adds log
		await this.sharedService.print(
			`caretaker ${profile.print()} password is reset!`, {
			action: LoggingAction.Update,
			table: 'caretaker_credential',
			requestId: req.id,
			profileId: profile.id,
		});

		return { profile };
	}

}
