import {
	IsNotEmpty,
	Matches,
	MinLength,
	MaxLength,
} from 'class-validator';
import { regexMail } from '../helpers/string_ex';
import { CaretakerEntityConsts } from './caretaker_entity_consts';


export class CaretakerResetDto {
	@IsNotEmpty()
	//@IsEmail()
	@Matches(regexMail)
	@MinLength(5)
	@MaxLength(CaretakerEntityConsts.Mail)
	mail: string;
}
