import {
	Logger,
	Injectable,
} from '@nestjs/common';
import { InjectDataSource, InjectRepository } from '@nestjs/typeorm';
import { DataSource, Repository } from 'typeorm';
import { Request } from 'express';
import { Errors } from '../helpers/errors';
import { SharedService } from '../shared/shared.service';
import { PrecinctService } from '../precinct/precinct.service';
import { CaretakerProfile } from './caretaker_profile.entity';
import { CaretakerAddress } from './caretaker_address.entity';
import { CaretakerAddressDto } from './caretaker_address.dto';


/**
 * This class provides loads and saves caretaker address(es) in business layer.
 */
@Injectable()
export class CaretakerAddressService {

	private readonly logger = new Logger(CaretakerAddressService.name);
	private readonly config = this.sharedService.config;

	constructor(
		@InjectDataSource()
		private readonly dataSource: DataSource,

		@InjectRepository(CaretakerAddress)
		private readonly addressRepo: Repository<CaretakerAddress>,

		private readonly sharedService: SharedService,
		private readonly precinctService: PrecinctService,
	) { }

	// ----------------------------------------------------------------------

	/**
	 * Finds one by id.
	 * 
	 * @returns If founds, return 'CaretakerAddress' record.  Otherwise, exception 'E404AddressNotFound' raises.
	 */
	async findById(id: string): Promise<CaretakerAddress> {
		const row = await this.dataSource.createQueryBuilder()
			.select('addr')
			.from(CaretakerAddress, 'addr')
			.leftJoinAndSelect('addr.canton', 'canton')
			.leftJoinAndSelect('addr.district', 'district')
			.leftJoinAndSelect('addr.province', 'province')
			.leftJoinAndSelect('addr.profile', 'profile')
			.where('addr.id = :id', { id })
			.getOne();
		if (!row)
			throw Errors.E404AddressNotFound();
		return row;
	}


	/**
	 * Finds all of caretaker's addresses.
	 */
	async findAllOfCaretaker(profile: CaretakerProfile): Promise<CaretakerAddress[]> {
		return await this.dataSource.createQueryBuilder()
			.select('addr')
			.from(CaretakerAddress, 'addr')
			.leftJoinAndSelect('addr.canton', 'canton')
			.leftJoinAndSelect('addr.district', 'district')
			.leftJoinAndSelect('addr.province', 'province')
			.leftJoinAndSelect('addr.profile', 'profile')
			.where('addr.profile.id = :profileId', { profileId: profile.id })
			.getMany();
	}


	/**
	 * Gets count of caretaker address(es).
	 */
	async countInCaretaker(profile: CaretakerProfile): Promise<number> {
		return await this.dataSource.createQueryBuilder()
			.select('addr')
			.from(CaretakerAddress, 'addr')
			.leftJoinAndSelect('addr.canton', 'canton')
			.leftJoinAndSelect('addr.district', 'district')
			.leftJoinAndSelect('addr.province', 'province')
			.leftJoinAndSelect('addr.profile', 'profile')
			.where('addr.profile.id = :profileId', { profileId: profile.id })
			.getCount();
	}

	// ----------------------------------------------------------------------

	/**
	 * Saves new address.
	 */
	async insert(
		req: Request,
		profile: CaretakerProfile,
		dto: CaretakerAddressDto,
	): Promise<CaretakerAddress> {
		// checks ZIP, canton, district and province
		const zip = await this.precinctService.verifyZip(dto.zip, dto.canton, dto.district, dto.province);
		const canton = await this.precinctService.findByCantonId(dto.canton, false);
		const district = await this.precinctService.findByDistrictId(dto.district, false);
		const province = await this.precinctService.findByProvinceId(dto.province, false);

		// adds address
		const addr = this.addressRepo.create();
		addr.profile = profile;
		addr.abode = dto.abode;
		addr.canton = canton;
		addr.district = district;
		addr.province = province;
		addr.zip = dto.zip;
		this.addressRepo.save(addr);
		this.logger.log(`${req.id}; caretaker ${profile.print()} add new address: ${addr.morePrint()}`);
		return addr;
	}


	/**
	 * Replaces existing address.
	 */
	async replace(
		req: Request,
		profile: CaretakerProfile,
		addr: CaretakerAddress,
		dto: CaretakerAddressDto,
	): Promise<CaretakerAddress> {
		// checks ZIP, canton, district and province
		const zip = await this.precinctService.verifyZip(dto.zip, dto.canton, dto.district, dto.province);
		const canton = await this.precinctService.findByCantonId(dto.canton, false);
		const district = await this.precinctService.findByDistrictId(dto.district, false);
		const province = await this.precinctService.findByProvinceId(dto.province, false);

		// checks DTO id must exists in database and same profile
		const row = await this.dataSource.createQueryBuilder()
			.select('addr')
			.from(CaretakerAddress, 'addr')
			.leftJoinAndSelect('addr.canton', 'canton')
			.leftJoinAndSelect('addr.district', 'district')
			.leftJoinAndSelect('addr.province', 'province')
			.leftJoinAndSelect('addr.profile', 'profile')
			.where('addr.profile.id = :profileId', { profileId: profile.id })
			.andWhere('addr.id = :id', { id: addr.id })
			.getOne();
		if (!row)
			throw Errors.E404AddressNotFound();

		// edit address
		row.id = addr.id;
		row.profile = profile;
		row.abode = dto.abode;
		row.canton = canton;
		row.district = district;
		row.province = province;
		row.zip = dto.zip;
		this.addressRepo.save(row);
		this.logger.log(`${req.id}; caretaker ${profile.print()} replace address: ${row.morePrint()}`);
		return row;
	}

	/**
	 * Deletes existing address.
	 */
	async delete(
		req: Request,
		profile: CaretakerProfile,
		addr: CaretakerAddress,
	): Promise<void> {
		const row = await this.dataSource.createQueryBuilder()
			.select('addr')
			.from(CaretakerAddress, 'addr')
			.leftJoinAndSelect('addr.canton', 'canton')
			.leftJoinAndSelect('addr.district', 'district')
			.leftJoinAndSelect('addr.province', 'province')
			.leftJoinAndSelect('addr.profile', 'profile')
			.where('addr.profile.id = :profileId', { profileId: profile.id })
			.andWhere('addr.id = :id', { id: addr.id })
			.getOne();
		if (!row)
			throw Errors.E404AddressNotFound();
		this.addressRepo.delete(row.id);
		this.logger.log(`${req.id}; caretaker ${profile.print()} delete address: ${row.morePrint()}`);
	}

}
