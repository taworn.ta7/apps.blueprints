import {
	Logger,
	Injectable,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Request } from 'express';
import { validatePassword } from '../helpers/crypto_ex';
import { Errors } from '../helpers/errors';
import { SharedService } from '../shared/shared.service';
import { CaretakerProfile } from './caretaker_profile.entity';
import { CaretakerCredential } from './caretaker_credential.entity';
import { CaretakerAuthenSignInDto } from './caretaker_authen.dto';
import { CaretakerService } from './caretaker.service';


/**
 * This class provides how to sign-in and retrives sign-in token in business layer.
 */
@Injectable()
export class CaretakerAuthenService {

	private readonly logger = new Logger(CaretakerAuthenService.name);
	private readonly config = this.sharedService.config;

	constructor(
		@InjectRepository(CaretakerProfile)
		private readonly profileRepo: Repository<CaretakerProfile>,

		@InjectRepository(CaretakerCredential)
		private readonly credentialRepo: Repository<CaretakerCredential>,

		private readonly sharedService: SharedService,
		private readonly caretakerService: CaretakerService,
	) { }

	// ----------------------------------------------------------------------

	/**
	 * Authorizes the mail and password.
	 * Returns caretaker data and sign-in token and will be use all the session.
	 */
	async signIn(req: Request, dto: CaretakerAuthenSignInDto): Promise<CaretakerProfile> {
		// selects profile
		const profile = await this.profileRepo.findOne({
			where: { mail: dto.mail },
			relations: ['credential', 'extra'],
		});
		if (!profile || !validatePassword(dto.password, profile.credential.salt, profile.credential.hash))
			throw Errors.E401CaretakerMailOrPasswordInvalid();
		this.caretakerService.checkProfileCanSignIn(profile);

		// profile sign-in
		await this.caretakerService.profileSignIn(profile);

		// success
		this.logger.log(`${req.id}; caretaker sign-in: ${profile.print()}`);
		return profile;
	}


	/**
	 * Signs off from the current session.  The sign-in token will be invalid.
	 */
	async signOut(req: Request, profileId: string): Promise<CaretakerProfile> {
		// selects profile
		const profile = await this.profileRepo.findOne({
			where: { id: profileId },
			relations: ['credential'],
		});

		// updates credential
		const now = new Date();
		profile.credential.end = now;
		profile.credential.token = null;
		await this.credentialRepo.save(profile.credential);

		// success
		this.logger.log(`${req.id}; caretaker sign-out: ${profile.print()}`);
		return profile;
	}

	// ----------------------------------------------------------------------

	/**
	 * Extracts bearer from authorization.
	 * 
	 * @param {string} authorization  Authorization string from Request.
	 * @returns Returns the bearer in authorization, or null if no bearer.
	 */
	tokenFromHeaders(authorization: string): string | null {
		if (authorization) {
			const a = authorization.split(' ');
			if (a.length >= 2) {
				if (a[0].toLowerCase() === 'bearer') {
					return a[1];
				}
			}
		}
		return null;
	}


	/**
	 * Creates a 'CaretakerProfile' from authorization header string.
	 * 
	 * @param {string} bearer  The bearer string, extracts from tokenFromHeaders().
	 * @returns Returns the 'CaretakerProfile' record.
	 */
	async caretakerFromHeaders(bearer: string): Promise<CaretakerProfile> {
		// searches bearer in 'CaretakerCredential'
		const credential = await this.credentialRepo.findOne({
			where: { token: bearer },
			relations: ['profile'],
		});
		if (!credential || !credential.profile)
			throw Errors.E401CaretakerNeedSignIn();
		this.caretakerService.checkProfileCanSignIn(credential.profile);

		// checks if session is expired
		const now = new Date();
		if (credential.expire.getTime() < now.getTime())
			throw Errors.E401CaretakerSignInTimeout();

		// checks to replenish time
		if (credential.refresh.getTime() + this.config.caretaker.authenRefresh < now.getTime()) {
			const expire = now.getTime() + this.config.caretaker.authenTimeOut;
			credential.refresh = now;
			credential.expire = new Date(expire);
			await this.credentialRepo.save(credential);
			this.logger.debug(`caretaker ${credential.print()} has been replenish session time!`);
		}

		return credential.profile;
	}

}
