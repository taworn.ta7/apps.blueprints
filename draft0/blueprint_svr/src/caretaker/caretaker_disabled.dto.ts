import {
	IsNotEmpty,
	IsBoolean,
	Matches,
	MinLength,
	MaxLength,
} from 'class-validator';
import { regexMail } from '../helpers/string_ex';
import { CaretakerEntityConsts } from './caretaker_entity_consts';


export class CaretakerDisabledDto {
	@IsNotEmpty()
	//@IsEmail()
	@Matches(regexMail)
	@MinLength(5)
	@MaxLength(CaretakerEntityConsts.Mail)
	mail: string;

	@IsNotEmpty()
	@IsBoolean()
	disabled: boolean;
}
