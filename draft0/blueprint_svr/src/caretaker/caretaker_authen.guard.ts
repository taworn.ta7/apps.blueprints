import {
	Logger,
	Injectable,
	CanActivate,
	ExecutionContext,
} from '@nestjs/common';
import { Request } from 'express';
import { Errors } from '../helpers/errors';
import { SharedService } from '../shared/shared.service';
import { CaretakerPrivilege } from './caretaker_profile.entity';
import { CaretakerAuthenService } from './caretaker_authen.service';


/**
 * Authentication root guard.
 * After this middleware, Request.profile is guarantee to be always not null and have 'root' privilege.
 */
@Injectable()
export class CaretakerAuthenRootGuard implements CanActivate {

	private readonly logger = new Logger(CaretakerAuthenGuard.name);
	private readonly config = this.sharedService.config;

	constructor(
		private readonly sharedService: SharedService,
		private readonly service: CaretakerAuthenService,
	) { }

	async canActivate(context: ExecutionContext): Promise<boolean> {
		const req = context.switchToHttp().getRequest<Request>();
		const authorization = req.headers.authorization;
		const bearer = this.service.tokenFromHeaders(authorization);
		if (!bearer)
			throw Errors.E404DataNotFound(/* because: no bearer */);
		const profile = await this.service.caretakerFromHeaders(bearer);
		if (profile.privilege !== CaretakerPrivilege.Root)
			throw Errors.E403CaretakerRootRequired();
		req.profile = profile;
		return true;
	}

}


/**
 * Authentication guard.
 * After this middleware, Request.profile is guarantee to be always not null.
 */
@Injectable()
export class CaretakerAuthenGuard implements CanActivate {

	private readonly logger = new Logger(CaretakerAuthenGuard.name);

	constructor(
		private readonly service: CaretakerAuthenService,
	) { }

	async canActivate(context: ExecutionContext): Promise<boolean> {
		const req = context.switchToHttp().getRequest<Request>();
		const authorization = req.headers.authorization;
		const bearer = this.service.tokenFromHeaders(authorization);
		if (!bearer)
			throw Errors.E404DataNotFound(/* because: no bearer */);
		req.profile = await this.service.caretakerFromHeaders(bearer);
		return true;
	}

}
