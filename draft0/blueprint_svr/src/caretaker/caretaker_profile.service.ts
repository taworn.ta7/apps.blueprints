import {
	Logger,
	Injectable,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Request } from 'express';
import { setPassword, generateToken } from '../helpers/crypto_ex';
import { SharedService } from '../shared/shared.service';
import { CaretakerProfile } from './caretaker_profile.entity';
import { CaretakerProfileExtra } from './caretaker_profile_extra.entity';
import { CaretakerCredential } from './caretaker_credential.entity';
import { CaretakerChangeProperty, CaretakerChangeAction } from './caretaker_change_property.entity';
import { CaretakerChangeMailDto, CaretakerChangePasswordDto, CaretakerResignDto } from './caretaker_profile.dto';
import { CaretakerSettingsDto } from './caretaker_settings.dto';
import { CaretakerService } from './caretaker.service';
import { CaretakerStockIconsService } from './caretaker_stock_icons.service';
import { CaretakerChangePropertyService } from './caretaker_change_property.service';


/**
 * This class provides profile editing in business layer.
 */
@Injectable()
export class CaretakerProfileService {

	private readonly logger = new Logger(CaretakerProfileService.name);
	private readonly config = this.sharedService.config;

	constructor(
		@InjectRepository(CaretakerProfile)
		private readonly profileRepo: Repository<CaretakerProfile>,

		@InjectRepository(CaretakerProfileExtra)
		private readonly extraRepo: Repository<CaretakerProfileExtra>,

		@InjectRepository(CaretakerCredential)
		private readonly credentialRepo: Repository<CaretakerCredential>,

		@InjectRepository(CaretakerChangeProperty)
		private readonly propertyRepo: Repository<CaretakerChangeProperty>,

		private readonly sharedService: SharedService,
		private readonly iconService: CaretakerStockIconsService,
		private readonly caretakerService: CaretakerService,
		private readonly propertyService: CaretakerChangePropertyService,
	) { }

	// ----------------------------------------------------------------------

	/////////////////////////////////////////////////////////////////////////
	// Retrives Full Profile
	/////////////////////////////////////////////////////////////////////////

	async profile(req: Request): Promise<CaretakerProfile> {
		const profile = await this.caretakerService.findById(req.profile?.id, true);
		this.caretakerService.checkProfileCanSignIn(profile);
		this.logger.log(`${req.id}; caretaker sign-in token: ${profile.print()}`);
		return profile;
	}


	/////////////////////////////////////////////////////////////////////////
	// Change Mail
	/////////////////////////////////////////////////////////////////////////

	/**
	 * Saves change mail form into database.
	 * 
	 * @param {CaretakerChangeProperty} dto  DTO contains change mail form.
	 * @returns Returns new 'CaretakerChangeProperty' record.
	 */
	async requestChangeMail(req: Request, mail: string, dto: CaretakerChangeMailDto): Promise<CaretakerChangeProperty> {
		// checks this mail must be available to use
		await this.propertyService.checkMailAvailable(dto.newMail);

		// checks mail along with password both must be correct
		const profile = await this.propertyService.checkPasswordAndLoadProfile(req, mail, dto.password);

		// creates change mail
		const prop = new CaretakerChangeProperty();
		prop.action = CaretakerChangeAction.ChangeMail;
		prop.mail = profile.mail;
		prop.newMail = dto.newMail;
		prop.done = new Date(Date.now() + this.config.caretaker.changeMailExpire);
		prop.confirmToken = generateToken(64);
		await this.propertyRepo.save(prop);
		this.logger.log(`${req.id}; caretaker ${profile.print()} request to change mail: ${prop.fullPrint()}`);

		return prop;
	}


	/**
	 * Confirms the change mail form.  This will set profile mail to the new one.
	 * 
	 * @param {string} code  Code given from the confirmation mail.
	 * @returns The 'CaretakerProfile' which updated new mail.
	 */
	async confirmChangeMail(req: Request, code: string): Promise<CaretakerProfile> {
		// checks code
		const prop = await this.propertyService.findCode(code, CaretakerChangeAction.ChangeMail);

		// again, checks this mail must be available to use
		await this.propertyService.checkMailAvailable(prop.newMail);

		// updates mail in profile
		const profile = await this.propertyService.loadProfile(req, prop.mail);
		profile.mail = prop.newMail;
		profile.credential.token = null;
		await this.profileRepo.save(profile);
		this.logger.log(`${req.id}; caretaker ${profile.print()} is updated mail!`);

		// disables this record, so it cannot used again
		prop.done = new Date();
		await this.propertyRepo.save(prop);

		return profile;
	}


	/////////////////////////////////////////////////////////////////////////
	// Change Password
	/////////////////////////////////////////////////////////////////////////

	/**
	 * This function updates password.
	 * 
	 * @returns The 'CaretakerProfile' which updated new password.
	 */
	async changePassword(req: Request, mail: string, dto: CaretakerChangePasswordDto): Promise<CaretakerProfile> {
		// updates password
		const profile = await this.propertyService.checkPasswordAndLoadProfile(req, mail, dto.currentPassword);
		const pass = setPassword(dto.newPassword);
		profile.credential.salt = pass.salt;
		profile.credential.hash = pass.hash;
		profile.credential.token = null;
		await this.credentialRepo.save(profile.credential);
		this.logger.log(`${req.id}; caretaker ${profile.print()} is updated password!`);

		return profile;
	}


	/////////////////////////////////////////////////////////////////////////
	// Change Settings
	/////////////////////////////////////////////////////////////////////////

	/**
	 * This function saves settings, in JSON, into database.
	 * The DTO is in 'caretaker_settings.dto'.
	 * 
	 * @returns The 'CaretakerProfile' which updated settings.
	 */
	async changeSettings(req: Request, id: string, dto: CaretakerSettingsDto): Promise<CaretakerProfile> {
		// loads profile with extra
		const profile = await this.caretakerService.findById(id, true);

		// saves and commit
		profile.extra.settings = JSON.stringify(dto);
		await this.extraRepo.save(profile.extra);
		this.logger.log(`${req.id}; caretaker ${profile.print()} is updated settings: ${profile.extra.settings}`);

		return profile;
	}


	/////////////////////////////////////////////////////////////////////////
	// Profile Icons
	/////////////////////////////////////////////////////////////////////////

	/**
	 * This function saves icon with MIME into database.
	 * 
	 * @returns The 'CaretakerProfile' which updated icon.
	 */
	async changeIcon(req: Request, id: string, icon: Express.Multer.File | null): Promise<CaretakerProfile> {
		// loads profile with extra
		const profile = await this.caretakerService.findById(id, true);

		// saves and commit
		const extra = profile.extra;
		if (icon) {
			extra.mime = icon.mimetype;
			extra.icon = icon.buffer;
			this.logger.verbose(`${req.id}; caretaker ${profile.print()} is uploaded icon`);
		}
		else {
			extra.mime = null;
			extra.icon = null;
			this.logger.verbose(`${req.id}; caretaker ${profile.print()} is set to default icon`);
		}
		await this.extraRepo.save(extra);

		return profile;
	}


	/**
	 * This function returns profile icon, or default icon if they not set.
	 * 
	 * @returns Returns icon with MIME type.
	 */
	async getIcon(req: Request, id: string): Promise<{ mime: string, icon: Buffer }> {
		// loads profile with extra
		const profile = await this.caretakerService.findById(id, true);

		// retrives from profile, if it uploaded
		if (profile.extra.icon)
			return { mime: profile.extra.mime, icon: profile.extra.icon };

		// retrives from profile icon stock
		return await this.iconService.get(profile.mail);
	}


	/////////////////////////////////////////////////////////////////////////
	// Resignation
	/////////////////////////////////////////////////////////////////////////

	/**
	 * This function resign current caretaker.
	 * 
	 * @returns The 'CaretakerProfile' which flag 'resigned' is set.
	 */
	async resign(req: Request, mail: string, dto: CaretakerResignDto): Promise<CaretakerProfile> {
		// updates resigned flag
		const profile = await this.propertyService.checkPasswordAndLoadProfile(req, mail, dto.currentPassword);
		profile.resigned = new Date();
		profile.credential.token = null;
		await this.profileRepo.save(profile);
		this.logger.log(`${req.id}; caretaker ${profile.print()} is resigned!`);

		return profile;
	}

}
