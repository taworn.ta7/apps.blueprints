import {
	IsNotEmpty,
	Matches,
	MinLength,
	MaxLength,
	Min,
	Max,
} from 'class-validator';
import { regexMail } from '../helpers/string_ex';
import { CaretakerEntityConsts } from './caretaker_entity_consts';
import { CaretakerPrivilege } from './caretaker_profile.entity';


export class CaretakerAssignDto {
	@IsNotEmpty()
	//@IsEmail()
	@Matches(regexMail)
	@MinLength(5)
	@MaxLength(CaretakerEntityConsts.Mail)
	mail: string;

	@IsNotEmpty()
	@Min(CaretakerPrivilege.Administrator)
	@Max(CaretakerPrivilege.Supervisor)
	privilege: CaretakerPrivilege;
}
