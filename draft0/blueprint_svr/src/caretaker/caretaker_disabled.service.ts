import {
	Logger,
	Injectable,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Request } from 'express';
import { Errors } from '../helpers/errors';
import { SharedService } from '../shared/shared.service';
import { MemberProfile } from '../member/member_profile.entity';
import { CaretakerProfile, CaretakerPrivilege } from './caretaker_profile.entity';
import { CaretakerDisabledDto } from './caretaker_disabled.dto';
import { MemberService } from '../member/member.service';
import { CaretakerService } from './caretaker.service';


/**
 * This class provides disabled caretaker feature in business layer.
 */
@Injectable()
export class CaretakerDisabledService {

	private readonly logger = new Logger(CaretakerDisabledService.name);
	private readonly config = this.sharedService.config;

	constructor(
		@InjectRepository(MemberProfile)
		private readonly memberProfileRepo: Repository<MemberProfile>,

		@InjectRepository(CaretakerProfile)
		private readonly caretakerProfileRepo: Repository<CaretakerProfile>,

		private readonly sharedService: SharedService,
		private readonly memberService: MemberService,
		private readonly caretakerService: CaretakerService,
	) { }

	// ----------------------------------------------------------------------

	async disabledMember(req: Request, dto: CaretakerDisabledDto): Promise<MemberProfile> {
		// disabled/enabled
		const profile = await this.memberService.findByMail(dto.mail);
		profile.disabled = dto.disabled ? (new Date()) : null;
		await this.memberProfileRepo.save(profile);
		this.logger.log(`${req.id}; caretaker ${(req.profile as CaretakerProfile).print()} set member disabled=${dto.disabled}, target profile: ${profile.fullPrint()}`);
		return profile;
	}

	async disabledCaretaker(req: Request, dto: CaretakerDisabledDto): Promise<CaretakerProfile> {
		// disabled/enabled
		const profile = await this.caretakerService.findByMail(dto.mail);
		if ((req.profile as CaretakerProfile).privilege >= profile.privilege)
			throw Errors.E403CaretakerNeedMorePriviledge();
		profile.disabled = dto.disabled ? (new Date()) : null;
		await this.caretakerProfileRepo.save(profile);
		this.logger.log(`${req.id}; caretaker ${(req.profile as CaretakerProfile).print()} set caretaker disabled=${dto.disabled}, target profile: ${profile.fullPrint()}`);
		return profile;
	}

}
