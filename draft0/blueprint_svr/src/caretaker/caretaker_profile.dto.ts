import {
	IsNotEmpty,
	Matches,
	MinLength,
	MaxLength,
} from 'class-validator';
import { regexMail } from '../helpers/string_ex';
import { CaretakerEntityConsts } from './caretaker_entity_consts';


export class CaretakerChangeMailDto {
	@IsNotEmpty()
	//@IsEmail()
	@Matches(regexMail)
	@MinLength(5)
	@MaxLength(CaretakerEntityConsts.Mail)
	newMail: string;

	@IsNotEmpty()
	@MinLength(CaretakerEntityConsts.PasswordMin)
	@MaxLength(CaretakerEntityConsts.PasswordMax)
	password: string;
}


export class CaretakerChangePasswordDto {
	@IsNotEmpty()
	@MinLength(CaretakerEntityConsts.PasswordMin)
	@MaxLength(CaretakerEntityConsts.PasswordMax)
	currentPassword: string;

	@IsNotEmpty()
	@MinLength(CaretakerEntityConsts.PasswordMin)
	@MaxLength(CaretakerEntityConsts.PasswordMax)
	newPassword: string;
}


export class CaretakerResignDto {
	@IsNotEmpty()
	@MinLength(CaretakerEntityConsts.PasswordMin)
	@MaxLength(CaretakerEntityConsts.PasswordMax)
	currentPassword: string;
}
