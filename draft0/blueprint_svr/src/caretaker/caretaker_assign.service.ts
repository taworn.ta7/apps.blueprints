import {
	Logger,
	Injectable,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Request } from 'express';
import { setPassword, generateToken } from '../helpers/crypto_ex';
import { SharedService } from '../shared/shared.service';
import { CaretakerProfile } from './caretaker_profile.entity';
import { CaretakerProfileExtra } from './caretaker_profile_extra.entity';
import { CaretakerCredential } from './caretaker_credential.entity';
import { CaretakerAssignDto } from './caretaker_assign.dto';
import { CaretakerChangePropertyService } from './caretaker_change_property.service';


/**
 * This class assigns a new caretaker in business layer.
 */
@Injectable()
export class CaretakerAssignService {

	private readonly logger = new Logger(CaretakerAssignService.name);
	private readonly config = this.sharedService.config;

	constructor(
		@InjectRepository(CaretakerProfile)
		private readonly profileRepo: Repository<CaretakerProfile>,

		private readonly sharedService: SharedService,
		private readonly propertyService: CaretakerChangePropertyService,
	) { }

	// ----------------------------------------------------------------------

	/**
	 * Creates a new caretaker into database.
	 * 
	 * @param {CaretakerChangeProperty} dto  DTO contains assign form.
	 * @returns Returns new 'CaretakerProfile' created.
	 */
	async assign(req: Request, dto: CaretakerAssignDto): Promise<{
		profile: CaretakerProfile,
		password: string,
	}> {
		// checks this mail must be available to use
		await this.propertyService.checkMailAvailable(dto.mail);

		// creates caretaker profile
		const generate = generateToken(8);
		const pass = setPassword(generate);
		const profile = new CaretakerProfile();
		profile.mail = dto.mail;
		profile.privilege = dto.privilege;
		profile.extra = new CaretakerProfileExtra();
		profile.extra.settings = '{}';
		profile.credential = new CaretakerCredential();
		profile.credential.salt = pass.salt;
		profile.credential.hash = pass.hash;
		profile.credential.token = null;
		await this.profileRepo.save(profile);
		this.logger.log(`${req.id}; caretaker created: ${profile.fullPrint()}`);

		return { profile, password: generate };
	}

}
