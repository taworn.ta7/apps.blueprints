import {
	Logger,
	Controller,
	Req,
	Param,
	Body,
	Get,
	Put,
	Post,
	Delete,
	UseGuards,
} from '@nestjs/common';
import { Request } from 'express';
import { Errors } from '../helpers/errors';
import { LoggingAction } from '../shared/logging.entity';
import { SharedService } from '../shared/shared.service';
import { CaretakerProfile } from './caretaker_profile.entity';
import { CaretakerAddress } from './caretaker_address.entity';
import { CaretakerAddressDto } from './caretaker_address.dto';
import { CaretakerAddressService } from './caretaker_address.service';
import { CaretakerAuthenGuard } from './caretaker_authen.guard';


@Controller(`/api/caretaker/address`)
@UseGuards(CaretakerAuthenGuard)
export class CaretakerAddressController {

	private readonly logger = new Logger(CaretakerAddressController.name);
	private readonly config = this.sharedService.config;

	constructor(
		private readonly sharedService: SharedService,
		private readonly service: CaretakerAddressService,
	) { }

	// ----------------------------------------------------------------------

	@Get()
	async view(
		@Req() req: Request,
	): Promise<{
		addresses: CaretakerAddress[],
	}> {
		const profile = req.profile as CaretakerProfile;
		return {
			addresses: await this.service.findAllOfCaretaker(profile),
		};
	}


	@Post()
	async save(
		@Req() req: Request,
		@Body('address') dto: CaretakerAddressDto,
	): Promise<{
		address: CaretakerAddress,
	}> {
		// retrives profile from request
		const profile = req.profile as CaretakerProfile;
		this.logger.verbose(`${req.id}; caretaker address DTO: ${JSON.stringify(dto, null, 2)}`);

		// checks address limit
		const count = await this.service.countInCaretaker(profile);
		if (count >= this.config.member.addressLimit)
			throw Errors.E403CaretakerAddressIsLimited(this.config.member.addressLimit);

		// saves
		const address = await this.service.insert(req, profile, dto);

		// adds log
		await this.sharedService.print(
			`caretaker ${profile.print()} add new address: ${address.fullPrint()}`, {
			action: LoggingAction.Create,
			table: 'caretaker_address',
			requestId: req.id,
			profileId: profile.id,
		});

		return { address };
	}


	@Put(':id')
	async replace(
		@Req() req: Request,
		@Param('id') id: string,
		@Body('address') dto: CaretakerAddressDto,
	): Promise<{
		address: CaretakerAddress,
	}> {
		// retrives profile from request
		const profile = req.profile as CaretakerProfile;
		this.logger.verbose(`${req.id}; replace ${id} with new address DTO: ${JSON.stringify(dto, null, 2)}`);

		// saves
		const current = await this.service.findById(id);
		const address = await this.service.replace(req, profile, current, dto);

		// adds log
		await this.sharedService.print(
			`caretaker ${profile.print()} replace address: ${address.fullPrint()}`, {
			action: LoggingAction.Update,
			table: 'caretaker_address',
			requestId: req.id,
			profileId: profile.id,
		});

		return { address };
	}


	@Delete(':id')
	async delete(
		@Req() req: Request,
		@Param('id') id: string,
	): Promise<{
	}> {
		// retrives profile from request
		const profile = req.profile as CaretakerProfile;
		this.logger.verbose(`${req.id}; delete address with id ${id}`);

		// deletes
		const current = await this.service.findById(id);
		await this.service.delete(req, profile, current);

		// adds log
		await this.sharedService.print(
			`caretaker ${profile.print()} delete address: ${current.fullPrint()}`, {
			action: LoggingAction.Delete,
			table: 'caretaker_address',
			requestId: req.id,
			profileId: profile.id,
		});

		return {};
	}

}
