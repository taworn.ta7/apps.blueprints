import {
	IsNotEmpty,
	MinLength,
	MaxLength,
} from 'class-validator';
import { CaretakerEntityConsts } from './caretaker_entity_consts';


export class CaretakerAuthenSignInDto {
	@IsNotEmpty()
	//@IsEmail()
	@MinLength(5)
	@MaxLength(CaretakerEntityConsts.Mail)
	mail: string;

	@IsNotEmpty()
	@MinLength(CaretakerEntityConsts.PasswordMin)
	@MaxLength(CaretakerEntityConsts.PasswordMax)
	password: string;
}
