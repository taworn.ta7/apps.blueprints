import {
	Logger,
	Injectable,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Request } from 'express';
import { validatePassword } from '../helpers/crypto_ex';
import { Errors } from '../helpers/errors';
import { SharedService } from '../shared/shared.service';
import { CaretakerProfile } from './caretaker_profile.entity';
import { CaretakerChangeProperty, CaretakerChangeAction } from './caretaker_change_property.entity';
import { CaretakerService } from './caretaker.service';


/**
 * This class provides how to do change properties in business layer.
 */
@Injectable()
export class CaretakerChangePropertyService {

	private readonly logger = new Logger(CaretakerChangeProperty.name);
	private readonly config = this.sharedService.config;

	constructor(
		@InjectRepository(CaretakerProfile)
		private readonly profileRepo: Repository<CaretakerProfile>,

		@InjectRepository(CaretakerChangeProperty)
		private readonly propertyRepo: Repository<CaretakerChangeProperty>,

		private readonly sharedService: SharedService,
		private readonly caretakerService: CaretakerService,
	) { }

	// ----------------------------------------------------------------------

	/**
	 * Gets record by code.
	 * 
	 * @param {string} code  Code given from the confirmation mail.
	 * @returns The 'CaretakerChangeProperty' record.
	 */
	async findCode(code: string, action: CaretakerChangeAction): Promise<CaretakerChangeProperty> {
		// checks code
		if (!code || code.trim() === '')
			throw Errors.E404DataNotFound();

		// checks entity
		const property = await this.propertyRepo.findOne({ where: { confirmToken: code } });
		if (!property)
			throw Errors.E404DataNotFound();

		// checks action
		if (property.action !== action)
			throw Errors.E403DataInvalid();

		// checks expiry
		if (property.done && property.done.getTime() < Date.now())
			throw Errors.E403DataExpire();

		return property;
	}


	/**
	 * Checks mail address must NOT be exists.  So it can be used.
	 * 
	 * @returns The result will always true.
	 */
	async checkMailAvailable(mail: string): Promise<boolean> {
		// this mail must NOT be in 'CaretakerProfile', yet
		const count = await this.profileRepo.count({ where: { mail } });
		if (count > 0)
			throw Errors.E403MailExists();
		return true;
	}


	/**
	 * Checks this mail must already in 'CaretakerProfile'.
	 * 
	 * @returns The 'CaretakerProfile' with 'CaretakerCredential' attached.
	 */
	async loadProfile(req: Request, mail: string): Promise<CaretakerProfile> {
		const profile = await this.profileRepo.findOne({
			where: { mail },
			relations: ['credential'],
		});
		if (!profile)
			throw Errors.E404CaretakerNotFound();
		this.caretakerService.checkProfileCanSignIn(profile);
		return profile;
	}


	/**
	 * Checks password must be valid.
	 * 
	 * @returns The 'CaretakerProfile' with 'CaretakerCredential' attached.
	 */
	async checkPasswordAndLoadProfile(req: Request, mail: string, password: string): Promise<CaretakerProfile> {
		const profile = await this.profileRepo.findOne({
			where: { mail },
			relations: ['credential'],
		});
		if (!profile)
			throw Errors.E404CaretakerNotFound();
		if (!validatePassword(password, profile.credential.salt, profile.credential.hash))
			throw Errors.E401CaretakerMailOrPasswordInvalid();
		this.caretakerService.checkProfileCanSignIn(profile);
		return profile;
	}

}
