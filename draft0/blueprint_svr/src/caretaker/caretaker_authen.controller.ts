import {
	Logger,
	Controller,
	Req,
	Body,
	Get,
	Put,
	UseGuards,
} from '@nestjs/common';
import { Request } from 'express';
import { LoggingAction } from '../shared/logging.entity';
import { SharedService } from '../shared/shared.service';
import { CaretakerProfile } from './caretaker_profile.entity';
import { CaretakerAuthenSignInDto } from './caretaker_authen.dto';
import { CaretakerAuthenService } from './caretaker_authen.service';
import { CaretakerAuthenGuard } from './caretaker_authen.guard';


/**
 * This class provides authentication with password.
 */
@Controller(`/api/caretaker/authen`)
export class CaretakerAuthenController {

	private readonly logger = new Logger(CaretakerAuthenController.name);
	private readonly config = this.sharedService.config;

	constructor(
		private readonly sharedService: SharedService,
		private readonly service: CaretakerAuthenService,
	) { }

	// ----------------------------------------------------------------------

	/**
	 * Sign-in with mail and password.
	 * 
	 * @returns If mail and password corrected, it will returns caretaker profile with sign-in token.
	 */
	@Put('signin')
	async signIn(
		@Req() req: Request,
		@Body('signIn') dto: CaretakerAuthenSignInDto,
	): Promise<{
		profile: CaretakerProfile,
		token: string,
	}> {
		// sign-in
		const profile = await this.service.signIn(req, dto);

		// adds log
		await this.sharedService.print(
			`caretaker sign-in: ${profile.fullPrint()}`, {
			action: LoggingAction.Update,
			table: 'caretaker_credential',
			requestId: req.id,
			profileId: profile.id,
		});

		const credential = profile.credential;
		delete profile.credential;
		return {
			profile,
			token: credential.token,
		};
	}


	@Put('signout')
	@UseGuards(CaretakerAuthenGuard)
	async signOut(
		@Req() req: Request,
	): Promise<void> {
		// sign-out
		const profile = await this.service.signOut(req, req.profile!.id);

		// adds log
		await this.sharedService.print(
			`caretaker sign-out: ${profile.fullPrint()}`, {
			action: LoggingAction.Update,
			table: 'caretaker_credential',
			requestId: req.id,
			profileId: profile.id,
		});
	}


	@Get('check')
	@UseGuards(CaretakerAuthenGuard)
	async check(
		@Req() req: Request,
	): Promise<{
		profile: CaretakerProfile,
	}> {
		return {
			profile: req.profile as CaretakerProfile,
		};
	}

}
