import {
	Logger,
	Controller,
	Req,
	Res,
	Body,
	Get,
	Put,
	Post,
	Delete,
	Query,
	UploadedFile,
	UseGuards,
	UseInterceptors,
	ValidationPipe,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { MailerService } from '@nestjs-modules/mailer';
import { Request, Response } from 'express';
import { inspect } from 'util';
import { convertBytes } from '../helpers/convert';
import { Errors } from '../helpers/errors';
import { LoggingAction } from '../shared/logging.entity';
import { SharedService } from '../shared/shared.service';
import { CaretakerProfile } from './caretaker_profile.entity';
import { CaretakerChangeProperty } from './caretaker_change_property.entity';
import { CaretakerChangeMailDto, CaretakerChangePasswordDto, CaretakerResignDto } from './caretaker_profile.dto';
import { CaretakerSettingsDto } from './caretaker_settings.dto';
import { CaretakerProfileService } from './caretaker_profile.service';
import { CaretakerAuthenGuard } from './caretaker_authen.guard';


/**
 * This class provides profile editing in application layer.
 */
@Controller(`/api/caretaker/profile`)
export class CaretakerProfileController {

	private readonly logger = new Logger(CaretakerProfileController.name);
	private readonly config = this.sharedService.config;

	constructor(
		private readonly mailerService: MailerService,
		private readonly sharedService: SharedService,
		private readonly service: CaretakerProfileService,
	) { }

	// ----------------------------------------------------------------------

	/////////////////////////////////////////////////////////////////////////
	// Retrives Full Profile
	/////////////////////////////////////////////////////////////////////////

	@Get('me')
	@UseGuards(CaretakerAuthenGuard)
	async profile(
		@Req() req: Request,
	): Promise<{
		profile: CaretakerProfile,
	}> {
		return {
			profile: await this.service.profile(req),
		}
	}


	/////////////////////////////////////////////////////////////////////////
	// Change Mail
	/////////////////////////////////////////////////////////////////////////

	@Post('mail')
	@UseGuards(CaretakerAuthenGuard)
	async requestChangeMail(
		@Req() req: Request,
		@Body('mail') dto: CaretakerChangeMailDto,
	): Promise<{
		change: CaretakerChangeProperty,
		url: string,
	}> {
		// creates change mail record
		const change = await this.service.requestChangeMail(req, req.profile.mail, dto);

		// creates URL
		const url = `${req.protocol}://${req.get('host')}/api/caretaker/profile/mail?code=${change.confirmToken}`;

		// sends mail
		await this.mailerService.sendMail({
			to: change.newMail,
			subject: `Change Mail Confirmation`,
			template: 'caretaker_change_mail',
			context: {
				oldMail: req.profile.mail,
				newMail: change.newMail,
				token: change.confirmToken,
				url,
			},
		});

		return { change, url }
	}


	@Get('mail')
	async confirmChangeMail(
		@Req() req: Request,
		@Query('code') code: string,
	): Promise<{
		profile: CaretakerProfile,
	}> {
		// updates mail
		const profile = await this.service.confirmChangeMail(req, code);

		// adds log
		await this.sharedService.print(
			`caretaker ${profile.print()} is updated mail!`, {
			action: LoggingAction.Update,
			table: 'caretaker_profile',
			requestId: req.id,
			profileId: profile.id,
		});

		return { profile };
	}

	// ----------------------------------------------------------------------

	/////////////////////////////////////////////////////////////////////////
	// Change Password
	/////////////////////////////////////////////////////////////////////////

	@Put('password')
	@UseGuards(CaretakerAuthenGuard)
	async changePassword(
		@Req() req: Request,
		@Body('password') dto: CaretakerChangePasswordDto,
	): Promise<{
		profile: CaretakerProfile,
	}> {
		// updates password
		const profile = await this.service.changePassword(req, req.profile.mail, dto);

		// adds log
		await this.sharedService.print(
			`caretaker ${profile.print()} is updated password!`, {
			action: LoggingAction.Update,
			table: 'caretaker_credential',
			requestId: req.id,
			profileId: profile.id,
		});

		return { profile };
	}

	// ----------------------------------------------------------------------

	/////////////////////////////////////////////////////////////////////////
	// Change Settings
	/////////////////////////////////////////////////////////////////////////

	@Put('settings')
	@UseGuards(CaretakerAuthenGuard)
	async changeSettings(
		@Req() req: Request,
		@Body('settings', new ValidationPipe({ whitelist: true })) dto: CaretakerSettingsDto,
	): Promise<{
		profile: CaretakerProfile,
	}> {
		// updates password
		const profile = await this.service.changeSettings(req, req.profile.id, dto);

		// adds log
		await this.sharedService.print(
			`caretaker ${profile.print()} is updated settings: ${profile.extra.settings}`, {
			action: LoggingAction.Update,
			table: 'caretaker_profile_extra',
			requestId: req.id,
			profileId: profile.id,
		});

		return { profile };
	}

	// ----------------------------------------------------------------------

	/////////////////////////////////////////////////////////////////////////
	// Profile Icons
	/////////////////////////////////////////////////////////////////////////

	@Post('icon')
	@UseGuards(CaretakerAuthenGuard)
	@UseInterceptors(FileInterceptor('icon'))
	async changeIcon(
		@Req() req: Request,
		@Res() res: Response,
		@UploadedFile() icon: Express.Multer.File,
	): Promise<any> {
		// checks uploaded image
		this.logger.debug(`${req.id}; icon: ${inspect(icon)}`);
		if (!icon)
			throw Errors.E400UploadFail();
		if (icon.mimetype !== 'image/png' && icon.mimetype !== 'image/jpeg' && icon.mimetype !== 'image/gif')
			throw Errors.E400UploadIsNotTypeImage();
		const limit = this.config.caretaker.iconFileLimit;
		if (icon.size >= limit)
			throw Errors.E400UploadIsTooBig(convertBytes(limit));

		// puts file into database
		const profile = await this.service.changeIcon(req, req.profile.id, icon);

		// puts icon to output
		res.writeHead(200, { 'Content-Type': profile.extra.mime });
		res.end(profile.extra.icon);
	}


	@Get('icon')
	@UseGuards(CaretakerAuthenGuard)
	async viewIcon(
		@Req() req: Request,
		@Res() res: Response,
	): Promise<any> {
		const { mime, icon } = await this.service.getIcon(req, req.profile.id);
		res.writeHead(200, { 'Content-Type': mime });
		res.end(icon);
	}


	@Delete('icon')
	@UseGuards(CaretakerAuthenGuard)
	async removeIcon(
		@Req() req: Request,
		@Res() res: Response,
	): Promise<any> {
		await this.service.changeIcon(req, req.profile.id, null);
		const { mime, icon } = await this.service.getIcon(req, req.profile.id);
		res.writeHead(200, { 'Content-Type': mime });
		res.end(icon);
	}

	// ----------------------------------------------------------------------

	/////////////////////////////////////////////////////////////////////////
	// Resignation
	/////////////////////////////////////////////////////////////////////////

	@Put('resign')
	@UseGuards(CaretakerAuthenGuard)
	async resign(
		@Req() req: Request,
		@Body('resign') dto: CaretakerResignDto,
	): Promise<{
		profile: CaretakerProfile,
	}> {
		// updates resigned flag
		const profile = await this.service.resign(req, req.profile.mail, dto);

		// adds log
		await this.sharedService.print(
			`caretaker ${profile.print()} is resigned!`, {
			action: LoggingAction.Update,
			table: 'caretaker_profile',
			requestId: req.id,
			profileId: profile.id,
		});

		return { profile };
	}
}
