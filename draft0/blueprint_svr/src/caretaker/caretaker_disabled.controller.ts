import {
	Logger,
	Controller,
	Req,
	Param,
	Body,
	Put,
	Delete,
	UseGuards,
} from '@nestjs/common';
import { Request } from 'express';
import { LoggingAction } from '../shared/logging.entity';
import { SharedService } from '../shared/shared.service';
import { MemberProfile } from '../member/member_profile.entity';
import { CaretakerProfile } from './caretaker_profile.entity';
import { CaretakerDisabledDto } from './caretaker_disabled.dto';
import { CaretakerDisabledService } from './caretaker_disabled.service';
import { CaretakerAuthenGuard } from './caretaker_authen.guard';


@Controller(`/api/caretaker/disabled`)
export class CaretakerDisabledController {

	private readonly logger = new Logger(CaretakerDisabledController.name);
	private readonly config = this.sharedService.config;

	constructor(
		private readonly sharedService: SharedService,
		private readonly service: CaretakerDisabledService,
	) { }

	// ----------------------------------------------------------------------

	@Put()
	@UseGuards(CaretakerAuthenGuard)
	async disabledMember(
		@Req() req: Request,
		@Body('disabled') dto: CaretakerDisabledDto,
	): Promise<{
		profile: MemberProfile,
	}> {
		// disabled/enabled
		const profile = await this.service.disabledMember(req, dto);

		// adds log
		await this.sharedService.print(
			`caretaker ${(req.profile as CaretakerProfile).print()} set member disabled=${dto.disabled}, target profile: ${profile.fullPrint()}`, {
			action: LoggingAction.Update,
			table: 'caretaker_address',
			requestId: req.id,
			profileId: profile.id,
		});

		return { profile };
	}

	@Put('caretaker')
	@UseGuards(CaretakerAuthenGuard)
	async disabledCaretaker(
		@Req() req: Request,
		@Body('disabled') dto: CaretakerDisabledDto,
	): Promise<{
		profile: CaretakerProfile,
	}> {
		// disabled/enabled
		const profile = await this.service.disabledCaretaker(req, dto);

		// adds log
		await this.sharedService.print(
			`caretaker ${(req.profile as CaretakerProfile).print()} set caretaker disabled=${dto.disabled}, target profile: ${profile.fullPrint()}`, {
			action: LoggingAction.Update,
			table: 'caretaker_address',
			requestId: req.id,
			profileId: profile.id,
		});

		return { profile };
	}

}
