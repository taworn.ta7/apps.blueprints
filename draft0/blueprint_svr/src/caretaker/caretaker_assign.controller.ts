import {
	Logger,
	Controller,
	Req,
	Body,
	Post,
	UseGuards,
} from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';
import { Request } from 'express';
import { LoggingAction } from '../shared/logging.entity';
import { SharedService } from '../shared/shared.service';
import { CaretakerProfile } from './caretaker_profile.entity';
import { CaretakerAssignDto } from './caretaker_assign.dto';
import { CaretakerAssignService } from './caretaker_assign.service';
import { CaretakerAuthenRootGuard } from './caretaker_authen.guard';


/**
 * This class assigns a new caretaker.
 */
@Controller(`/api/caretaker/assign`)
export class CaretakerAssignController {

	private readonly logger = new Logger(CaretakerAssignController.name);
	private readonly config = this.sharedService.config;

	constructor(
		private readonly mailerService: MailerService,
		private readonly sharedService: SharedService,
		private readonly service: CaretakerAssignService,
	) { }

	// ----------------------------------------------------------------------

	@Post()
	@UseGuards(CaretakerAuthenRootGuard)
	async assign(
		@Req() req: Request,
		@Body('assign') dto: CaretakerAssignDto,
	): Promise<{
		profile: CaretakerProfile,
		password: string,
	}> {
		// creates assign record
		const { profile, password } = await this.service.assign(req, dto);

		// sends mail
		await this.mailerService.sendMail({
			to: profile.mail,
			subject: `New Caretaker Confirmation`,
			template: 'caretaker_assign',
			context: {
				mail: profile.mail,
				password,
			},
		});

		// adds log
		await this.sharedService.print(
			`caretaker created: ${profile.fullPrint()}`, {
			action: LoggingAction.Create,
			table: 'caretaker_profile',
			requestId: req.id,
			profileId: profile.id,
		});

		return { profile, password }
	}

}
