import {
	Entity,
	Column,
	PrimaryGeneratedColumn,
	CreateDateColumn,
	UpdateDateColumn,
	OneToOne,
	JoinColumn,
	Index,
} from 'typeorm';
import { inspect } from 'node:util';
import { CaretakerEntityConsts } from './caretaker_entity_consts';
import { CaretakerProfile } from './caretaker_profile.entity';


/**
 * Caretaker's credential data.
 */
@Entity({
	name: 'caretaker_credential',
})
export class CaretakerCredential {

	@PrimaryGeneratedColumn({
		type: 'bigint',
	})
	id: string;

	/**
	 * Link to [CaretakerProfile]{@link CaretakerProfile.html}.
	 */
	@OneToOne(() => CaretakerProfile, (o) => o.credential)
	@JoinColumn({
		name: 'profile_id',
	})
	profile: CaretakerProfile;

	// ----------------------------------------------------------------------

	// Password

	@Column({
		length: CaretakerEntityConsts.Salt,
	})
	salt: string;

	@Column({
		length: CaretakerEntityConsts.Hash,
	})
	hash: string;

	/**
	 * Sign-in token.
	 */
	@Index({
		unique: true,
	})
	@Column({
		length: CaretakerEntityConsts.Token,
		nullable: true,
	})
	token: string | null;

	/**
	 * Last sign-in date/time.
	 */
	@Column({
		nullable: true,
	})
	begin: Date | null;

	/**
	 * Date/time to update expiry time.
	 */
	@Column({
		nullable: true,
	})
	refresh: Date | null;

	/**
	 * Session expiry date/time.
	 */
	@Column({
		nullable: true,
	})
	expire: Date | null;

	/**
	 * Last sign-out date/time.
	 */
	@Column({
		nullable: true,
	})
	end: Date | null;

	// ----------------------------------------------------------------------

	@CreateDateColumn({})
	created: Date;

	@UpdateDateColumn({})
	updated: Date;

	constructor(o?: Partial<CaretakerCredential>) {
		Object.assign(this, o)
	}

	// ----------------------------------------------------------------------

	print() {
		if (!this.profile)
			return `[${this.id}]`;
		else
			return `[owner ${this.profile.id}/${this.profile.mail}, ${this.id}]`;
	}

	fullPrint(depth?: number) {
		return inspect(this, { depth });
	}

}
