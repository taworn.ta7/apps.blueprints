import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
//import { AxiosService } from '../shared/axios.service';
import { SharedService } from '../shared/shared.service';
import { SharedModule } from '../shared/shared.module';
import { PrecinctService } from '../precinct/precinct.service';
import { PrecinctModule } from '../precinct/precinct.module';
import { MemberService } from '../member/member.service';
import { MemberModule } from '../member/member.module';

import { CaretakerProfile } from './caretaker_profile.entity';
import { CaretakerProfileExtra } from './caretaker_profile_extra.entity';
import { CaretakerCredential } from './caretaker_credential.entity';
import { CaretakerChangeProperty } from './caretaker_change_property.entity';
import { CaretakerAddress } from './caretaker_address.entity';

import { CaretakerService } from './caretaker.service';
import { CaretakerStockIconsService } from './caretaker_stock_icons.service';
import { CaretakerChangePropertyService } from './caretaker_change_property.service';
import { CaretakerProfileService } from './caretaker_profile.service';
import { CaretakerAssignService } from './caretaker_assign.service';
import { CaretakerResetService } from './caretaker_reset.service';
import { CaretakerAuthenService } from './caretaker_authen.service';
import { CaretakerAddressService } from './caretaker_address.service';
import { CaretakerDisabledService } from './caretaker_disabled.service';

import { CaretakerProfileController } from './caretaker_profile.controller';
import { CaretakerAssignController } from './caretaker_assign.controller';
import { CaretakerResetController } from './caretaker_reset.controller';
import { CaretakerAuthenController } from './caretaker_authen.controller';
import { CaretakerAddressController } from './caretaker_address.controller';
import { CaretakerDisabledController } from './caretaker_disabled.controller';


@Module({
	imports: [
		SharedModule,
		PrecinctModule,
		MemberModule,
		TypeOrmModule.forFeature([
			CaretakerProfile,
			CaretakerProfileExtra,
			CaretakerCredential,
			CaretakerChangeProperty,
			CaretakerAddress,
		]),
	],
	exports: [
		TypeOrmModule,
	],
	providers: [
		//AxiosService
		SharedService,
		PrecinctService,
		MemberService,
		CaretakerService,
		CaretakerStockIconsService,
		CaretakerChangePropertyService,
		CaretakerProfileService,
		CaretakerAssignService,
		CaretakerResetService,
		CaretakerAuthenService,
		CaretakerAddressService,
		CaretakerDisabledService,
	],
	controllers: [
		CaretakerProfileController,
		CaretakerAssignController,
		CaretakerResetController,
		CaretakerAuthenController,
		CaretakerAddressController,
		CaretakerDisabledController,
	],
})
export class CaretakerModule { }
