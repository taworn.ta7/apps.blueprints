import {
	Allow,
	IsNotEmpty,
	IsInt,
	Min,
	Max,
} from 'class-validator';


export class CaretakerSettingsDto {
	@Allow()
	@IsNotEmpty()
	@IsInt()
	@Min(1)
	@Max(100)
	dummy: number;
}
