import {
	Logger,
	Injectable,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Request } from 'express';
import { setPassword, generateToken } from '../helpers/crypto_ex';
import { SharedService } from '../shared/shared.service';
import { CaretakerProfile } from './caretaker_profile.entity';
import { CaretakerCredential } from './caretaker_credential.entity';
import { CaretakerChangeProperty, CaretakerChangeAction } from './caretaker_change_property.entity';
import { CaretakerResetDto } from './caretaker_reset.dto';
import { CaretakerChangePropertyService } from './caretaker_change_property.service';


/**
 * This class provides how to do reset password in business layer.
 */
@Injectable()
export class CaretakerResetService {

	private readonly logger = new Logger(CaretakerResetService.name);
	private readonly config = this.sharedService.config;

	constructor(
		@InjectRepository(CaretakerCredential)
		private readonly credentialRepo: Repository<CaretakerCredential>,

		@InjectRepository(CaretakerChangeProperty)
		private readonly propertyRepo: Repository<CaretakerChangeProperty>,

		private readonly sharedService: SharedService,
		private readonly propertyService: CaretakerChangePropertyService,
	) { }

	// ----------------------------------------------------------------------

	/**
	 * Saves reset password form into database.
	 * 
	 * @param {CaretakerChangeProperty} dto  DTO contains reset password form.
	 * @returns Returns new 'CaretakerChangeProperty' record.
	 */
	async reset(req: Request, dto: CaretakerResetDto): Promise<CaretakerChangeProperty> {
		// this mail must already in 'CaretakerProfile'
		await this.propertyService.loadProfile(req, dto.mail);

		// creates reset
		const prop = new CaretakerChangeProperty();
		prop.action = CaretakerChangeAction.ResetPassword;
		prop.mail = dto.mail;
		prop.done = new Date(Date.now() + this.config.caretaker.resetExpire);
		prop.confirmToken = generateToken(64);
		await this.propertyRepo.save(prop);
		this.logger.log(`${req.id}; caretaker request to reset password: ${prop.fullPrint()}`);

		return prop;
	}


	/**
	 * Confirms the reset password form.  This will generate new password and send mail with new password.
	 * 
	 * @param {string} code  Code given from the confirmation mail.
	 * @returns The 'CaretakerProfile' which reset password, along with password.
	 */
	async confirm(req: Request, code: string): Promise<{
		profile: CaretakerProfile,
		password: string,
	}> {
		// checks code
		const prop = await this.propertyService.findCode(code, CaretakerChangeAction.ResetPassword);

		// again, this mail must already in 'CaretakerProfile'
		const profile = await this.propertyService.loadProfile(req, prop.mail);

		// generates new password
		const generate = generateToken(8);
		const pass = setPassword(generate);
		profile.credential.salt = pass.salt;
		profile.credential.hash = pass.hash;
		profile.credential.token = null;
		await this.credentialRepo.save(profile.credential);
		this.logger.log(`${req.id}; caretaker ${profile.print()} password is reset!`);

		// disables this record, so it cannot used again
		prop.done = new Date();
		await this.propertyRepo.save(prop);

		return { profile, password: generate };
	}

}
