import {
	Entity,
	Column,
	PrimaryGeneratedColumn,
	CreateDateColumn,
	UpdateDateColumn,
	Index,
} from 'typeorm';
import { inspect } from 'node:util';
import { CaretakerEntityConsts } from './caretaker_entity_consts';


/**
 * What to change?
 */
export enum CaretakerChangeAction {
	SignUp = 'signup',
	ResetPassword = 'reset',
	ChangeMail = 'change_mail',
}


/**
 * Caretaker's sign-up or changing entity.
 */
@Entity({
	name: 'caretaker_change_property'
})
export class CaretakerChangeProperty {

	@PrimaryGeneratedColumn({
		type: 'bigint',
	})
	id: string;

	// ----------------------------------------------------------------------

	/**
	 * What to change?
	 */
	@Column({
		type: 'enum',
		enum: CaretakerChangeAction,
	})
	action: CaretakerChangeAction;

	/**
	 * Current mail address to using.
	 */
	@Column({
		length: CaretakerEntityConsts.Mail,
	})
	mail: string;

	/**
	 * New mail to change to.
	 */
	@Column({
		length: CaretakerEntityConsts.Mail,
		nullable: true,
		name: 'new_mail',
	})
	newMail: string | null;

	// ----------------------------------------------------------------------

	// Password

	@Column({
		length: CaretakerEntityConsts.Salt,
		nullable: true,
	})
	salt: string | null;

	@Column({
		length: CaretakerEntityConsts.Hash,
		nullable: true,
	})
	hash: string | null;

	// ----------------------------------------------------------------------

	/**
	 * This record are expired after this date.
	 */
	@Column({
		nullable: true,
	})
	done: Date | null;

	/**
	 * Confirmation token
	 */
	@Index({
		unique: true,
	})
	@Column({
		length: CaretakerEntityConsts.ConfirmToken,
	})
	confirmToken: string;

	// ----------------------------------------------------------------------

	@CreateDateColumn({})
	created: Date;

	@UpdateDateColumn({})
	updated: Date;

	constructor(o?: Partial<CaretakerChangeProperty>) {
		Object.assign(this, o)
	}

	// ----------------------------------------------------------------------

	print() {
		return `[${this.id}]`;
	}

	fullPrint(depth?: number) {
		return inspect(this, { depth });
	}

}
