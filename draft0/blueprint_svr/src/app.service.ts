import {
	Logger,
	Injectable,
} from '@nestjs/common';


@Injectable()
export class AppService {

	private readonly logger = new Logger(AppService.name);

	constructor(
	) { }

	// ----------------------------------------------------------------------

	/**
	 * Returns server name and version.
	 */
	about(): {
		app: string,
		version: string,
	} {
		this.logger.log(`let's go`);
		this.logger.verbose(`go on`);
		this.logger.debug(`on and on`);
		this.logger.warn(`still fighting`);
		this.logger.error(`alive and kicking`);
		this.logger.fatal(`I'm dieing T_T`);
		return {
			app: process.env.npm_package_name,
			version: process.env.npm_package_version,
		};
	}


	/**
	 * Returns current configuration.
	 */
	config(): any {
		const env = process.env;
		return {
			config: {
				// logging folder
				LOG_DIR: env.LOG_DIR,

				// logging outputs
				LOG_TO_CONSOLE: env.LOG_TO_CONSOLE,
				LOG_TO_FILE: env.LOG_TO_FILE,

				// storage folder
				STORAGE_DIR: env.STORAGE_DIR,

				// upload folder
				UPLOAD_DIR: env.UPLOAD_DIR,

				// database
				DB_USE: env.DB_USE,
				DB_HOST: env.DB_HOST,
				DB_PORT: env.DB_PORT,
				DB_USER: env.DB_USER,
				DB_PASS: '****',
				DB_NAME: env.DB_NAME,
				DB_FILE: env.DB_FILE,

				// test database
				TESTDB_USE: env.TESTDB_USE,
				TESTDB_HOST: env.TESTDB_HOST,
				TESTDB_PORT: env.TESTDB_PORT,
				TESTDB_USER: env.TESTDB_USER,
				TESTDB_PASS: '****',
				TESTDB_NAME: env.TESTDB_NAME,
				TESTDB_FILE: env.TESTDB_FILE,

				// mail
				MAIL_HOST: env.MAIL_HOST,
				MAIL_PORT: env.MAIL_PORT,
				MAIL_USER: env.MAIL_USER,
				MAIL_PASS: '****',
				MAIL_ADMIN: env.MAIL_ADMIN,

				// HTTP port
				HTTP_PORT: env.HTTP_PORT,

				// days to keep before deleting old data?
				DAYS_TO_KEEP_LOGS: env.DAYS_TO_KEEP_LOGS,
				DAYS_TO_KEEP_DBLOGS: env.DAYS_TO_KEEP_DBLOGS,
				DAYS_TO_KEEP_MEMBER_CHANGE_PROP: env.DAYS_TO_KEEP_MEMBER_CHANGE_PROP,

				// client
				CLIENT_HOST: env.CLIENT_HOST,

				// Google
				GOOGLE_CLIENT_ID: env.GOOGLE_CLIENT_ID,
				GOOGLE_CLIENT_SECRET: env.GOOGLE_CLIENT_SECRET,
				GOOGLE_REDIRECT_URL: env.GOOGLE_REDIRECT_URL,

				// LINE
				LINE_CLIENT_ID: env.LINE_CLIENT_ID,
				LINE_CLIENT_SECRET: env.LINE_CLIENT_SECRET,
				LINE_REDIRECT_URL: env.LINE_REDIRECT_URL,

				// member config
				MEMBER_ICON_FILE_LIMIT: env.MEMBER_ICON_FILE_LIMIT,
				MEMBER_ADDRESS_LIMIT: env.MEMBER_ADDRESS_LIMIT,

				// member change property must have expire in days
				// must keep this for DAYS_TO_KEEP_*, too
				MEMBER_SIGNUP_EXPIRE: env.MEMBER_SIGNUP_EXPIRE,
				MEMBER_RESET_EXPIRE: env.MEMBER_RESET_EXPIRE,
				MEMBER_CHANGE_MAIL_EXPIRE: env.MEMBER_CHANGE_MAIL_EXPIRE,

				// member authen, timeout and refresh unit are days
				MEMBER_AUTHEN_SECRET: '****',
				MEMBER_AUTHEN_TIMEOUT: env.AUTHEN_TIMEOUT,
				MEMBER_AUTHEN_REFRESH: env.AUTHEN_REFRESH,

				// caretaker config
				CARETAKER_ICON_FILE_LIMIT: env.CARETAKER_ICON_FILE_LIMIT,
				CARETAKER_ADDRESS_LIMIT: env.CARETAKER_ADDRESS_LIMIT,

				// caretaker change property must have expire in days
				// must keep this for DAYS_TO_KEEP_*, too
				CARETAKER_SIGNUP_EXPIRE: env.CARETAKER_SIGNUP_EXPIRE,
				CARETAKER_RESET_EXPIRE: env.CARETAKER_RESET_EXPIRE,
				CARETAKER_CHANGE_MAIL_EXPIRE: env.CARETAKER_CHANGE_MAIL_EXPIRE,

				// caretaker authen, timeout and refresh unit are days
				CARETAKER_AUTHEN_SECRET: '****',
				CARETAKER_AUTHEN_TIMEOUT: env.AUTHEN_TIMEOUT,
				CARETAKER_AUTHEN_REFRESH: env.AUTHEN_REFRESH,
			}
		};
	}

}
