import {
	Logger,
	Injectable,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Request } from 'express';
import { setPassword, generateToken } from '../helpers/crypto_ex';
import { SharedService } from '../shared/shared.service';
import { MemberProfile } from './member_profile.entity';
import { MemberCredential } from './member_credential.entity';
import { MemberChangeProperty, MemberChangeAction } from './member_change_property.entity';
import { MemberResetDto } from './member_reset.dto';
import { MemberChangePropertyService } from './member_change_property.service';


/**
 * This class provides how to do reset password in business layer.
 */
@Injectable()
export class MemberResetService {

	private readonly logger = new Logger(MemberResetService.name);
	private readonly config = this.sharedService.config;

	constructor(
		@InjectRepository(MemberCredential)
		private readonly credentialRepo: Repository<MemberCredential>,

		@InjectRepository(MemberChangeProperty)
		private readonly propertyRepo: Repository<MemberChangeProperty>,

		private readonly sharedService: SharedService,
		private readonly propertyService: MemberChangePropertyService,
	) { }

	// ----------------------------------------------------------------------

	/**
	 * Saves reset password form into database.
	 * 
	 * @param {MemberChangeProperty} dto  DTO contains reset password form.
	 * @returns Returns new 'MemberChangeProperty' record.
	 */
	async reset(req: Request, dto: MemberResetDto): Promise<MemberChangeProperty> {
		// this mail must already in 'MemberProfile'
		await this.propertyService.loadProfile(req, dto.mail);

		// creates reset
		const prop = new MemberChangeProperty();
		prop.action = MemberChangeAction.ResetPassword;
		prop.mail = dto.mail;
		prop.done = new Date(Date.now() + this.config.member.resetExpire);
		prop.confirmToken = generateToken(64);
		await this.propertyRepo.save(prop);
		this.logger.log(`${req.id}; member request to reset password: ${prop.fullPrint()}`);

		return prop;
	}


	/**
	 * Confirms the reset password form.  This will generate new password and send mail with new password.
	 * 
	 * @param {string} code  Code given from the confirmation mail.
	 * @returns The 'MemberProfile' which reset password, along with password.
	 */
	async confirm(req: Request, code: string): Promise<{
		profile: MemberProfile,
		password: string,
	}> {
		// checks code
		const prop = await this.propertyService.findCode(code, MemberChangeAction.ResetPassword);

		// again, this mail must already in 'MemberProfile'
		const profile = await this.propertyService.loadProfile(req, prop.mail);

		// generates new password
		const generate = generateToken(8);
		const pass = setPassword(generate);
		profile.credential.salt = pass.salt;
		profile.credential.hash = pass.hash;
		profile.credential.token = null;
		await this.credentialRepo.save(profile.credential);
		this.logger.log(`${req.id}; member ${profile.print()} password is reset!`);

		// disables this record, so it cannot used again
		prop.done = new Date();
		await this.propertyRepo.save(prop);

		return { profile, password: generate };
	}

}
