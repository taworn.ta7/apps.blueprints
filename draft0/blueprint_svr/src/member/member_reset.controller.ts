import {
	Logger,
	Controller,
	Req,
	Body,
	Get,
	Post,
	Query,
} from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';
import { Request } from 'express';
import { LoggingAction } from '../shared/logging.entity';
import { SharedService } from '../shared/shared.service';
import { MemberProfile } from './member_profile.entity';
import { MemberChangeProperty } from './member_change_property.entity';
import { MemberResetDto } from './member_reset.dto';
import { MemberResetService } from './member_reset.service';


/**
 * This class provides how to reset password.
 * 
 * The process is:
 * * receives request reset form
 * * get mail which has a link to URL
 * * user click link, which will reset password and send mail new password, too.
 */
@Controller(`/api/member/reset`)
export class MemberResetController {

	private readonly logger = new Logger(MemberResetController.name);
	private readonly config = this.sharedService.config;

	constructor(
		private readonly mailerService: MailerService,
		private readonly sharedService: SharedService,
		private readonly service: MemberResetService,
	) { }

	// ----------------------------------------------------------------------

	@Post()
	async reset(
		@Req() req: Request,
		@Body('reset') dto: MemberResetDto,
	): Promise<{
		reset: MemberChangeProperty,
		url: string,
	}> {
		// creates reset record
		const reset = await this.service.reset(req, dto);

		// creates URL
		//const url = `${req.protocol}://${req.get('host')}/api/member/reset/confirm?code=${reset.confirmToken}`;
		const url = `${this.config.client.host}/mail_shells/member_reset?code=${reset.confirmToken}`;

		// sends mail
		await this.mailerService.sendMail({
			to: reset.mail,
			subject: `Reset Password Confirmation`,
			template: 'member_reset',
			context: {
				mail: reset.mail,
				token: reset.confirmToken,
				url,
			},
		});

		return { reset, url }
	}


	@Get('confirm')
	async confirm(
		@Req() req: Request,
		@Query('code') code: string,
	): Promise<{
		profile: MemberProfile,
	}> {
		// reset member password
		const { profile, password } = await this.service.confirm(req, code);

		// sends mail
		await this.mailerService.sendMail({
			to: profile.mail,
			subject: "Reset Password Success",
			template: 'member_reset_new_password',
			context: {
				mail: profile.mail,
				password,
			}
		});

		// adds log
		await this.sharedService.print(
			`member ${profile.print()} password is reset!`, {
			action: LoggingAction.Update,
			table: 'member_credential',
			requestId: req.id,
			profileId: profile.id,
		});

		return { profile };
	}

}
