import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AxiosService } from '../shared/axios.service';
import { SharedService } from '../shared/shared.service';
import { SharedModule } from '../shared/shared.module';
import { PrecinctService } from '../precinct/precinct.service';
import { PrecinctModule } from '../precinct/precinct.module';

import { MemberProfile } from './member_profile.entity';
import { MemberProfileExtra } from './member_profile_extra.entity';
import { MemberCredential } from './member_credential.entity';
import { MemberChangeProperty } from './member_change_property.entity';
import { MemberAddress } from './member_address.entity';

import { MemberService } from './member.service';
import { MemberStockIconsService } from './member_stock_icons.service';
import { MemberChangePropertyService } from './member_change_property.service';
import { MemberProfileService } from './member_profile.service';
import { MemberSignUpService } from './member_signup.service';
import { MemberResetService } from './member_reset.service';
import { MemberAuthenService } from './member_authen.service';
import { MemberAuthenXGoogleService } from './member_authenx_google.service';
import { MemberAuthenXLineService } from './member_authenx_line.service';
import { MemberAddressService } from './member_address.service';

import { MemberProfileController } from './member_profile.controller';
import { MemberSignUpController } from './member_signup.controller';
import { MemberResetController } from './member_reset.controller';
import { MemberAuthenController } from './member_authen.controller';
import { MemberAuthenXGoogleController } from './member_authenx_google.controller';
import { MemberAuthenXLineController } from './member_authenx_line.controller';
import { MemberAddressController } from './member_address.controller';


@Module({
	imports: [
		SharedModule,
		PrecinctModule,
		TypeOrmModule.forFeature([
			MemberProfile,
			MemberProfileExtra,
			MemberCredential,
			MemberChangeProperty,
			MemberAddress,
		]),
	],
	exports: [
		TypeOrmModule,
	],
	providers: [
		AxiosService,
		SharedService,
		PrecinctService,
		MemberService,
		MemberStockIconsService,
		MemberChangePropertyService,
		MemberProfileService,
		MemberSignUpService,
		MemberResetService,
		MemberAuthenService,
		MemberAuthenXGoogleService,
		MemberAuthenXLineService,
		MemberAddressService,
	],
	controllers: [
		MemberProfileController,
		MemberSignUpController,
		MemberResetController,
		MemberAuthenController,
		MemberAuthenXGoogleController,
		MemberAuthenXLineController,
		MemberAddressController,
	],
})
export class MemberModule { }
