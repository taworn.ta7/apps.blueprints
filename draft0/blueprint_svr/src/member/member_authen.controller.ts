import {
	Logger,
	Controller,
	Req,
	Body,
	Get,
	Put,
	UseGuards,
} from '@nestjs/common';
import { Request } from 'express';
import { LoggingAction } from '../shared/logging.entity';
import { SharedService } from '../shared/shared.service';
import { MemberProfile } from './member_profile.entity';
import { MemberAuthenSignInDto } from './member_authen.dto';
import { MemberAuthenService } from './member_authen.service';
import { MemberAuthenGuard, MemberAuthenOptionalGuard } from './member_authen.guard';


/**
 * This class provides authentication with password.
 */
@Controller(`/api/member/authen`)
export class MemberAuthenController {

	private readonly logger = new Logger(MemberAuthenController.name);
	private readonly config = this.sharedService.config;

	constructor(
		private readonly sharedService: SharedService,
		private readonly service: MemberAuthenService,
	) { }

	// ----------------------------------------------------------------------

	/**
	 * Sign-in with mail and password.
	 * 
	 * @returns If mail and password corrected, it will returns member profile with sign-in token.
	 */
	@Put('signin')
	async signIn(
		@Req() req: Request,
		@Body('signIn') dto: MemberAuthenSignInDto,
	): Promise<{
		profile: MemberProfile,
		token: string,
	}> {
		// sign-in
		const profile = await this.service.signIn(req, dto);

		// adds log
		await this.sharedService.print(
			`member sign-in: ${profile.fullPrint()}`, {
			action: LoggingAction.Update,
			table: 'member_credential',
			requestId: req.id,
			profileId: profile.id,
		});

		const credential = profile.credential;
		delete profile.credential;
		return {
			profile,
			token: credential.token,
		};
	}


	@Put('signout')
	@UseGuards(MemberAuthenGuard)
	async signOut(
		@Req() req: Request,
	): Promise<void> {
		// sign-out
		const profile = await this.service.signOut(req, req.profile!.id);

		// adds log
		await this.sharedService.print(
			`member sign-out: ${profile.fullPrint()}`, {
			action: LoggingAction.Update,
			table: 'member_credential',
			requestId: req.id,
			profileId: profile.id,
		});
	}


	@Get('check')
	@UseGuards(MemberAuthenOptionalGuard)
	async check(
		@Req() req: Request,
	): Promise<{
		profile: MemberProfile,
	}> {
		return {
			profile: req.profile as MemberProfile,
		};
	}

}
