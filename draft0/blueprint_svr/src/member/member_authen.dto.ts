import {
	IsNotEmpty,
	MinLength,
	MaxLength,
} from 'class-validator';
import { MemberEntityConsts } from './member_entity_consts';


export class MemberAuthenSignInDto {
	@IsNotEmpty()
	//@IsEmail()
	@MinLength(5)
	@MaxLength(MemberEntityConsts.Mail)
	mail: string;

	@IsNotEmpty()
	@MinLength(MemberEntityConsts.PasswordMin)
	@MaxLength(MemberEntityConsts.PasswordMax)
	password: string;
}
