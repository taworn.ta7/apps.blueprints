import {
	Logger,
	Injectable,
} from '@nestjs/common';
import * as path from 'node:path';
import * as fs from 'node:fs';
import { SharedService } from '../shared/shared.service';


/**
 * This class provides default member icons.
 */
@Injectable()
export class MemberStockIconsService {

	private readonly logger = new Logger(MemberStockIconsService.name);
	private readonly config = this.sharedService.config;

	constructor(
		private readonly sharedService: SharedService,
	) { }

	// ----------------------------------------------------------------------

	/**
	 * Cached icons.
	 */
	static icons = {
		'a': null, 'b': null,
		'c': null, 'd': null,
		'e': null, 'f': null,
		'g': null, 'h': null,
		'i': null, 'j': null,
		'k': null, 'l': null,
		'm': null, 'n': null,
		'o': null, 'p': null,
		'q': null, 'r': null,
		's': null, 't': null,
		'u': null, 'v': null,
		'w': null, 'x': null,
		'y': null, 'z': null,
		'0': null, '1': null,
		'2': null, '3': null,
		'4': null, '5': null,
		'6': null, '7': null,
		'8': null, '9': null,
		' ': null,
	};


	/**
	 * Gets stock icon.
	 * 
	 * @param {string} name  A name for guessing icon, may not be used.
	 * @returns Returns icon with MIME type.
	 */
	async get(name: string): Promise<{ mime: string, icon: Buffer }> {
		// extracts first character or just space
		let c = ' ';
		if (name && name.length >= 1 && /^[a-zA-Z0-9]/.test(name))
			c = name.charAt(0).toLowerCase();

		// checks icons pack
		if (MemberStockIconsService.icons[c] === null) {
			// loads icon
			const part = c === ' ' ? `ecology` : `${c}-key`;
			const dir = path.join(process.cwd(), 'dist', 'member', 'icons');
			const file = path.join(dir, `icons8-${part}-50.png`);
			const image = await fs.promises.readFile(file);
			MemberStockIconsService.icons[c] = image;
		}

		return {
			mime: 'image/png',
			icon: MemberStockIconsService.icons[c],
		}
	}

}
