import {
	Logger,
	Controller,
	Req,
	Body,
	Get,
	Post,
	Query,
} from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';
import { Request } from 'express';
import { LoggingAction } from '../shared/logging.entity';
import { SharedService } from '../shared/shared.service';
import { MemberProfile } from './member_profile.entity';
import { MemberChangeProperty } from './member_change_property.entity';
import { MemberSignUpDto } from './member_signup.dto';
import { MemberSignUpService } from './member_signup.service';


/**
 * This class provides how to let user sign-up.
 * 
 * The process is:
 * * receives sign-up form
 * * get mail which has a link to URL
 * * user click link, which will create member in database.
 */
@Controller(`/api/member/signup`)
export class MemberSignUpController {

	private readonly logger = new Logger(MemberSignUpController.name);
	private readonly config = this.sharedService.config;

	constructor(
		private readonly mailerService: MailerService,
		private readonly sharedService: SharedService,
		private readonly service: MemberSignUpService,
	) { }

	// ----------------------------------------------------------------------

	@Post()
	async signUp(
		@Req() req: Request,
		@Body('signUp') dto: MemberSignUpDto,
	): Promise<{
		signUp: MemberChangeProperty,
		url: string,
	}> {
		// creates sign-up record
		const signUp = await this.service.signUp(req, dto);

		// creates URL
		//const url = `${req.protocol}://${req.get('host')}${req.path}/confirm?code=${signUp.confirmToken}`;
		const url = `${this.config.client.host}/mail_shells/member_signup?code=${signUp.confirmToken}`;

		// sends mail
		await this.mailerService.sendMail({
			to: signUp.mail,
			subject: `Sign-up Confirmation`,
			template: 'member_signup',
			context: {
				mail: signUp.mail,
				token: signUp.confirmToken,
				url,
			},
		});

		return { signUp, url }
	}


	@Get('confirm')
	async confirm(
		@Req() req: Request,
		@Query('code') code: string,
	): Promise<{
		profile: MemberProfile,
	}> {
		// creates member profile
		const profile = await this.service.confirm(req, code);

		// adds log
		await this.sharedService.print(
			`member created: ${profile.fullPrint()}`, {
			action: LoggingAction.Create,
			table: 'member_profile',
			requestId: req.id,
			profileId: profile.id,
		});

		return { profile };
	}

}
