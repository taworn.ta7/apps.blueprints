import {
	Logger,
	Injectable,
} from '@nestjs/common';
import { InjectDataSource, InjectRepository } from '@nestjs/typeorm';
import { DataSource, Repository } from 'typeorm';
import { Request } from 'express';
import { Errors } from '../helpers/errors';
import { SharedService } from '../shared/shared.service';
import { PrecinctService } from '../precinct/precinct.service';
import { MemberProfile } from './member_profile.entity';
import { MemberAddress } from './member_address.entity';
import { MemberAddressDto } from './member_address.dto';


/**
 * This class provides loads and saves member address(es) in business layer.
 */
@Injectable()
export class MemberAddressService {

	private readonly logger = new Logger(MemberAddressService.name);
	private readonly config = this.sharedService.config;

	constructor(
		@InjectDataSource()
		private readonly dataSource: DataSource,

		@InjectRepository(MemberAddress)
		private readonly addressRepo: Repository<MemberAddress>,

		private readonly sharedService: SharedService,
		private readonly precinctService: PrecinctService,
	) { }

	// ----------------------------------------------------------------------

	/**
	 * Finds one by id.
	 * 
	 * @returns If founds, return 'MemberAddress' record.  Otherwise, exception 'E404AddressNotFound' raises.
	 */
	async findById(id: string): Promise<MemberAddress> {
		const row = await this.dataSource.createQueryBuilder()
			.select('addr')
			.from(MemberAddress, 'addr')
			.leftJoinAndSelect('addr.canton', 'canton')
			.leftJoinAndSelect('addr.district', 'district')
			.leftJoinAndSelect('addr.province', 'province')
			.leftJoinAndSelect('addr.profile', 'profile')
			.where('addr.id = :id', { id })
			.getOne();
		if (!row)
			throw Errors.E404AddressNotFound();
		return row;
	}


	/**
	 * Finds all of member's addresses.
	 */
	async findAllOfMember(profile: MemberProfile): Promise<MemberAddress[]> {
		return await this.dataSource.createQueryBuilder()
			.select('addr')
			.from(MemberAddress, 'addr')
			.leftJoinAndSelect('addr.canton', 'canton')
			.leftJoinAndSelect('addr.district', 'district')
			.leftJoinAndSelect('addr.province', 'province')
			.leftJoinAndSelect('addr.profile', 'profile')
			.where('addr.profile.id = :profileId', { profileId: profile.id })
			.getMany();
	}


	/**
	 * Gets count of member address(es).
	 */
	async countInMember(profile: MemberProfile): Promise<number> {
		return await this.dataSource.createQueryBuilder()
			.select('addr')
			.from(MemberAddress, 'addr')
			.leftJoinAndSelect('addr.canton', 'canton')
			.leftJoinAndSelect('addr.district', 'district')
			.leftJoinAndSelect('addr.province', 'province')
			.leftJoinAndSelect('addr.profile', 'profile')
			.where('addr.profile.id = :profileId', { profileId: profile.id })
			.getCount();
	}

	// ----------------------------------------------------------------------

	/**
	 * Saves new address.
	 */
	async insert(
		req: Request,
		profile: MemberProfile,
		dto: MemberAddressDto,
	): Promise<MemberAddress> {
		// checks ZIP, canton, district and province
		const zip = await this.precinctService.verifyZip(dto.zip, dto.canton, dto.district, dto.province);
		const canton = await this.precinctService.findByCantonId(dto.canton, false);
		const district = await this.precinctService.findByDistrictId(dto.district, false);
		const province = await this.precinctService.findByProvinceId(dto.province, false);

		// adds address
		const addr = this.addressRepo.create();
		addr.profile = profile;
		addr.abode = dto.abode;
		addr.canton = canton;
		addr.district = district;
		addr.province = province;
		addr.zip = dto.zip;
		this.addressRepo.save(addr);
		this.logger.log(`${req.id}; member ${profile.print()} add new address: ${addr.morePrint()}`);
		return addr;
	}


	/**
	 * Replaces existing address.
	 */
	async replace(
		req: Request,
		profile: MemberProfile,
		addr: MemberAddress,
		dto: MemberAddressDto,
	): Promise<MemberAddress> {
		// checks ZIP, canton, district and province
		const zip = await this.precinctService.verifyZip(dto.zip, dto.canton, dto.district, dto.province);
		const canton = await this.precinctService.findByCantonId(dto.canton, false);
		const district = await this.precinctService.findByDistrictId(dto.district, false);
		const province = await this.precinctService.findByProvinceId(dto.province, false);

		// checks DTO id must exists in database and same profile
		const row = await this.dataSource.createQueryBuilder()
			.select('addr')
			.from(MemberAddress, 'addr')
			.leftJoinAndSelect('addr.canton', 'canton')
			.leftJoinAndSelect('addr.district', 'district')
			.leftJoinAndSelect('addr.province', 'province')
			.leftJoinAndSelect('addr.profile', 'profile')
			.where('addr.profile.id = :profileId', { profileId: profile.id })
			.andWhere('addr.id = :id', { id: addr.id })
			.getOne();
		if (!row)
			throw Errors.E404AddressNotFound();

		// edit address
		row.id = addr.id;
		row.profile = profile;
		row.abode = dto.abode;
		row.canton = canton;
		row.district = district;
		row.province = province;
		row.zip = dto.zip;
		this.addressRepo.save(row);
		this.logger.log(`${req.id}; member ${profile.print()} replace address: ${row.morePrint()}`);
		return row;
	}

	/**
	 * Deletes existing address.
	 */
	async delete(
		req: Request,
		profile: MemberProfile,
		addr: MemberAddress,
	): Promise<void> {
		const row = await this.dataSource.createQueryBuilder()
			.select('addr')
			.from(MemberAddress, 'addr')
			.leftJoinAndSelect('addr.canton', 'canton')
			.leftJoinAndSelect('addr.district', 'district')
			.leftJoinAndSelect('addr.province', 'province')
			.leftJoinAndSelect('addr.profile', 'profile')
			.where('addr.profile.id = :profileId', { profileId: profile.id })
			.andWhere('addr.id = :id', { id: addr.id })
			.getOne();
		if (!row)
			throw Errors.E404AddressNotFound();
		this.addressRepo.delete(row.id);
		this.logger.log(`${req.id}; member ${profile.print()} delete address: ${row.morePrint()}`);
	}

}
