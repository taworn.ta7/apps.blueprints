import {
	Entity,
	Column,
	PrimaryGeneratedColumn,
	CreateDateColumn,
	UpdateDateColumn,
	OneToOne,
	Index,
} from 'typeorm';
import { inspect } from 'node:util';
import { MemberEntityConsts } from './member_entity_consts';
import { MemberProfileExtra } from './member_profile_extra.entity';
import { MemberCredential } from './member_credential.entity';


/**
 * Member profile entity.
 */
@Entity({
	name: 'member_profile',
})
export class MemberProfile {

	@PrimaryGeneratedColumn({
		type: 'bigint',
	})
	id: string;

	// ----------------------------------------------------------------------

	/**
	 * Your personal mail.
	 */
	@Index({
		unique: true,
	})
	@Column({
		length: MemberEntityConsts.Mail,
	})
	mail: string;

	/**
	 * Created date/time for sign-up with our site.
	 */
	@Column({
		default: () => 'CURRENT_TIMESTAMP',
	})
	begin: Date;

	/**
	 * Disabled by admin.
	 */
	@Column({
		nullable: true,
	})
	disabled: Date | null;

	/**
	 * Resigned by this member.
	 */
	@Column({
		nullable: true,
	})
	resigned: Date | null;

	// ----------------------------------------------------------------------

	/**
	 * Google sign-in mail.
	 */
	@Index({
		unique: true,
	})
	@Column({
		length: MemberEntityConsts.Mail,
		nullable: true,
		name: 'google_mail',
	})
	google_mail: string;

	/**
	 * Google sign-in timestamp.
	 */
	@Column({
		nullable: true,
	})
	google_signin: Date;

	// ----------------------------------------------------------------------

	/**
	 * Line sign-in mail.
	 */
	@Index({
		unique: true,
	})
	@Column({
		length: MemberEntityConsts.Mail,
		nullable: true,
		name: 'line_mail',
	})
	line_mail: string;

	/**
	 * Line sign-in timestamp.
	 */
	@Column({
		nullable: true,
	})
	line_signin: Date;

	// ----------------------------------------------------------------------

	/**
	 * Link to [MemberProfileExtra]{@link MemberProfileExtra.html}.
	 */
	@OneToOne(() => MemberProfileExtra, (o) => o.profile, {
		cascade: true,
		onDelete: 'CASCADE',
		orphanedRowAction: 'delete',
	})
	extra: MemberProfileExtra;

	/**
	 * Link to [MemberCredential]{@link MemberCredential.html}.
	 */
	@OneToOne(() => MemberCredential, (o) => o.profile, {
		cascade: true,
		onDelete: 'CASCADE',
		orphanedRowAction: 'delete',
	})
	credential: MemberCredential;

	// ----------------------------------------------------------------------

	@CreateDateColumn({})
	created: Date;

	@UpdateDateColumn({})
	updated: Date;

	constructor(o?: Partial<MemberProfile>) {
		Object.assign(this, o)
	}

	// ----------------------------------------------------------------------

	print() {
		return `[${this.id + '/' + this.mail}]`;
	}

	fullPrint(depth?: number) {
		return inspect(this, { depth });
	}

}
