import {
	Logger,
	Controller,
	Req,
	Param,
	Body,
	Get,
	Put,
	Post,
	Delete,
	UseGuards,
} from '@nestjs/common';
import { Request } from 'express';
import { Errors } from '../helpers/errors';
import { LoggingAction } from '../shared/logging.entity';
import { SharedService } from '../shared/shared.service';
import { MemberProfile } from './member_profile.entity';
import { MemberAddress } from './member_address.entity';
import { MemberAddressDto } from './member_address.dto';
import { MemberAddressService } from './member_address.service';
import { MemberAuthenGuard } from './member_authen.guard';


@Controller(`/api/member/address`)
@UseGuards(MemberAuthenGuard)
export class MemberAddressController {

	private readonly logger = new Logger(MemberAddressController.name);
	private readonly config = this.sharedService.config;

	constructor(
		private readonly sharedService: SharedService,
		private readonly service: MemberAddressService,
	) { }

	// ----------------------------------------------------------------------

	@Get()
	async view(
		@Req() req: Request,
	): Promise<{
		addresses: MemberAddress[],
	}> {
		const profile = req.profile as MemberProfile;
		return {
			addresses: await this.service.findAllOfMember(profile),
		};
	}


	@Post()
	async save(
		@Req() req: Request,
		@Body('address') dto: MemberAddressDto,
	): Promise<{
		address: MemberAddress,
	}> {
		// retrives profile from request
		const profile = req.profile as MemberProfile;
		this.logger.verbose(`${req.id}; member address DTO: ${JSON.stringify(dto, null, 2)}`);

		// checks address limit
		const count = await this.service.countInMember(profile);
		if (count >= this.config.member.addressLimit)
			throw Errors.E403MemberAddressIsLimited(this.config.member.addressLimit);

		// saves
		const address = await this.service.insert(req, profile, dto);

		// adds log
		await this.sharedService.print(
			`member ${profile.print()} add new address: ${address.fullPrint()}`, {
			action: LoggingAction.Create,
			table: 'member_address',
			requestId: req.id,
			profileId: profile.id,
		});

		return { address };
	}


	@Put(':id')
	async replace(
		@Req() req: Request,
		@Param('id') id: string,
		@Body('address') dto: MemberAddressDto,
	): Promise<{
		address: MemberAddress,
	}> {
		// retrives profile from request
		const profile = req.profile as MemberProfile;
		this.logger.verbose(`${req.id}; replace ${id} with new address DTO: ${JSON.stringify(dto, null, 2)}`);

		// saves
		const current = await this.service.findById(id);
		const address = await this.service.replace(req, profile, current, dto);

		// adds log
		await this.sharedService.print(
			`member ${profile.print()} replace address: ${address.fullPrint()}`, {
			action: LoggingAction.Update,
			table: 'member_address',
			requestId: req.id,
			profileId: profile.id,
		});

		return { address };
	}


	@Delete(':id')
	async delete(
		@Req() req: Request,
		@Param('id') id: string,
	): Promise<{
	}> {
		// retrives profile from request
		const profile = req.profile as MemberProfile;
		this.logger.verbose(`${req.id}; delete address with id ${id}`);

		// deletes
		const current = await this.service.findById(id);
		await this.service.delete(req, profile, current);

		// adds log
		await this.sharedService.print(
			`member ${profile.print()} delete address: ${current.fullPrint()}`, {
			action: LoggingAction.Delete,
			table: 'member_address',
			requestId: req.id,
			profileId: profile.id,
		});

		return {};
	}

}
