import {
	Logger,
	Injectable,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Request } from 'express';
import { validatePassword } from '../helpers/crypto_ex';
import { Errors } from '../helpers/errors';
import { SharedService } from '../shared/shared.service';
import { MemberProfile } from './member_profile.entity';
import { MemberChangeProperty, MemberChangeAction } from './member_change_property.entity';
import { MemberService } from './member.service';


/**
 * This class provides how to do change properties in business layer.
 */
@Injectable()
export class MemberChangePropertyService {

	private readonly logger = new Logger(MemberChangeProperty.name);
	private readonly config = this.sharedService.config;

	constructor(
		@InjectRepository(MemberProfile)
		private readonly profileRepo: Repository<MemberProfile>,

		@InjectRepository(MemberChangeProperty)
		private readonly propertyRepo: Repository<MemberChangeProperty>,

		private readonly sharedService: SharedService,
		private readonly memberService: MemberService,
	) { }

	// ----------------------------------------------------------------------

	/**
	 * Gets record by code.
	 * 
	 * @param {string} code  Code given from the confirmation mail.
	 * @returns The 'MemberChangeProperty' record.
	 */
	async findCode(code: string, action: MemberChangeAction): Promise<MemberChangeProperty> {
		// checks code
		if (!code || code.trim() === '')
			throw Errors.E404DataNotFound();

		// checks entity
		const property = await this.propertyRepo.findOne({ where: { confirmToken: code } });
		if (!property)
			throw Errors.E404DataNotFound();

		// checks action
		if (property.action !== action)
			throw Errors.E403DataInvalid();

		// checks expiry
		if (property.done && property.done.getTime() < Date.now())
			throw Errors.E403DataExpire();

		return property;
	}


	/**
	 * Checks mail address must NOT be exists.  So it can be used.
	 * 
	 * @returns The result will always true.
	 */
	async checkMailAvailable(mail: string): Promise<boolean> {
		// this mail must NOT be in 'MemberProfile', yet
		const count = await this.profileRepo.count({ where: { mail } });
		if (count > 0)
			throw Errors.E403MailExists();
		return true;
	}


	/**
	 * Checks this mail must already in 'MemberProfile'.
	 * 
	 * @returns The 'MemberProfile' with 'MemberCredential' attached.
	 */
	async loadProfile(req: Request, mail: string): Promise<MemberProfile> {
		const profile = await this.profileRepo.findOne({
			where: { mail },
			relations: ['credential'],
		});
		if (!profile)
			throw Errors.E404MemberNotFound();
		this.memberService.checkProfileCanSignIn(profile);
		return profile;
	}


	/**
	 * Checks password must be valid.
	 * 
	 * @returns The 'MemberProfile' with 'MemberCredential' attached.
	 */
	async checkPasswordAndLoadProfile(req: Request, mail: string, password: string): Promise<MemberProfile> {
		const profile = await this.profileRepo.findOne({
			where: { mail },
			relations: ['credential'],
		});
		if (!profile)
			throw Errors.E404MemberNotFound();
		if (!validatePassword(password, profile.credential.salt, profile.credential.hash))
			throw Errors.E401MemberMailOrPasswordInvalid();
		this.memberService.checkProfileCanSignIn(profile);
		return profile;
	}

}
