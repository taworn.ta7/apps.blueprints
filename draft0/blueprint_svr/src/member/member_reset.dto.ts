import {
	IsNotEmpty,
	Matches,
	MinLength,
	MaxLength,
} from 'class-validator';
import { regexMail } from '../helpers/string_ex';
import { MemberEntityConsts } from './member_entity_consts';


export class MemberResetDto {
	@IsNotEmpty()
	//@IsEmail()
	@Matches(regexMail)
	@MinLength(5)
	@MaxLength(MemberEntityConsts.Mail)
	mail: string;
}
