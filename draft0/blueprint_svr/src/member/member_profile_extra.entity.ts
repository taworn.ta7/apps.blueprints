import {
	Entity,
	Column,
	PrimaryGeneratedColumn,
	CreateDateColumn,
	UpdateDateColumn,
	OneToOne,
	JoinColumn,
} from 'typeorm';
import { inspect } from 'node:util';
import { MemberProfile } from './member_profile.entity';


/**
 * Member profile extra entity.
 */
@Entity({
	name: 'member_profile_extra',
})
export class MemberProfileExtra {

	@PrimaryGeneratedColumn({
		type: 'bigint',
	})
	id: string;

	/**
	 * Link to [MemberProfile]{@link MemberProfile.html}.
	 */
	@OneToOne(() => MemberProfile, (o) => o.extra)
	@JoinColumn({
		name: 'profile_id',
	})
	profile: MemberProfile;

	// ----------------------------------------------------------------------

	/**
	 * Icon MIME.
	 */
	@Column({
		length: 50,
		nullable: true,
	})
	mime: string | null;

	/**
	 * Icon stored.
	 */
	@Column({
		type: 'mediumblob',
		nullable: true,
	})
	icon: Buffer | null;

	/**
	 * Settings in JSON form.
	 */
	@Column({
		type: 'mediumtext',
	})
	settings: string;

	// ----------------------------------------------------------------------

	@CreateDateColumn({})
	created: Date;

	@UpdateDateColumn({})
	updated: Date;

	constructor(o?: Partial<MemberProfileExtra>) {
		Object.assign(this, o)
	}

	// ----------------------------------------------------------------------

	print() {
		if (!this.profile)
			return `[${this.id}]`;
		else
			return `[owner ${this.profile.id}/${this.profile.mail}, ${this.id}]`;
	}

	fullPrint(depth?: number) {
		return inspect(this, { depth });
	}

}
