import {
	IsNotEmpty,
	Matches,
	MinLength,
	MaxLength,
} from 'class-validator';
import { regexMail } from '../helpers/string_ex';
import { MemberEntityConsts } from './member_entity_consts';


export class MemberChangeMailDto {
	@IsNotEmpty()
	//@IsEmail()
	@Matches(regexMail)
	@MinLength(5)
	@MaxLength(MemberEntityConsts.Mail)
	newMail: string;

	@IsNotEmpty()
	@MinLength(MemberEntityConsts.PasswordMin)
	@MaxLength(MemberEntityConsts.PasswordMax)
	password: string;
}


export class MemberChangePasswordDto {
	@IsNotEmpty()
	@MinLength(MemberEntityConsts.PasswordMin)
	@MaxLength(MemberEntityConsts.PasswordMax)
	currentPassword: string;

	@IsNotEmpty()
	@MinLength(MemberEntityConsts.PasswordMin)
	@MaxLength(MemberEntityConsts.PasswordMax)
	newPassword: string;
}


export class MemberResignDto {
	@IsNotEmpty()
	@MinLength(MemberEntityConsts.PasswordMin)
	@MaxLength(MemberEntityConsts.PasswordMax)
	currentPassword: string;
}
