import {
	Logger,
	Injectable,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Request } from 'express';
import { validatePassword } from '../helpers/crypto_ex';
import { Errors } from '../helpers/errors';
import { SharedService } from '../shared/shared.service';
import { MemberProfile } from './member_profile.entity';
import { MemberCredential } from './member_credential.entity';
import { MemberAuthenSignInDto } from './member_authen.dto';
import { MemberService } from './member.service';


/**
 * This class provides how to sign-in and retrives sign-in token in business layer.
 */
@Injectable()
export class MemberAuthenService {

	private readonly logger = new Logger(MemberAuthenService.name);
	private readonly config = this.sharedService.config;

	constructor(
		@InjectRepository(MemberProfile)
		private readonly profileRepo: Repository<MemberProfile>,

		@InjectRepository(MemberCredential)
		private readonly credentialRepo: Repository<MemberCredential>,

		private readonly sharedService: SharedService,
		private readonly memberService: MemberService,
	) { }

	// ----------------------------------------------------------------------

	/**
	 * Authorizes the mail and password.
	 * Returns member data and sign-in token and will be use all the session.
	 */
	async signIn(req: Request, dto: MemberAuthenSignInDto): Promise<MemberProfile> {
		// selects profile
		const profile = await this.profileRepo.findOne({
			where: { mail: dto.mail },
			relations: ['credential', 'extra'],
		});
		if (!profile || !validatePassword(dto.password, profile.credential.salt, profile.credential.hash))
			throw Errors.E401MemberMailOrPasswordInvalid();
		this.memberService.checkProfileCanSignIn(profile);

		// profile sign-in
		await this.memberService.profileSignIn(profile);

		// success
		this.logger.log(`${req.id}; member sign-in: ${profile.print()}`);
		return profile;
	}


	/**
	 * Signs off from the current session.  The sign-in token will be invalid.
	 */
	async signOut(req: Request, profileId: string): Promise<MemberProfile> {
		// selects profile
		const profile = await this.profileRepo.findOne({
			where: { id: profileId },
			relations: ['credential'],
		});

		// updates credential
		const now = new Date();
		profile.credential.end = now;
		profile.credential.token = null;
		await this.credentialRepo.save(profile.credential);

		// success
		this.logger.log(`${req.id}; member sign-out: ${profile.print()}`);
		return profile;
	}

	// ----------------------------------------------------------------------

	/**
	 * Extracts bearer from authorization.
	 *
	 * @param {string} authorization  Authorization string from Request.
	 * @returns Returns the bearer in authorization, or null if no bearer.
	 */
	tokenFromHeaders(authorization: string): string | null {
		if (authorization) {
			const a = authorization.split(' ');
			if (a.length >= 2) {
				if (a[0].toLowerCase() === 'bearer') {
					return a[1];
				}
			}
		}
		return null;
	}


	/**
	 * Creates a 'MemberProfile' from authorization header string.
	 *
	 * @param {string} bearer  The bearer string, extracts from tokenFromHeaders().
	 * @returns Returns the 'MemberProfile' record.
	 */
	async memberFromHeaders(bearer: string): Promise<MemberProfile> {
		// searches bearer in 'MemberCredential'
		const credential = await this.credentialRepo.findOne({
			where: { token: bearer },
			relations: ['profile'],
		});
		if (!credential || !credential.profile)
			throw Errors.E401MemberNeedSignIn();
		this.memberService.checkProfileCanSignIn(credential.profile);

		// checks if session is expired
		const now = new Date();
		if (credential.expire.getTime() < now.getTime())
			throw Errors.E401MemberSignInTimeout();

		// checks to replenish time
		if (credential.refresh.getTime() + this.config.member.authenRefresh < now.getTime()) {
			const expire = now.getTime() + this.config.member.authenTimeOut;
			credential.refresh = now;
			credential.expire = new Date(expire);
			await this.credentialRepo.save(credential);
			this.logger.debug(`member ${credential.print()} has been replenish session time!`);
		}

		return credential.profile;
	}

}
