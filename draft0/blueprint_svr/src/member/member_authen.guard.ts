import {
	Logger,
	Injectable,
	CanActivate,
	ExecutionContext,
} from '@nestjs/common';
import { Request } from 'express';
import { Errors } from '../helpers/errors';
import { SharedService } from '../shared/shared.service';
import { MemberAuthenService } from './member_authen.service';


/**
 * Authentication guard.
 * After this middleware, Request.profile is guarantee to be always not null.
 */
@Injectable()
export class MemberAuthenGuard implements CanActivate {

	private readonly logger = new Logger(MemberAuthenGuard.name);
	private readonly config = this.sharedService.config;

	constructor(
		private readonly sharedService: SharedService,
		private readonly service: MemberAuthenService,
	) { }

	async canActivate(context: ExecutionContext): Promise<boolean> {
		const req = context.switchToHttp().getRequest<Request>();
		const authorization = req.headers.authorization;
		const bearer = this.service.tokenFromHeaders(authorization);
		if (!bearer)
			throw Errors.E404DataNotFound(/* because: no bearer */);
		req.profile = await this.service.memberFromHeaders(bearer);
		return true;
	}

}


/**
 * Optional guard.  Can pass this with Request.profile attached or not.
 */
@Injectable()
export class MemberAuthenOptionalGuard implements CanActivate {

	private readonly logger = new Logger(MemberAuthenOptionalGuard.name);

	constructor(
		private readonly service: MemberAuthenService,
	) { }

	async canActivate(context: ExecutionContext): Promise<boolean> {
		const req = context.switchToHttp().getRequest<Request>();
		const authorization = req.headers.authorization;
		const bearer = this.service.tokenFromHeaders(authorization);
		if (bearer) {
			try {
				req.profile = await this.service.memberFromHeaders(bearer);
			}
			catch (ex) {
				req.profile = null;
			}
		}
		return true;
	}

}
