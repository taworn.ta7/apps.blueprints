import {
	Logger,
	Controller,
	Req,
	Query,
	Body,
	Get,
	Post,
} from '@nestjs/common';
import { Request } from 'express';
import { SharedService } from '../shared/shared.service';
import { MemberProfile } from './member_profile.entity';
import { MemberExternalSignUp } from './member_authenx.dto';
import { MemberAuthenXLineService } from './member_authenx_line.service';


/**
 * This class provides authentication with Line sign-in.
 */
@Controller(`/api/member/authenx`)
export class MemberAuthenXLineController {

	private readonly logger = new Logger(MemberAuthenXLineController.name);
	private readonly config = this.sharedService.config;

	constructor(
		private readonly sharedService: SharedService,
		private readonly service: MemberAuthenXLineService,
	) { }

	// ----------------------------------------------------------------------

	/**
	 * Line sign-in.
	 */
	@Get('line')
	async signInLine(
		@Req() req: Request,
		@Query('code') code: string,
		@Query('scope') scope: string,
		@Query('authuser') authuser: string,
		@Query('prompt') prompt: string,
	): Promise<{
		token: any,
		user: any,
		profile: MemberProfile,
	}> {
		// loads user information from code
		const token = await this.service.codeToToken(req, code, scope, authuser, prompt);
		const user = await this.service.tokenToUser(req, token.access_token, token.id_token);

		// checks our database
		const mail = user.email;
		let profile = await this.service.loadProfile(req, mail);
		if (!profile) {
			// no records, need confirmation to create new member
			return {
				token,
				user,
				profile: null,
			}
		}

		// checks mail
		if (profile.mail === mail) {
			// we found record with profile.mail
			if (!profile.line_mail) {
				// no Line mail, put it
				await this.service.updateMail(req, mail, profile);
			}
			else if (profile.line_mail === mail) {
				// same profile.mail and Line mail
				// no things to do
			}
			else {  // profile.line_mail !== mail
				// difference between profile.mail and Line mail
				profile = await this.service.loadProfileInOtherMail(req, mail);
			}
		}
		else {  // profile.mail !== mail || !profile.mail
			profile = await this.service.loadProfileInOtherMail(req, mail);
		}

		// finally, sign-in and returns
		await this.service.signIn(req, profile);
		return {
			token,
			user,
			profile,
		}
	}


	/**
	 * Confirms with Line sign-in.
	 */
	@Post('line')
	async signInLineConfirm(
		@Req() req: Request,
		@Body('signUp') dto: MemberExternalSignUp,
	): Promise<{
		user: any,
		profile: MemberProfile,
	}> {
		// loads user information from token
		const user = await this.service.tokenToUser(req, dto.access_token, dto.id_token);

		// checks our database
		const mail = user.email;
		let profile = await this.service.loadProfile(req, mail);
		if (!profile) {
			// no records, sign-up
			profile = await this.service.signUp(req, user);
			return {
				user,
				profile,
			};
		}

		// checks mail
		if (profile.mail === mail) {
			// we found record with profile.mail
			if (!profile.line_mail) {
				// no Line mail, put it
				profile.line_mail = mail;
				profile.line_signin = new Date();
			}
			else if (profile.line_mail === mail) {
				// same profile.mail and Line mail
				// no things to do
			}
			else {  // profile.line_mail !== mail
				// difference between profile.mail and Line mail
				profile = await this.service.loadProfileInOtherMail(req, mail);
			}
		}
		else {  // profile.mail !== mail || !profile.mail
			profile = await this.service.loadProfileInOtherMail(req, mail);
		}

		// finally, sign-in and returns
		await this.service.signIn(req, profile);
		return {
			user,
			profile,
		}
	}

}
