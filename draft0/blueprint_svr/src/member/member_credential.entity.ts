import {
	Entity,
	Column,
	PrimaryGeneratedColumn,
	CreateDateColumn,
	UpdateDateColumn,
	OneToOne,
	JoinColumn,
	Index,
} from 'typeorm';
import { inspect } from 'node:util';
import { MemberEntityConsts } from './member_entity_consts';
import { MemberProfile } from './member_profile.entity';


/**
 * Member's credential data.
 */
@Entity({
	name: 'member_credential',
})
export class MemberCredential {

	@PrimaryGeneratedColumn({
		type: 'bigint',
	})
	id: string;

	/**
	 * Link to [MemberProfile]{@link MemberProfile.html}.
	 */
	@OneToOne(() => MemberProfile, (o) => o.credential)
	@JoinColumn({
		name: 'profile_id',
	})
	profile: MemberProfile;

	// ----------------------------------------------------------------------

	// Password

	@Column({
		length: MemberEntityConsts.Salt,
	})
	salt: string;

	@Column({
		length: MemberEntityConsts.Hash,
	})
	hash: string;

	/**
	 * Sign-in token.
	 */
	@Index({
		unique: true,
	})
	@Column({
		length: MemberEntityConsts.Token,
		nullable: true,
	})
	token: string | null;

	/**
	 * Last sign-in date/time.
	 */
	@Column({
		nullable: true,
	})
	begin: Date | null;

	/**
	 * Date/time to update expiry time.
	 */
	@Column({
		nullable: true,
	})
	refresh: Date | null;

	/**
	 * Session expiry date/time.
	 */
	@Column({
		nullable: true,
	})
	expire: Date | null;

	/**
	 * Last sign-out date/time.
	 */
	@Column({
		nullable: true,
	})
	end: Date | null;

	// ----------------------------------------------------------------------

	@CreateDateColumn({})
	created: Date;

	@UpdateDateColumn({})
	updated: Date;

	constructor(o?: Partial<MemberCredential>) {
		Object.assign(this, o)
	}

	// ----------------------------------------------------------------------

	print() {
		if (!this.profile)
			return `[${this.id}]`;
		else
			return `[owner ${this.profile.id}/${this.profile.mail}, ${this.id}]`;
	}

	fullPrint(depth?: number) {
		return inspect(this, { depth });
	}

}
