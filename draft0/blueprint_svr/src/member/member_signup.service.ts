import {
	Logger,
	Injectable,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Request } from 'express';
import { setPassword, generateToken } from '../helpers/crypto_ex';
import { SharedService } from '../shared/shared.service';
import { MemberProfile } from './member_profile.entity';
import { MemberProfileExtra } from './member_profile_extra.entity';
import { MemberCredential } from './member_credential.entity';
import { MemberChangeProperty, MemberChangeAction } from './member_change_property.entity';
import { MemberSignUpDto } from './member_signup.dto';
import { MemberChangePropertyService } from './member_change_property.service';


/**
 * This class provides how to do sign-up process in business layer.
 */
@Injectable()
export class MemberSignUpService {

	private readonly logger = new Logger(MemberSignUpService.name);
	private readonly config = this.sharedService.config;

	constructor(
		@InjectRepository(MemberProfile)
		private readonly profileRepo: Repository<MemberProfile>,

		@InjectRepository(MemberChangeProperty)
		private readonly propertyRepo: Repository<MemberChangeProperty>,

		private readonly sharedService: SharedService,
		private readonly propertyService: MemberChangePropertyService,
	) { }

	// ----------------------------------------------------------------------

	/**
	 * Saves sign-up form into database.
	 *
	 * @param {MemberChangeProperty} dto  DTO contains sign-up form.
	 * @returns Returns new 'MemberChangeProperty' record.
	 */
	async signUp(req: Request, dto: MemberSignUpDto): Promise<MemberChangeProperty> {
		// checks this mail must be available to use
		await this.propertyService.checkMailAvailable(dto.mail);

		// creates sign-up
		const pass = setPassword(dto.password);
		const prop = new MemberChangeProperty();
		prop.action = MemberChangeAction.SignUp;
		prop.mail = dto.mail;
		prop.salt = pass.salt;
		prop.hash = pass.hash;
		prop.done = new Date(Date.now() + this.config.member.signUpExpire);
		prop.confirmToken = generateToken(64);
		await this.propertyRepo.save(prop);
		this.logger.log(`${req.id}; an applicant applied form: ${prop.fullPrint()}`);

		return prop;
	}


	/**
	 * Confirms the sign-up form, then, copies sign-up data into new 'MemberProfile' entity.
	 *
	 * @param {string} code  Code given from the confirmation mail.
	 * @returns The new 'MemberProfile' created.
	 */
	async confirm(req: Request, code: string): Promise<MemberProfile> {
		// checks code
		const prop = await this.propertyService.findCode(code, MemberChangeAction.SignUp);

		// again, checks this mail must be available to use
		await this.propertyService.checkMailAvailable(prop.mail);

		// creates member profile
		const profile = new MemberProfile();
		profile.mail = prop.mail;
		profile.extra = new MemberProfileExtra();
		profile.extra.settings = '{}';
		profile.credential = new MemberCredential();
		profile.credential.salt = prop.salt;
		profile.credential.hash = prop.hash;
		profile.credential.token = null;
		await this.profileRepo.save(profile);
		this.logger.log(`${req.id}; member created: ${profile.fullPrint()}`);

		// disables this record, so it cannot used again
		prop.done = new Date();
		await this.propertyRepo.save(prop);

		return profile;
	}

}
