import {
	Entity,
	Column,
	PrimaryGeneratedColumn,
	CreateDateColumn,
	UpdateDateColumn,
	Index,
} from 'typeorm';
import { inspect } from 'node:util';
import { MemberEntityConsts } from './member_entity_consts';


/**
 * What to change?
 */
export enum MemberChangeAction {
	SignUp = 'signup',
	ResetPassword = 'reset',
	ChangeMail = 'change_mail',
}


/**
 * Member's sign-up or changing entity.
 */
@Entity({
	name: 'member_change_property'
})
export class MemberChangeProperty {

	@PrimaryGeneratedColumn({
		type: 'bigint',
	})
	id: string;

	// ----------------------------------------------------------------------

	/**
	 * What to change?
	 */
	@Column({
		type: 'enum',
		enum: MemberChangeAction,
	})
	action: MemberChangeAction;

	/**
	 * Current mail address to using.
	 */
	@Column({
		length: MemberEntityConsts.Mail,
	})
	mail: string;

	/**
	 * New mail to change to.
	 */
	@Column({
		length: MemberEntityConsts.Mail,
		nullable: true,
		name: 'new_mail',
	})
	newMail: string | null;

	// ----------------------------------------------------------------------

	// Password

	@Column({
		length: MemberEntityConsts.Salt,
		nullable: true,
	})
	salt: string | null;

	@Column({
		length: MemberEntityConsts.Hash,
		nullable: true,
	})
	hash: string | null;

	// ----------------------------------------------------------------------

	/**
	 * This record are expired after this date.
	 */
	@Column({
		nullable: true,
	})
	done: Date | null;

	/**
	 * Confirmation token
	 */
	@Index({
		unique: true,
	})
	@Column({
		length: MemberEntityConsts.ConfirmToken,
	})
	confirmToken: string;

	// ----------------------------------------------------------------------

	@CreateDateColumn({})
	created: Date;

	@UpdateDateColumn({})
	updated: Date;

	constructor(o?: Partial<MemberChangeProperty>) {
		Object.assign(this, o)
	}

	// ----------------------------------------------------------------------

	print() {
		return `[${this.id}]`;
	}

	fullPrint(depth?: number) {
		return inspect(this, { depth });
	}

}
