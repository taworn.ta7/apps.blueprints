import {
	IsNotEmpty,
	Matches,
	MinLength,
	MaxLength,
} from 'class-validator';
import { IsEqualTo } from '../decorators/IsEqualTo';
import { regexMail } from '../helpers/string_ex';
import { MemberEntityConsts } from './member_entity_consts';


export class MemberSignUpDto {
	@IsNotEmpty()
	//@IsEmail()
	@Matches(regexMail)
	@MinLength(5)
	@MaxLength(MemberEntityConsts.Mail)
	mail: string;

	@IsNotEmpty()
	@MinLength(MemberEntityConsts.PasswordMin)
	@MaxLength(MemberEntityConsts.PasswordMax)
	password: string;

	@IsNotEmpty()
	@IsEqualTo('password')
	confirmPassword: string;
}
