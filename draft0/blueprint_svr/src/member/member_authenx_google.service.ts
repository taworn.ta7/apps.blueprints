import {
	Logger,
	Injectable,
	UnauthorizedException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Request } from 'express';
import { AxiosService } from '../shared/axios.service';
import { SharedService } from '../shared/shared.service';
import { MemberProfile } from './member_profile.entity';
import { MemberProfileExtra } from './member_profile_extra.entity';
import { MemberCredential } from './member_credential.entity';
import { MemberService } from './member.service';
import { MemberStockIconsService } from './member_stock_icons.service';


/**
 * This class connects to Google service(s) and retrives user information.
 */
@Injectable()
export class MemberAuthenXGoogleService {

	private readonly logger = new Logger(MemberAuthenXGoogleService.name);
	private readonly config = this.sharedService.config;

	constructor(
		@InjectRepository(MemberProfile)
		private readonly profileRepo: Repository<MemberProfile>,

		@InjectRepository(MemberCredential)
		private readonly credentialRepo: Repository<MemberCredential>,

		private readonly axiosService: AxiosService,
		private readonly sharedService: SharedService,
		private readonly iconService: MemberStockIconsService,
		private readonly memberService: MemberService,
	) { }

	// ----------------------------------------------------------------------

	async codeToToken(
		req: Request,
		code: string,
		scope: string,
		authuser: string,
		prompt: string,
	): Promise<any> {
		const result = await this.axiosService.call(`https://oauth2.googleapis.com/token`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
			},
			data: {
				code,
				client_id: this.config.google.clientId,
				client_secret: this.config.google.clientSecret,
				redirect_uri: this.config.google.redirectUrl,
				grant_type: 'authorization_code',
			},
		});
		if (result.status !== 200) {
			this.logger.debug(`${req.id}; ${result.status}`);
			throw new UnauthorizedException();
		}
		const token = result.json;
		if (!token.access_token || !token.id_token) {
			throw new UnauthorizedException();
		}
		return token;
	}


	async tokenToUser(
		req: Request,
		access_token: string,
		id_token: string,
	): Promise<any> {
		const result = await this.axiosService.call(`https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=${access_token}`, {
			method: 'GET',
			headers: {
				'Authorization': `Bearer ${id_token}`,
			},
		});
		if (result.status !== 200) {
			this.logger.debug(`${req.id}; ${result.status}`);
			throw new UnauthorizedException();
		}
		const user = await result.json;
		if (!user.email || !user.name) {
			throw new UnauthorizedException();
		}
		return user;
	}


	async loadProfile(
		req: Request,
		mail: string,
	): Promise<MemberProfile> {
		const profile = await this.profileRepo.findOne({
			where: [
				{ mail },
				{ google_mail: mail },
			],
			relations: ['credential', 'extra'],
		});
		if (!profile)
			return null;
		this.memberService.checkProfileCanSignIn(profile);
		return profile;
	}


	async loadProfileInOtherMail(
		req: Request,
		mail: string,
	): Promise<MemberProfile> {
		const profile = await this.profileRepo.findOne({
			where: { google_mail: mail },
			relations: ['credential', 'extra'],
		});
		if (!profile)
			return null;
		this.memberService.checkProfileCanSignIn(profile);
		return profile;
	}


	async updateMail(
		req: Request,
		mail: string,
		profile: MemberProfile,
	): Promise<void> {
		profile.google_mail = mail;
		profile.google_signin = new Date();
		await this.profileRepo.save(profile);
	}


	async signIn(
		req: Request,
		profile: MemberProfile,
	): Promise<MemberProfile> {
		// profile sign-in
		await this.memberService.profileSignIn(profile);

		// success
		this.logger.log(`${req.id}; Google sign-in: ${profile.fullPrint()}`);
		return profile;
	}


	async signUp(
		req: Request,
		user: any,
	): Promise<MemberProfile> {
		// loads profile picture
		let mime: string = null;
		let icon: Buffer = null;
		if (user.picture) {
			const result = await this.axiosService.call(user.picture, {
				responseType: 'arraybuffer',
			});
			mime = result.res.headers['content-type'];
			icon = result.res.data;
		}
		if (icon === null) {
			const mimeicon = await this.iconService.get(user.email);
			mime = mimeicon.mime;
			icon = mimeicon.icon;
		}

		// creates member profile
		const profile = new MemberProfile();
		profile.mail = user.email;
		profile.extra = new MemberProfileExtra();
		profile.extra.mime = mime;
		profile.extra.icon = icon;
		profile.extra.settings = '{}';
		profile.credential = new MemberCredential();
		profile.credential.salt = '';
		profile.credential.hash = '';
		profile.credential.token = null;
		await this.profileRepo.save(profile);
		this.logger.log(`${req.id}; Google created: ${profile.fullPrint()}`);

		// profile sign-in
		await this.memberService.profileSignIn(profile);

		// success
		this.logger.log(`${req.id}; Google sign-in: ${profile.fullPrint()}`);
		return profile;
	}

}
