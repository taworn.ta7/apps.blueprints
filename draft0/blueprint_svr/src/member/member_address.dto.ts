import {
	IsNotEmpty,
	IsInt,
	MinLength,
	MaxLength,
} from 'class-validator';
import { PrecinctEntityConsts } from '../precinct/precinct_entity_consts';
import { MemberEntityConsts } from './member_entity_consts';


export class MemberAddressDto {
	@IsNotEmpty()
	@MaxLength(MemberEntityConsts.Abode)
	abode: string;

	@IsNotEmpty()
	@IsInt()
	canton: number;

	@IsNotEmpty()
	@IsInt()
	district: number;

	@IsNotEmpty()
	@IsInt()
	province: number;

	@IsNotEmpty()
	@MinLength(PrecinctEntityConsts.Zip)
	@MaxLength(PrecinctEntityConsts.Zip)
	zip: string;
}
