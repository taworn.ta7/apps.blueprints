import {
	IsNotEmpty,
} from 'class-validator';


export class MemberExternalSignUp {
	@IsNotEmpty()
	access_token: string;

	@IsNotEmpty()
	id_token: string;
}
