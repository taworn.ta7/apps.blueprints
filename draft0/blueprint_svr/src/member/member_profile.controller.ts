import {
	Logger,
	Controller,
	Req,
	Res,
	Body,
	Get,
	Put,
	Post,
	Delete,
	Query,
	UploadedFile,
	UseGuards,
	UseInterceptors,
	ValidationPipe,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { MailerService } from '@nestjs-modules/mailer';
import { Express, Request, Response } from 'express';
import { inspect } from 'util';
import { convertBytes } from '../helpers/convert';
import { Errors } from '../helpers/errors';
import { LoggingAction } from '../shared/logging.entity';
import { SharedService } from '../shared/shared.service';
import { MemberProfile } from './member_profile.entity';
import { MemberChangeProperty } from './member_change_property.entity';
import { MemberChangeMailDto, MemberChangePasswordDto, MemberResignDto } from './member_profile.dto';
import { MemberSettingsDto } from './member_settings.dto';
import { MemberProfileService } from './member_profile.service';
import { MemberAuthenGuard } from './member_authen.guard';


/**
 * This class provides profile editing in application layer.
 */
@Controller(`/api/member/profile`)
export class MemberProfileController {

	private readonly logger = new Logger(MemberProfileController.name);
	private readonly config = this.sharedService.config;

	constructor(
		private readonly mailerService: MailerService,
		private readonly sharedService: SharedService,
		private readonly service: MemberProfileService,
	) { }

	// ----------------------------------------------------------------------

	/////////////////////////////////////////////////////////////////////////
	// Retrives Full Profile
	/////////////////////////////////////////////////////////////////////////

	@Get('me')
	@UseGuards(MemberAuthenGuard)
	async profile(
		@Req() req: Request,
	): Promise<{
		profile: MemberProfile,
	}> {
		return {
			profile: await this.service.profile(req),
		}
	}


	/////////////////////////////////////////////////////////////////////////
	// Change Mail
	/////////////////////////////////////////////////////////////////////////

	@Post('mail')
	@UseGuards(MemberAuthenGuard)
	async requestChangeMail(
		@Req() req: Request,
		@Body('mail') dto: MemberChangeMailDto,
	): Promise<{
		change: MemberChangeProperty,
		url: string,
	}> {
		// creates change mail record
		const change = await this.service.requestChangeMail(req, req.profile.mail, dto);

		// creates URL
		//const url = `${req.protocol}://${req.get('host')}/api/member/profile/mail?code=${change.confirmToken}`;
		const url = `${this.config.client.host}/mail_shells/member_change_mail?code=${change.confirmToken}`;

		// sends mail
		await this.mailerService.sendMail({
			to: change.newMail,
			subject: `Change Mail Confirmation`,
			template: 'member_change_mail',
			context: {
				oldMail: req.profile.mail,
				newMail: change.newMail,
				token: change.confirmToken,
				url,
			},
		});

		return { change, url }
	}


	@Get('mail')
	async confirmChangeMail(
		@Req() req: Request,
		@Query('code') code: string,
	): Promise<{
		profile: MemberProfile,
	}> {
		// updates mail
		const profile = await this.service.confirmChangeMail(req, code);

		// adds log
		await this.sharedService.print(
			`member ${profile.print()} is updated mail!`, {
			action: LoggingAction.Update,
			table: 'member_profile',
			requestId: req.id,
			profileId: profile.id,
		});

		return { profile };
	}

	// ----------------------------------------------------------------------

	/////////////////////////////////////////////////////////////////////////
	// Change Password
	/////////////////////////////////////////////////////////////////////////

	@Put('password')
	@UseGuards(MemberAuthenGuard)
	async changePassword(
		@Req() req: Request,
		@Body('password') dto: MemberChangePasswordDto,
	): Promise<{
		profile: MemberProfile,
	}> {
		// updates password
		const profile = await this.service.changePassword(req, req.profile.mail, dto);

		// adds log
		await this.sharedService.print(
			`member ${profile.print()} is updated password!`, {
			action: LoggingAction.Update,
			table: 'member_credential',
			requestId: req.id,
			profileId: profile.id,
		});

		return { profile };
	}

	// ----------------------------------------------------------------------

	/////////////////////////////////////////////////////////////////////////
	// Change Settings
	/////////////////////////////////////////////////////////////////////////

	@Put('settings')
	@UseGuards(MemberAuthenGuard)
	async changeSettings(
		@Req() req: Request,
		@Body('settings', new ValidationPipe({ whitelist: true })) dto: MemberSettingsDto,
	): Promise<{
		profile: MemberProfile,
	}> {
		// updates password
		const profile = await this.service.changeSettings(req, req.profile.id, dto);

		// adds log
		await this.sharedService.print(
			`member ${profile.print()} is updated settings: ${profile.extra.settings}`, {
			action: LoggingAction.Update,
			table: 'member_profile_extra',
			requestId: req.id,
			profileId: profile.id,
		});

		return { profile };
	}

	// ----------------------------------------------------------------------

	/////////////////////////////////////////////////////////////////////////
	// Profile Icons
	/////////////////////////////////////////////////////////////////////////

	@Post('icon')
	@UseGuards(MemberAuthenGuard)
	@UseInterceptors(FileInterceptor('icon'))
	async changeIcon(
		@Req() req: Request,
		@Res() res: Response,
		@UploadedFile() icon: Express.Multer.File,
	): Promise<any> {
		// checks uploaded image
		this.logger.debug(`${req.id}; icon: ${inspect(icon)}`);
		if (!icon)
			throw Errors.E400UploadFail();
		if (icon.mimetype !== 'image/png' && icon.mimetype !== 'image/jpeg' && icon.mimetype !== 'image/gif')
			throw Errors.E400UploadIsNotTypeImage();
		const limit = this.config.member.iconFileLimit;
		if (icon.size >= limit)
			throw Errors.E400UploadIsTooBig(convertBytes(limit));

		// puts file into database
		const profile = await this.service.changeIcon(req, req.profile.id, icon);

		// puts icon to output
		res.writeHead(200, { 'Content-Type': profile.extra.mime });
		res.end(profile.extra.icon);
	}


	@Get('icon')
	@UseGuards(MemberAuthenGuard)
	async viewIcon(
		@Req() req: Request,
		@Res() res: Response,
	): Promise<any> {
		const { mime, icon } = await this.service.getIcon(req, req.profile.id);
		res.writeHead(200, { 'Content-Type': mime });
		res.end(icon);
	}


	@Delete('icon')
	@UseGuards(MemberAuthenGuard)
	async removeIcon(
		@Req() req: Request,
		@Res() res: Response,
	): Promise<any> {
		await this.service.changeIcon(req, req.profile.id, null);
		const { mime, icon } = await this.service.getIcon(req, req.profile.id);
		res.writeHead(200, { 'Content-Type': mime });
		res.end(icon);
	}

	// ----------------------------------------------------------------------

	/////////////////////////////////////////////////////////////////////////
	// Resignation
	/////////////////////////////////////////////////////////////////////////

	@Put('resign')
	@UseGuards(MemberAuthenGuard)
	async resign(
		@Req() req: Request,
		@Body('resign') dto: MemberResignDto,
	): Promise<{
		profile: MemberProfile,
	}> {
		// updates resigned flag
		const profile = await this.service.resign(req, req.profile.mail, dto);

		// adds log
		await this.sharedService.print(
			`member ${profile.print()} is resigned!`, {
			action: LoggingAction.Update,
			table: 'member_profile',
			requestId: req.id,
			profileId: profile.id,
		});

		return { profile };
	}
}
