import {
	Entity,
	Column,
	PrimaryGeneratedColumn,
	CreateDateColumn,
	UpdateDateColumn,
	ManyToOne,
	JoinColumn,
} from 'typeorm';
import { inspect } from 'node:util';
import { PrecinctEntityConsts } from '../precinct/precinct_entity_consts';
import { PrecinctCanton } from '../precinct/precinct_canton.entity';
import { PrecinctDistrict } from '../precinct/precinct_district.entity';
import { PrecinctProvince } from '../precinct/precinct_province.entity';
import { MemberEntityConsts } from './member_entity_consts';
import { MemberProfile } from './member_profile.entity';


/**
 * Member's address.
 */
@Entity({
	name: 'member_address',
})
export class MemberAddress {

	@PrimaryGeneratedColumn({
		type: 'bigint',
	})
	id: string;

	/**
	 * Link to [MemberProfile]{@link MemberProfile.html}.
	 */
	@ManyToOne(() => MemberProfile)
	@JoinColumn({
		name: 'profile_id',
	})
	profile: MemberProfile;

	// ----------------------------------------------------------------------

	/**
	 * Abode
	 */
	@Column({
		length: MemberEntityConsts.Abode,
		name: 'abode',
	})
	abode: string;

	/**
	 * Links to [PrecinctCanton]{@link PrecinctCanton.html}.
	 */
	@ManyToOne(() => PrecinctCanton, {
	})
	@JoinColumn({
		name: 'canton_id',
	})
	canton: PrecinctCanton;

	/**
	 * Links to [PrecinctDistrict]{@link PrecinctDistrict.html}.
	 */
	@ManyToOne(() => PrecinctDistrict, {
	})
	@JoinColumn({
		name: 'district_id',
	})
	district: PrecinctDistrict;

	/**
	 * Links to [PrecinctProvince]{@link PrecinctProvince.html}.
	 */
	@ManyToOne(() => PrecinctProvince, {
	})
	@JoinColumn({
		name: 'province_id',
	})
	province: PrecinctProvince;

	/**
	 * ZIP
	 */
	@Column({
		length: PrecinctEntityConsts.Zip,
		name: 'zip',
	})
	zip: string;

	// ----------------------------------------------------------------------

	@CreateDateColumn({})
	created: Date;

	@UpdateDateColumn({})
	updated: Date;

	constructor(o?: Partial<MemberAddress>) {
		Object.assign(this, o)
	}

	// ----------------------------------------------------------------------

	print() {
		if (!this.profile)
			return `[${this.id}]`;
		else
			return `[owner ${this.profile.id}/${this.profile.mail}, ${this.id}]`;
	}

	fullPrint(depth?: number) {
		return inspect(this, { depth });
	}

	morePrint() {
		let out = {
			id: this.print(),
			abode: this.abode,
			canton: this.canton?.print(),
			district: this.district?.print(),
			province: this.province?.print(),
			zip: this.zip,
		}
		return inspect(out);
	}

}
