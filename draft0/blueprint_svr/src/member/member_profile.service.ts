import {
	Logger,
	Injectable,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Request } from 'express';
import { setPassword, generateToken } from '../helpers/crypto_ex';
import { SharedService } from '../shared/shared.service';
import { MemberProfile } from './member_profile.entity';
import { MemberProfileExtra } from './member_profile_extra.entity';
import { MemberCredential } from './member_credential.entity';
import { MemberChangeProperty, MemberChangeAction } from './member_change_property.entity';
import { MemberChangeMailDto, MemberChangePasswordDto, MemberResignDto } from './member_profile.dto';
import { MemberSettingsDto } from './member_settings.dto';
import { MemberService } from './member.service';
import { MemberStockIconsService } from './member_stock_icons.service';
import { MemberChangePropertyService } from './member_change_property.service';


/**
 * This class provides profile editing in business layer.
 */
@Injectable()
export class MemberProfileService {

	private readonly logger = new Logger(MemberProfileService.name);
	private readonly config = this.sharedService.config;

	constructor(
		@InjectRepository(MemberProfile)
		private readonly profileRepo: Repository<MemberProfile>,

		@InjectRepository(MemberProfileExtra)
		private readonly extraRepo: Repository<MemberProfileExtra>,

		@InjectRepository(MemberCredential)
		private readonly credentialRepo: Repository<MemberCredential>,

		@InjectRepository(MemberChangeProperty)
		private readonly propertyRepo: Repository<MemberChangeProperty>,

		private readonly sharedService: SharedService,
		private readonly iconService: MemberStockIconsService,
		private readonly memberService: MemberService,
		private readonly propertyService: MemberChangePropertyService,
	) { }

	// ----------------------------------------------------------------------

	/////////////////////////////////////////////////////////////////////////
	// Retrives Full Profile
	/////////////////////////////////////////////////////////////////////////

	async profile(req: Request): Promise<MemberProfile> {
		const profile = await this.memberService.findById(req.profile?.id, true);
		this.memberService.checkProfileCanSignIn(profile);
		this.logger.log(`${req.id}; member sign-in token: ${profile.print()}`);
		return profile;
	}


	/////////////////////////////////////////////////////////////////////////
	// Change Mail
	/////////////////////////////////////////////////////////////////////////

	/**
	 * Saves change mail form into database.
	 * 
	 * @param {MemberChangeProperty} dto  DTO contains change mail form.
	 * @returns Returns new 'MemberChangeProperty' record.
	 */
	async requestChangeMail(req: Request, mail: string, dto: MemberChangeMailDto): Promise<MemberChangeProperty> {
		// checks this mail must be available to use
		await this.propertyService.checkMailAvailable(dto.newMail);

		// checks mail along with password both must be correct
		const profile = await this.propertyService.checkPasswordAndLoadProfile(req, mail, dto.password);

		// creates change mail
		const prop = new MemberChangeProperty();
		prop.action = MemberChangeAction.ChangeMail;
		prop.mail = profile.mail;
		prop.newMail = dto.newMail;
		prop.done = new Date(Date.now() + this.config.member.changeMailExpire);
		prop.confirmToken = generateToken(64);
		await this.propertyRepo.save(prop);
		this.logger.log(`${req.id}; member ${profile.print()} request to change mail: ${prop.fullPrint()}`);

		return prop;
	}


	/**
	 * Confirms the change mail form.  This will set profile mail to the new one.
	 * 
	 * @param {string} code  Code given from the confirmation mail.
	 * @returns The 'MemberProfile' which updated new mail.
	 */
	async confirmChangeMail(req: Request, code: string): Promise<MemberProfile> {
		// checks code
		const prop = await this.propertyService.findCode(code, MemberChangeAction.ChangeMail);

		// again, checks this mail must be available to use
		await this.propertyService.checkMailAvailable(prop.newMail);

		// updates mail in profile
		const profile = await this.propertyService.loadProfile(req, prop.mail);
		profile.mail = prop.newMail;
		profile.credential.token = null;
		await this.profileRepo.save(profile);
		this.logger.log(`${req.id}; member ${profile.print()} is updated mail!`);

		// disables this record, so it cannot used again
		prop.done = new Date();
		await this.propertyRepo.save(prop);

		return profile;
	}


	/////////////////////////////////////////////////////////////////////////
	// Change Password
	/////////////////////////////////////////////////////////////////////////

	/**
	 * This function updates password.
	 * 
	 * @returns The 'MemberProfile' which updated new password.
	 */
	async changePassword(req: Request, mail: string, dto: MemberChangePasswordDto): Promise<MemberProfile> {
		// updates password
		const profile = await this.propertyService.checkPasswordAndLoadProfile(req, mail, dto.currentPassword);
		const pass = setPassword(dto.newPassword);
		profile.credential.salt = pass.salt;
		profile.credential.hash = pass.hash;
		profile.credential.token = null;
		await this.credentialRepo.save(profile.credential);
		this.logger.log(`${req.id}; member ${profile.print()} is updated password!`);

		return profile;
	}


	/////////////////////////////////////////////////////////////////////////
	// Change Settings
	/////////////////////////////////////////////////////////////////////////

	/**
	 * This function saves settings, in JSON, into database.
	 * The DTO is in 'member_settings.dto'.
	 * 
	 * @returns The 'MemberProfile' which updated settings.
	 */
	async changeSettings(req: Request, id: string, dto: MemberSettingsDto): Promise<MemberProfile> {
		// loads profile with extra
		const profile = await this.memberService.findById(id, true);

		// saves and commit
		profile.extra.settings = JSON.stringify(dto);
		await this.extraRepo.save(profile.extra);
		this.logger.log(`${req.id}; member ${profile.print()} is updated settings: ${profile.extra.settings}`);

		return profile;
	}


	/////////////////////////////////////////////////////////////////////////
	// Profile Icons
	/////////////////////////////////////////////////////////////////////////

	/**
	 * This function saves icon with MIME into database.
	 * 
	 * @returns The 'MemberProfile' which updated icon.
	 */
	async changeIcon(req: Request, id: string, icon: Express.Multer.File | null): Promise<MemberProfile> {
		// loads profile with extra
		const profile = await this.memberService.findById(id, true);

		// saves and commit
		const extra = profile.extra;
		if (icon) {
			extra.mime = icon.mimetype;
			extra.icon = icon.buffer;
			this.logger.verbose(`${req.id}; member ${profile.print()} is uploaded icon`);
		}
		else {
			extra.mime = null;
			extra.icon = null;
			this.logger.verbose(`${req.id}; member ${profile.print()} is set to default icon`);
		}
		await this.extraRepo.save(extra);

		return profile;
	}


	/**
	 * This function returns profile icon, or default icon if they not set.
	 * 
	 * @returns Returns icon with MIME type.
	 */
	async getIcon(req: Request, id: string): Promise<{ mime: string, icon: Buffer }> {
		// loads profile with extra
		const profile = await this.memberService.findById(id, true);

		// retrives from profile, if it uploaded
		if (profile.extra.icon)
			return { mime: profile.extra.mime, icon: profile.extra.icon };

		// retrives from profile icon stock
		return await this.iconService.get(profile.mail);
	}


	/////////////////////////////////////////////////////////////////////////
	// Resignation
	/////////////////////////////////////////////////////////////////////////

	/**
	 * This function resign current member.
	 * 
	 * @returns The 'MemberProfile' which flag 'resigned' is set.
	 */
	async resign(req: Request, mail: string, dto: MemberResignDto): Promise<MemberProfile> {
		// updates resigned flag
		const profile = await this.propertyService.checkPasswordAndLoadProfile(req, mail, dto.currentPassword);
		profile.resigned = new Date();
		profile.credential.token = null;
		await this.profileRepo.save(profile);
		this.logger.log(`${req.id}; member ${profile.print()} is resigned!`);

		return profile;
	}

}
