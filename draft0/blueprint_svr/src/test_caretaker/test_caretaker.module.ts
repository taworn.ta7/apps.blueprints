import { Module } from '@nestjs/common';
import { SharedService } from '../shared/shared.service';
import { SharedModule } from '../shared/shared.module';
import { PrecinctService } from '../precinct/precinct.service';
import { PrecinctModule } from '../precinct/precinct.module';
import { CaretakerService } from '../caretaker/caretaker.service';
import { CaretakerChangePropertyService } from '../caretaker/caretaker_change_property.service';
import { CaretakerStockIconsService } from '../caretaker/caretaker_stock_icons.service';
import { CaretakerProfileService } from '../caretaker/caretaker_profile.service';
import { CaretakerAssignService } from '../caretaker/caretaker_assign.service';
import { CaretakerResetService } from '../caretaker/caretaker_reset.service';
import { CaretakerAuthenService } from '../caretaker/caretaker_authen.service';
import { CaretakerAddressService } from '../caretaker/caretaker_address.service';
import { CaretakerModule } from '../caretaker/caretaker.module';
import { TestCaretakerController } from './test_caretaker.controller';


@Module({
	imports: [
		SharedModule,
		PrecinctModule,
		CaretakerModule,
	],
	providers: [
		SharedService,
		PrecinctService,
		CaretakerService,
		CaretakerChangePropertyService,
		CaretakerStockIconsService,
		CaretakerProfileService,
		CaretakerAssignService,
		CaretakerResetService,
		CaretakerAuthenService,
		CaretakerAddressService,
	],
	controllers: [
		TestCaretakerController,
	],
})
export class TestCaretakerModule { }
