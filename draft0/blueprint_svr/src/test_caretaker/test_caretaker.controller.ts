import {
	Logger,
	Controller,
	Req,
	Res,
	Get,
	Param,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { regexMail } from '../helpers/string_ex';
import { Pagination, queryPagination } from '../helpers/pagination';
import { SharedService } from '../shared/shared.service';
import { CaretakerProfile } from '../caretaker/caretaker_profile.entity';
import { CaretakerService } from '../caretaker/caretaker.service';
import { CaretakerStockIconsService } from '../caretaker/caretaker_stock_icons.service';


@Controller(`/api/caretaker`)
export class TestCaretakerController {

	private readonly logger = new Logger(TestCaretakerController.name);
	private readonly config = this.sharedService.config;

	constructor(
		private readonly sharedService: SharedService,
		private readonly iconService: CaretakerStockIconsService,
		private readonly caretakerService: CaretakerService,
	) { }

	// ----------------------------------------------------------------------

	@Get('find/:idOrMail')
	async findOne(
		@Req() req: Request,
		@Param('idOrMail') idOrMail: string,
	): Promise<{
		profile: CaretakerProfile,
	}> {
		let profile: CaretakerProfile;
		if (regexMail.test(idOrMail))
			profile = await this.caretakerService.findByMail(idOrMail);
		else
			profile = await this.caretakerService.findById(idOrMail);
		return { profile };
	}


	@Get('find/icon/:idOrMail')
	async findIcon(
		@Req() req: Request,
		@Res() res: Response,
		@Param('idOrMail') idOrMail: string,
	): Promise<any> {
		let profile: CaretakerProfile;
		if (regexMail.test(idOrMail))
			profile = await this.caretakerService.findByMail(idOrMail, true);
		else
			profile = await this.caretakerService.findById(idOrMail, true);
		if (profile.extra.icon) {
			res.writeHead(200, { 'Content-Type': profile.extra.mime });
			res.end(profile.extra.icon);
		}
		else {
			const { mime, icon } = await this.iconService.get(profile.mail);
			res.writeHead(200, { 'Content-Type': mime });
			res.end(icon);
		}
	}


	@Get('find')
	async find(
		@Req() req: Request,
	): Promise<{
		profiles: CaretakerProfile[],
		pagination: Pagination,
	}> {
		const options = queryPagination(req, 10, {
			mail: 'mail',
			created: 'created',
			updated: 'updated',
		});
		return await this.caretakerService.find(req, options);
	}

	// ----------------------------------------------------------------------

	@Get('icons/:name')
	async getProfileIcon(
		@Req() req: Request,
		@Res() res: Response,
		@Param('name') name: string,
	): Promise<any> {
		const { mime, icon } = await this.iconService.get(name);
		res.writeHead(200, { 'Content-Type': mime });
		res.end(icon);
	}

}
