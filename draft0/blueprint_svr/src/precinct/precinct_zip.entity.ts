import {
	Entity,
	Column,
	PrimaryGeneratedColumn,
	CreateDateColumn,
	UpdateDateColumn,
	ManyToOne,
	JoinColumn,
	Index,
} from 'typeorm';
import { inspect } from 'node:util';
import { PrecinctEntityConsts } from './precinct_entity_consts';
import { PrecinctCanton } from './precinct_canton.entity';


/**
 * ZIP code (รหัสไปรษณีย์) model.
 */
@Entity({
	name: 'precinct_zip',
})
export class PrecinctZip {

	@PrimaryGeneratedColumn({
		type: 'int',
	})
	id: number;

	// ----------------------------------------------------------------------

	/**
	 * ZIP code.
	 */
	@Index()
	@Column({
		length: PrecinctEntityConsts.Zip,
		name: 'zip',
	})
	zip: string;

	// ----------------------------------------------------------------------

	/**
	 * Links back to [PrecinctCanton]{@link PrecinctCanton.html}.
	 */
	@Index()
	@ManyToOne(() => PrecinctCanton, (inverse) => inverse.zipList, {
		//cascade: true,
		onDelete: 'CASCADE',
	})
	@JoinColumn({
		name: 'canton_id',
	})
	canton: PrecinctCanton;

	// ----------------------------------------------------------------------

	@CreateDateColumn({})
	created: Date;

	@UpdateDateColumn({})
	updated: Date;

	constructor(o?: Partial<PrecinctZip>) {
		Object.assign(this, o)
	}

	// ----------------------------------------------------------------------

	print() {
		return `[${this.id + '/' + this.zip}]`;
	}

	fullPrint(depth?: number) {
		return inspect(this, { depth });
	}

}
