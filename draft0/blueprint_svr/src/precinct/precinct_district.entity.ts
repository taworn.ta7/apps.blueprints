import {
	Entity,
	Column,
	PrimaryColumn,
	CreateDateColumn,
	UpdateDateColumn,
	OneToMany,
	ManyToOne,
	JoinColumn,
	Index,
} from 'typeorm';
import { inspect } from 'node:util';
import { PrecinctEntityConsts } from './precinct_entity_consts';
import { PrecinctProvince } from './precinct_province.entity';
import { PrecinctCanton } from './precinct_canton.entity';


/**
 * District (เขต/อำเภอ) model.
 */
@Entity({
	name: 'precinct_district',
})
export class PrecinctDistrict {

	@PrimaryColumn({
		type: 'int',
	})
	id: number;

	// ----------------------------------------------------------------------

	/**
	 * District name English.
	 */
	@Index()
	@Column({
		length: PrecinctEntityConsts.NameEn,
		name: 'name_en',
	})
	nameEn: string;

	/**
	 * District name Thai.
	 */
	@Index()
	@Column({
		length: PrecinctEntityConsts.NameTh,
		name: 'name_th',
	})
	nameTh: string;

	// ----------------------------------------------------------------------

	/**
	 * Links back to [PrecinctProvince]{@link PrecinctProvince.html}.
	 */
	@Index()
	@ManyToOne(() => PrecinctProvince, (inverse) => inverse.districtList, {
		//cascade: true,
		onDelete: 'CASCADE',
	})
	@JoinColumn({
		name: 'province_id',
	})
	province: PrecinctProvince;

	/**
	 * Links to [PrecinctCanton]{@link PrecinctCanton.html}.
	 */
	@OneToMany(() => PrecinctCanton, (inverse) => inverse.district)
	cantonList: PrecinctCanton[];

	// ----------------------------------------------------------------------

	@CreateDateColumn({})
	created: Date;

	@UpdateDateColumn({})
	updated: Date;

	constructor(o?: Partial<PrecinctDistrict>) {
		Object.assign(this, o)
	}

	// ----------------------------------------------------------------------

	print() {
		return `[${this.id + '/' + this.nameEn}]`;
	}

	fullPrint(depth?: number) {
		return inspect(this, { depth });
	}

}
