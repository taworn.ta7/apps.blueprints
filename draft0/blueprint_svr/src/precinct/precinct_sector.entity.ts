import {
	Entity,
	Column,
	PrimaryColumn,
	CreateDateColumn,
	UpdateDateColumn,
	OneToMany,
} from 'typeorm';
import { inspect } from 'node:util';
import { PrecinctEntityConsts } from './precinct_entity_consts';
import { PrecinctProvince } from './precinct_province.entity';


/**
 * Sector (ภาค) model.
 */
@Entity({
	name: 'precinct_sector',
})
export class PrecinctSector {

	@PrimaryColumn({
		type: 'int',
	})
	id: number;

	// ----------------------------------------------------------------------

	/**
	 * Sector name English.
	 */
	@Column({
		length: PrecinctEntityConsts.NameEn,
		name: 'name_en',
	})
	nameEn: string;

	/**
	 * Sector name Thai.
	 */
	@Column({
		length: PrecinctEntityConsts.NameTh,
		name: 'name_th',
	})
	nameTh: string;

	// ----------------------------------------------------------------------

	/**
	 * Links to [PrecinctProvince]{@link PrecinctProvince.html}.
	 */
	@OneToMany(() => PrecinctProvince, (inverse) => inverse.sector)
	provinceList: PrecinctProvince[];

	// ----------------------------------------------------------------------

	@CreateDateColumn({})
	created: Date;

	@UpdateDateColumn({})
	updated: Date;

	constructor(o?: Partial<PrecinctSector>) {
		Object.assign(this, o)
	}

	// ----------------------------------------------------------------------

	print() {
		return `[${this.id + '/' + this.nameEn}]`;
	}

	fullPrint(depth?: number) {
		return inspect(this, { depth });
	}

}
