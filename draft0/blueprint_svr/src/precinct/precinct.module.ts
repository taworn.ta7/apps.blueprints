import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PrecinctCanton } from './precinct_canton.entity';
import { PrecinctDistrict } from './precinct_district.entity';
import { PrecinctProvince } from './precinct_province.entity';
import { PrecinctSector } from './precinct_sector.entity';
import { PrecinctZip } from './precinct_zip.entity';
import { PrecinctService } from './precinct.service';


@Module({
	imports: [
		TypeOrmModule.forFeature([
			PrecinctCanton,
			PrecinctDistrict,
			PrecinctProvince,
			PrecinctSector,
			PrecinctZip,
		]),
	],
	exports: [
		TypeOrmModule,
	],
	providers: [
		PrecinctService,
	],
	controllers: [
	],
})
export class PrecinctModule { }
