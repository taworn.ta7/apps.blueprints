import {
	Logger,
	Injectable,
} from '@nestjs/common';
import { InjectDataSource, InjectRepository } from '@nestjs/typeorm';
import { DataSource, Repository } from 'typeorm';
import { Errors } from '../helpers/errors';
import { PrecinctCanton } from './precinct_canton.entity';
import { PrecinctDistrict } from './precinct_district.entity';
import { PrecinctProvince } from './precinct_province.entity';
import { PrecinctSector } from './precinct_sector.entity';
import { PrecinctZip } from './precinct_zip.entity';


/**
 * This class provides about cantons, districts, provinces, sectors and ZIP.
 */
@Injectable()
export class PrecinctService {

	private readonly logger = new Logger(PrecinctService.name);

	constructor(
		@InjectDataSource()
		private readonly dataSource: DataSource,

		@InjectRepository(PrecinctCanton)
		private readonly cantonRepo: Repository<PrecinctCanton>,

		@InjectRepository(PrecinctDistrict)
		private readonly districtRepo: Repository<PrecinctDistrict>,

		@InjectRepository(PrecinctProvince)
		private readonly provinceRepo: Repository<PrecinctProvince>,

		@InjectRepository(PrecinctSector)
		private readonly sectorRepo: Repository<PrecinctSector>,

		@InjectRepository(PrecinctZip)
		private readonly zipRepo: Repository<PrecinctZip>,
	) { }

	// ----------------------------------------------------------------------

	/////////////////////////////////////////////////////////////////////////
	// CANTON
	/////////////////////////////////////////////////////////////////////////

	/**
	 * Gets a canton data from id.
	 * 
	 * @param {number} id     The key identifier.
	 * @param {boolean} more  Provides more data related to canton.
	 * @returns Returns a canton data, or exception 'E404CantonNotFound' raises.
	 */
	async findByCantonId(id: number, more?: boolean): Promise<PrecinctCanton> {
		const canton = await this.cantonRepo.findOne({
			where: { id },
			relations: more ? ['district', 'zipList'] : [],
		});
		if (!canton)
			throw Errors.E404PrecinctCantonNotFound();
		return canton;
	}

	/**
	 * Loads all cantons data.
	 * 
	 * @param {boolean} more  Provides more data related to canton.
	 * @returns Returns all cantons data.
	 */
	async findAllCantons(more?: boolean): Promise<PrecinctCanton[]> {
		return await this.cantonRepo.find({
			relations: more ? ['district', 'zipList'] : [],
		});
	}

	/**
	 * Loads all cantons data within one district.
	 * 
	 * @param {number} districtId  The key of district.
	 * @param {boolean} more       Provides more data related to canton.
	 * @returns Returns all cantons data.
	 */
	async findCantonsByDistrict(districtId: number, more?: boolean): Promise<PrecinctCanton[]> {
		if (more) {
			return await this.dataSource.createQueryBuilder()
				.select('canton')
				.from(PrecinctCanton, 'canton')
				.leftJoinAndSelect('canton.district', 'district')
				.leftJoinAndSelect('canton.zipList', 'zipList')
				.where('canton.district = :district', { district: districtId })
				.getMany();
		}
		else {
			return await this.dataSource.createQueryBuilder()
				.select('canton')
				.from(PrecinctCanton, 'canton')
				.where('canton.district = :district', { district: districtId })
				.getMany();
		}
	}


	/////////////////////////////////////////////////////////////////////////
	// DISTRICT
	/////////////////////////////////////////////////////////////////////////

	/**
	 * Gets a district data from id.
	 * 
	 * @param {number} id     The key identifier.
	 * @param {boolean} more  Provides more data related to district.
	 * @returns Returns a district data, or exception 'E404DistrictNotFound' raises.
	 */
	async findByDistrictId(id: number, more?: boolean): Promise<PrecinctDistrict> {
		const district = await this.districtRepo.findOne({
			where: { id },
			relations: more ? ['province', 'cantonList'] : [],
		});
		if (!district)
			throw Errors.E404PrecinctDistrictNotFound();
		return district;
	}

	/**
	 * Loads all districts data.
	 * 
	 * @param {boolean} more  Provides more data related to district.
	 * @returns Returns all districts data.
	 */
	async findAllDistricts(more?: boolean): Promise<PrecinctDistrict[]> {
		return await this.districtRepo.find({
			relations: more ? ['province', 'cantonList'] : [],
		});
	}

	/**
	 * Loads all districts data within one province.
	 * 
	 * @param {number} provinceId  The key of province.
	 * @param {boolean} more       Provides more data related to district.
	 * @returns Returns all districts data.
	 */
	async findDistrictsByProvince(provinceId: number, more?: boolean): Promise<PrecinctDistrict[]> {
		if (more) {
			return await this.dataSource.createQueryBuilder()
				.select('district')
				.from(PrecinctDistrict, 'district')
				.leftJoinAndSelect('district.province', 'province')
				.leftJoinAndSelect('district.cantonList', 'cantonList')
				.where('district.province = :province', { province: provinceId })
				.getMany();
		}
		else {
			return await this.dataSource.createQueryBuilder()
				.select('district')
				.from(PrecinctDistrict, 'district')
				.where('district.province = :province', { province: provinceId })
				.getMany();
		}
	}


	/////////////////////////////////////////////////////////////////////////
	// PROVINCE
	/////////////////////////////////////////////////////////////////////////

	/**
	 * Gets a province data from id.
	 * 
	 * @param {number} id     The key identifier.
	 * @param {boolean} more  Provides more data related to province.
	 * @returns Returns a province data, or exception 'E404ProvinceNotFound' raises.
	 */
	async findByProvinceId(id: number, more?: boolean): Promise<PrecinctProvince> {
		const province = await this.provinceRepo.findOne({
			where: { id },
			relations: more ? ['sector', 'districtList'] : [],
		});
		if (!province)
			throw Errors.E404PrecinctProvinceNotFound();
		return province;
	}

	/**
	 * Loads all provinces data.
	 * 
	 * @param {boolean} more  Provides more data related to province.
	 * @returns Returns all provinces data.
	 */
	async findAllProvinces(more?: boolean): Promise<PrecinctProvince[]> {
		return await this.provinceRepo.find({
			relations: more ? ['sector', 'districtList'] : [],
		});
	}

	/**
	 * Loads all provinces data within one sector.
	 * 
	 * @param {number} sectorId  The key of sector.
	 * @param {boolean} more     Provides more data related to province.
	 * @returns Returns all provinces data.
	 */
	async findProvincesBySector(sectorId: number, more?: boolean): Promise<PrecinctProvince[]> {
		if (more) {
			return await this.dataSource.createQueryBuilder()
				.select('province')
				.from(PrecinctProvince, 'province')
				.leftJoinAndSelect('province.sector', 'sector')
				.leftJoinAndSelect('province.districtList', 'districtList')
				.where('province.sector = :sector', { sector: sectorId })
				.getMany();
		}
		else {
			return await this.dataSource.createQueryBuilder()
				.select('province')
				.from(PrecinctProvince, 'province')
				.where('province.sector = :sector', { sector: sectorId })
				.getMany();
		}
	}


	/////////////////////////////////////////////////////////////////////////
	// SECTOR
	/////////////////////////////////////////////////////////////////////////

	/**
	 * Gets a sector data from id.
	 * 
	 * @param {number} id     The key identifier.
	 * @param {boolean} more  Provides more data related to sector.
	 * @returns Returns a sector data, or exception 'E404SectorNotFound' raises.
	 */
	async findBySectorId(id: number, more?: boolean): Promise<PrecinctSector> {
		const sector = await this.sectorRepo.findOne({
			where: { id },
			relations: more ? ['provinceList'] : [],
		});
		if (!sector)
			throw Errors.E404PrecinctSectorNotFound();
		return sector;
	}

	/**
	 * Loads all sectors data.
	 * 
	 * @param {boolean} more  Provides more data related to sector.
	 * @returns Returns all sectors data.
	 */
	async findAllSectors(more?: boolean): Promise<PrecinctSector[]> {
		return await this.sectorRepo.find({
			relations: more ? ['provinceList'] : [],
		});
	}


	/////////////////////////////////////////////////////////////////////////
	// ZIP
	/////////////////////////////////////////////////////////////////////////

	/**
	 * Gets a ZIP data from id.
	 * 
	 * @param {number} id  The key identifier.
	 * @returns Returns a zip data, or exception 'E404ZipNotFound' raises.
	 */
	async findByZipId(id: number): Promise<PrecinctZip> {
		const zip = await this.zipRepo.findOne({
			where: { id },
			relations: ['canton'],
			order: { 'zip': 'ASC' },
		});
		if (!zip)
			throw Errors.E404PrecinctZipNotFound();
		return zip;
	}

	/**
	 * Loads all ZIP data.
	 * 
	 * @returns Returns all ZIP data.
	 */
	async findAllZips(): Promise<PrecinctZip[]> {
		return await this.zipRepo.find({
			relations: ['canton'],
			order: { 'zip': 'ASC' },
		});
	}

	/**
	 * Loads province, district and canton from ZIP code.
	 * 
	 * The function loads array of province, district and canton.
	 * Then, build tree of provinces, then each provinces will have sub-tree of district, and same district and canton.
	 * 
	 * @returns Returns array of provinces with sub-tree of districts and cantons.  It's may be zero items to indicated no data.
	 */
	async findFromZip(zip: string): Promise<PrecinctProvince[]> {
		// loads provinces, districts and cantons
		const rows = await this.dataSource.createQueryBuilder()
			.select('zip')
			.from(PrecinctZip, 'zip')
			.leftJoinAndSelect('zip.canton', 'canton')
			.leftJoinAndSelect('canton.district', 'district')
			.leftJoinAndSelect('district.province', 'province')
			.where('zip.zip = :zip', { zip })
			.getMany();

		// loops all results, to build tree
		const result: PrecinctProvince[] = [];
		for (var i = 0; i < rows.length; i++) {
			const item = rows[i];

			// find province node
			var province = result.find((element) => element.id === item.canton.district.province.id);
			if (!province) {
				// build tree node of province
				const o = item.canton.district.province;
				province = new PrecinctProvince({
					id: o.id,
					nameEn: o.nameEn,
					nameTh: o.nameTh,
				});
				result.push(province)
			}

			// find district node
			if (!province.districtList)
				province.districtList = [];
			var district = province.districtList.find((element) => element.id === item.canton.district.id);
			if (!district) {
				// build tree node of district
				const o = item.canton.district;
				district = new PrecinctDistrict({
					id: o.id,
					nameEn: o.nameEn,
					nameTh: o.nameTh,
				});
				province.districtList.push(district);
			}

			// find canton node
			if (!district.cantonList)
				district.cantonList = [];
			var canton = district.cantonList.find((element) => element.id === item.canton.id);
			if (!canton) {
				// build tree node of canton
				const o = item.canton;
				canton = new PrecinctCanton({
					id: o.id,
					nameEn: o.nameEn,
					nameTh: o.nameTh,
				});
				district.cantonList.push(canton);
			}
		}

		return result;
	}

	/**
	 * Checks whether data is ok with database.
	 * 
	 * @param {number} cantonId    The canton id.
	 * @param {number} districtId  The district id.
	 * @param {number} provinceId  The province id,
	 * @param {string} zip         The ZIP code, must be 5 characters.
	 * @returns If ok, return 'PrecinctZip' record.  Otherwise, exception 'E404ZipOrCantonInvalid' raises.
	 */
	async verifyZip(zip: string, cantonId: number, districtId: number, provinceId: number): Promise<PrecinctZip> {
		const row = await this.dataSource.createQueryBuilder()
			.select('zip')
			.from(PrecinctZip, 'zip')
			.leftJoinAndSelect('zip.canton', 'canton')
			.leftJoinAndSelect('canton.district', 'district')
			.leftJoinAndSelect('district.province', 'province')
			.where('zip.zip = :zip', { zip })
			.andWhere('zip.canton = :canton', { canton: cantonId })
			.andWhere('district.id = :district', { district: districtId })
			.andWhere('province.id = :province', { province: provinceId })
			.getOne();
		if (!row)
			throw Errors.E404PrecinctZipOrCantonInvalid();
		return row;
	}

}
