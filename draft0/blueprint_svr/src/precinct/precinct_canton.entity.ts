import {
	Entity,
	Column,
	PrimaryColumn,
	CreateDateColumn,
	UpdateDateColumn,
	OneToMany,
	ManyToOne,
	JoinColumn,
	Index,
} from 'typeorm';
import { inspect } from 'node:util';
import { PrecinctEntityConsts } from './precinct_entity_consts';
import { PrecinctDistrict } from './precinct_district.entity';
import { PrecinctZip } from './precinct_zip.entity';


/**
 * Canton (แขวง/ตำบล) model.
 */
@Entity({
	name: 'precinct_canton',
})
export class PrecinctCanton {

	@PrimaryColumn({
		type: 'int',
	})
	id: number;

	// ----------------------------------------------------------------------

	/**
	 * Canton name English.
	 */
	@Index()
	@Column({
		length: PrecinctEntityConsts.NameEn,
		name: 'name_en',
	})
	nameEn: string;

	/**
	 * Canton name Thai.
	 */
	@Index()
	@Column({
		length: PrecinctEntityConsts.NameTh,
		name: 'name_th',
	})
	nameTh: string;

	// ----------------------------------------------------------------------

	/**
	 * Links back to [PrecinctDistrict]{@link PrecinctDistrict.html}.
	 */
	@Index()
	@ManyToOne(() => PrecinctDistrict, (inverse) => inverse.cantonList, {
		//cascade: true,
		onDelete: 'CASCADE',
	})
	@JoinColumn({
		name: 'district_id',
	})
	district: PrecinctDistrict;

	/**
	 * Links to [PrecinctZip]{@link PrecinctZip.html}.
	 */
	@OneToMany(() => PrecinctZip, (inverse) => inverse.canton)
	zipList: PrecinctZip[];

	// ----------------------------------------------------------------------

	@CreateDateColumn({})
	created: Date;

	@UpdateDateColumn({})
	updated: Date;

	constructor(o?: Partial<PrecinctCanton>) {
		Object.assign(this, o)
	}

	// ----------------------------------------------------------------------

	print() {
		return `[${this.id + '/' + this.nameEn}]`;
	}

	fullPrint(depth?: number) {
		return inspect(this, { depth });
	}

}
