import {
	Entity,
	Column,
	PrimaryColumn,
	CreateDateColumn,
	UpdateDateColumn,
	OneToMany,
	ManyToOne,
	JoinColumn,
	Index,
} from 'typeorm';
import { inspect } from 'node:util';
import { PrecinctEntityConsts } from './precinct_entity_consts';
import { PrecinctSector } from './precinct_sector.entity';
import { PrecinctDistrict } from './precinct_district.entity';


/**
 * Province (จังหวัด) model.
 */
@Entity({
	name: 'precinct_province',
})
export class PrecinctProvince {

	@PrimaryColumn({
		type: 'int',
	})
	id: number;

	// ----------------------------------------------------------------------

	/**
	 * Province name English.
	 */
	@Index()
	@Column({
		length: PrecinctEntityConsts.NameEn,
		name: 'name_en',
	})
	nameEn: string;

	/**
	 * Province name Thai.
	 */
	@Index()
	@Column({
		length: PrecinctEntityConsts.NameTh,
		name: 'name_th',
	})
	nameTh: string;

	// ----------------------------------------------------------------------

	/**
	 * Links back to [PrecinctSector]{@link PrecinctSector.html}.
	 */
	@Index()
	@ManyToOne(() => PrecinctSector, (inverse) => inverse.provinceList, {
		//cascade: true,
		//onDelete: 'CASCADE',
	})
	@JoinColumn({
		name: 'sector_id',
	})
	sector: PrecinctSector;

	/**
	 * Links to [PrecinctDistrict]{@link PrecinctDistrict.html}.
	 */
	@OneToMany(() => PrecinctDistrict, (inverse) => inverse.province)
	districtList: PrecinctDistrict[];

	// ----------------------------------------------------------------------

	@CreateDateColumn({})
	created: Date;

	@UpdateDateColumn({})
	updated: Date;

	constructor(o?: Partial<PrecinctProvince>) {
		Object.assign(this, o)
	}

	// ----------------------------------------------------------------------

	print() {
		return `[${this.id + '/' + this.nameEn}]`;
	}

	fullPrint(depth?: number) {
		return inspect(this, { depth });
	}

}
