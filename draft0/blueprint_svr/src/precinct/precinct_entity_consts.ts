/**
 * Precinct's entities constants.
 */
export enum PrecinctEntityConsts {
	NameEn = 50,
	NameTh = 50,
	Zip = 5,
}
