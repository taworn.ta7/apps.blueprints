import { Module } from '@nestjs/common';
import { SharedService } from '../shared/shared.service';
import { SharedModule } from '../shared/shared.module';
import { PrecinctService } from '../precinct/precinct.service';
import { PrecinctModule } from '../precinct/precinct.module';
import { MemberService } from '../member/member.service';
import { MemberChangePropertyService } from '../member/member_change_property.service';
import { MemberStockIconsService } from '../member/member_stock_icons.service';
import { MemberProfileService } from '../member/member_profile.service';
import { MemberSignUpService } from '../member/member_signup.service';
import { MemberResetService } from '../member/member_reset.service';
import { MemberAuthenService } from '../member/member_authen.service';
import { MemberAddressService } from '../member/member_address.service';
import { MemberModule } from '../member/member.module';
import { TestMemberController } from './test_member.controller';


@Module({
	imports: [
		SharedModule,
		PrecinctModule,
		MemberModule,
	],
	providers: [
		SharedService,
		PrecinctService,
		MemberService,
		MemberChangePropertyService,
		MemberStockIconsService,
		MemberProfileService,
		MemberSignUpService,
		MemberResetService,
		MemberAuthenService,
		MemberAddressService,
	],
	controllers: [
		TestMemberController,
	],
})
export class TestMemberModule { }
