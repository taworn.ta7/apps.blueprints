import {
	Logger,
	Controller,
	Req,
	Res,
	Get,
	Param,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { regexMail } from '../helpers/string_ex';
import { Pagination, queryPagination } from '../helpers/pagination';
import { SharedService } from '../shared/shared.service';
import { MemberProfile } from '../member/member_profile.entity';
import { MemberService } from '../member/member.service';
import { MemberStockIconsService } from '../member/member_stock_icons.service';


@Controller(`/api/member`)
export class TestMemberController {

	private readonly logger = new Logger(TestMemberController.name);
	private readonly config = this.sharedService.config;

	constructor(
		private readonly sharedService: SharedService,
		private readonly iconService: MemberStockIconsService,
		private readonly memberService: MemberService,
	) { }

	// ----------------------------------------------------------------------

	@Get('find/:idOrMail')
	async findOne(
		@Req() req: Request,
		@Param('idOrMail') idOrMail: string,
	): Promise<{
		profile: MemberProfile,
	}> {
		let profile: MemberProfile;
		if (regexMail.test(idOrMail))
			profile = await this.memberService.findByMail(idOrMail);
		else
			profile = await this.memberService.findById(idOrMail);
		return { profile };
	}


	@Get('find/icon/:idOrMail')
	async findIcon(
		@Req() req: Request,
		@Res() res: Response,
		@Param('idOrMail') idOrMail: string,
	): Promise<any> {
		let profile: MemberProfile;
		if (regexMail.test(idOrMail))
			profile = await this.memberService.findByMail(idOrMail, true);
		else
			profile = await this.memberService.findById(idOrMail, true);
		if (profile.extra.icon) {
			res.writeHead(200, { 'Content-Type': profile.extra.mime });
			res.end(profile.extra.icon);
		}
		else {
			const { mime, icon } = await this.iconService.get(profile.mail);
			res.writeHead(200, { 'Content-Type': mime });
			res.end(icon);
		}
	}


	@Get('find')
	async find(
		@Req() req: Request,
	): Promise<{
		profiles: MemberProfile[],
		pagination: Pagination,
	}> {
		const options = queryPagination(req, 10, {
			mail: 'mail',
			created: 'created',
			updated: 'updated',
		});
		return await this.memberService.find(req, options);
	}

	// ----------------------------------------------------------------------

	@Get('icons/:name')
	async getProfileIcon(
		@Req() req: Request,
		@Res() res: Response,
		@Param('name') name: string,
	): Promise<any> {
		const { mime, icon } = await this.iconService.get(name);
		res.writeHead(200, { 'Content-Type': mime });
		res.end(icon);
	}

}
