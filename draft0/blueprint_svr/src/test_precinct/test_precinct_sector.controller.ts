import {
	Logger,
	Controller,
	Req,
	Get,
	Param,
	Query,
} from '@nestjs/common';
import { Request } from 'express';
import * as string_ex from '../helpers/string_ex';
import { PrecinctSector } from '../precinct/precinct_sector.entity';
import { PrecinctService } from '../precinct/precinct.service';


@Controller(`/api/precinct/sector`)
export class TestPrecinctSectorController {

	private readonly logger = new Logger(TestPrecinctSectorController.name);

	constructor(
		private readonly service: PrecinctService,
	) { }

	// ----------------------------------------------------------------------

	@Get('id/:id')
	async findBySectorId(
		@Req() req: Request,
		@Param('id') id: number,
		@Query('more') more: string,
	): Promise<{
		sector: PrecinctSector,
	}> {
		const sector = await this.service.findBySectorId(id, string_ex.queryStringToBool(more));
		this.logger.debug(`sector: ${sector.print()}, full: ${sector.fullPrint()}`);
		return {
			sector,
		}
	}

	@Get('all')
	async findAllSectors(
		@Req() req: Request,
		@Query('more') more: string,
	): Promise<{
		sectorList: PrecinctSector[],
	}> {
		return {
			sectorList: await this.service.findAllSectors(string_ex.queryStringToBool(more)),
		}
	}

}
