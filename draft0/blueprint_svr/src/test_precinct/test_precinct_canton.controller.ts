import {
	Logger,
	Controller,
	Req,
	Get,
	Param,
	Query,
} from '@nestjs/common';
import { Request } from 'express';
import * as string_ex from '../helpers/string_ex';
import { PrecinctCanton } from '../precinct/precinct_canton.entity';
import { PrecinctService } from '../precinct/precinct.service';


@Controller(`/api/precinct/canton`)
export class TestPrecinctCantonController {

	private readonly logger = new Logger(TestPrecinctCantonController.name);

	constructor(
		private readonly service: PrecinctService,
	) { }

	// ----------------------------------------------------------------------

	@Get('id/:id')
	async findByCantonId(
		@Req() req: Request,
		@Param('id') id: number,
		@Query('more') more: string,
	): Promise<{
		canton: PrecinctCanton,
	}> {
		const canton = await this.service.findByCantonId(id, string_ex.queryStringToBool(more));
		this.logger.debug(`canton: ${canton.print()}, full: ${canton.fullPrint()}`);
		return {
			canton,
		}
	}

	@Get('all')
	async findAllCantons(
		@Req() req: Request,
		@Query('more') more: string,
	): Promise<{
		cantonList: PrecinctCanton[],
	}> {
		return {
			cantonList: await this.service.findAllCantons(string_ex.queryStringToBool(more)),
		}
	}

	@Get('district/:district')
	async findCantonsByDistrict(
		@Req() req: Request,
		@Param('district') districtId: number,
		@Query('more') more: string,
	): Promise<{
		cantonList: PrecinctCanton[],
	}> {
		return {
			cantonList: await this.service.findCantonsByDistrict(districtId, string_ex.queryStringToBool(more)),
		}
	}

}
