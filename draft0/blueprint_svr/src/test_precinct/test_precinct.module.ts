import { Module } from '@nestjs/common';
import { PrecinctService } from '../precinct/precinct.service';
import { PrecinctModule } from '../precinct/precinct.module';
import { TestPrecinctCantonController } from './test_precinct_canton.controller';
import { TestPrecinctDistrictController } from './test_precinct_district.controller';
import { TestPrecinctProvinceController } from './test_precinct_province.controller';
import { TestPrecinctSectorController } from './test_precinct_sector.controller';
import { TestPrecinctZipController } from './test_precinct_zip.controller';


@Module({
	imports: [
		PrecinctModule,
	],
	providers: [
		PrecinctService,
	],
	controllers: [
		TestPrecinctCantonController,
		TestPrecinctDistrictController,
		TestPrecinctProvinceController,
		TestPrecinctSectorController,
		TestPrecinctZipController,
	],
})
export class TestPrecinctModule { }
