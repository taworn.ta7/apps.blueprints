import {
	Logger,
	Controller,
	Req,
	Get,
	Param,
	Query,
} from '@nestjs/common';
import { Request } from 'express';
import { PrecinctZip } from '../precinct/precinct_zip.entity';
import { PrecinctProvince } from '../precinct/precinct_province.entity';
import { PrecinctService } from '../precinct/precinct.service';


@Controller(`/api/precinct/zip`)
export class TestPrecinctZipController {

	private readonly logger = new Logger(TestPrecinctZipController.name);

	constructor(
		private readonly service: PrecinctService,
	) { }

	// ----------------------------------------------------------------------

	@Get('id/:id')
	async findByZipId(
		@Req() req: Request,
		@Param('id') id: number,
	): Promise<{
		zip: PrecinctZip,
	}> {
		const zip = await this.service.findByZipId(id);
		this.logger.debug(`zip: ${zip.print()}, full: ${zip.fullPrint()}`);
		return {
			zip,
		}
	}

	@Get('all')
	async findAllZip(
		@Req() req: Request,
	): Promise<{
		zipList: PrecinctZip[],
	}> {
		return {
			zipList: await this.service.findAllZips(),
		}
	}

	// ----------------------------------------------------------------------

	/////////////////////////////////////////////////////////////////////////
	// Find From ZIP
	/////////////////////////////////////////////////////////////////////////

	@Get('find-from-zip/:zip')
	async findFromZip(
		@Req() req: Request,
		@Param('zip') zip: string,
	): Promise<{
		provinceList: PrecinctProvince[],
	}> {
		return {
			provinceList: await this.service.findFromZip(zip.trim()),
		}
	}

}
