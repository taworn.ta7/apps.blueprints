import {
	Logger,
	Controller,
	Req,
	Get,
	Param,
	Query,
} from '@nestjs/common';
import { Request } from 'express';
import * as string_ex from '../helpers/string_ex';
import { PrecinctProvince } from '../precinct/precinct_province.entity';
import { PrecinctService } from '../precinct/precinct.service';


@Controller(`/api/precinct/province`)
export class TestPrecinctProvinceController {

	private readonly logger = new Logger(TestPrecinctProvinceController.name);

	constructor(
		private readonly service: PrecinctService,
	) { }

	// ----------------------------------------------------------------------

	@Get('id/:id')
	async findByProvinceId(
		@Req() req: Request,
		@Param('id') id: number,
		@Query('more') more: string,
	): Promise<{
		province: PrecinctProvince,
	}> {
		const province = await this.service.findByProvinceId(id, string_ex.queryStringToBool(more));
		this.logger.debug(`province: ${province.print()}, full: ${province.fullPrint()}`);
		return {
			province,
		}
	}

	@Get('all')
	async findAllProvinces(
		@Req() req: Request,
		@Query('more') more: string,
	): Promise<{
		provinceList: PrecinctProvince[],
	}> {
		return {
			provinceList: await this.service.findAllProvinces(string_ex.queryStringToBool(more)),
		}
	}

	@Get('sector/:sector')
	async findProvincesBySector(
		@Req() req: Request,
		@Param('sector') sectorId: number,
		@Query('more') more: string,
	): Promise<{
		provinceList: PrecinctProvince[],
	}> {
		return {
			provinceList: await this.service.findProvincesBySector(sectorId, string_ex.queryStringToBool(more)),
		}
	}

}
