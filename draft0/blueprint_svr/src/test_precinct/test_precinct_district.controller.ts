import {
	Logger,
	Controller,
	Req,
	Get,
	Param,
	Query,
} from '@nestjs/common';
import { Request } from 'express';
import * as string_ex from '../helpers/string_ex';
import { PrecinctDistrict } from '../precinct/precinct_district.entity';
import { PrecinctService } from '../precinct/precinct.service';


@Controller(`/api/precinct/district`)
export class TestPrecinctDistrictController {

	private readonly logger = new Logger(TestPrecinctDistrictController.name);

	constructor(
		private readonly service: PrecinctService,
	) { }

	// ----------------------------------------------------------------------

	@Get('id/:id')
	async findByDistrictId(
		@Req() req: Request,
		@Param('id') id: number,
		@Query('more') more: string,
	): Promise<{
		district: PrecinctDistrict,
	}> {
		const district = await this.service.findByDistrictId(id, string_ex.queryStringToBool(more));
		this.logger.debug(`district: ${district.print()}, full: ${district.fullPrint()}`);
		return {
			district,
		}
	}

	@Get('all')
	async findAllDistricts(
		@Req() req: Request,
		@Query('more') more: string,
	): Promise<{
		districtList: PrecinctDistrict[],
	}> {
		return {
			districtList: await this.service.findAllDistricts(string_ex.queryStringToBool(more)),
		}
	}

	@Get('province/:province')
	async findDistrictsByProvince(
		@Req() req: Request,
		@Param('province') provinceId: number,
		@Query('more') more: string,
	): Promise<{
		districtList: PrecinctDistrict[],
	}> {
		return {
			districtList: await this.service.findDistrictsByProvince(provinceId, string_ex.queryStringToBool(more)),
		}
	}

}
