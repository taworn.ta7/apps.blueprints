import { IncomingHttpHeaders } from 'http';
import { Express, Request } from 'express';
import { MemberProfile } from '../member/member_profile.entity';
import { CaretakerProfile } from '../caretaker/caretaker_profile.entity';


declare module 'http' {
	interface IncomingHttpHeaders {
		"Authorization-X"?: string;
	}
}


declare global {
	namespace Express {
		export interface Request {
			// adds request id field
			id?: string;

			// adds authentication member/caretaker profile field
			profile?: MemberProfile | CaretakerProfile;
			//profile?: any;
		}
	}
}
