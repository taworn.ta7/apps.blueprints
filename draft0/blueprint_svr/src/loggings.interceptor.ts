import {
	Logger,
	CallHandler,
	ExecutionContext,
	Injectable,
	NestInterceptor,
} from '@nestjs/common';
import { Request } from 'express';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { MemberProfile } from './member/member_profile.entity';
import { CaretakerProfile } from './caretaker/caretaker_profile.entity';


@Injectable()
export class LoggingsInterceptor implements NestInterceptor {

	private readonly logger = new Logger(LoggingsInterceptor.name);

	private static requestId: number = 0;

	generateRequestId(): string {
		const result = LoggingsInterceptor.requestId++;
		if (LoggingsInterceptor.requestId >= 1000000000)
			LoggingsInterceptor.requestId = 0;
		return result.toString().padStart(9, '0')
	}

	intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
		const req = context.switchToHttp().getRequest<Request>();
		req.id = this.generateRequestId();

		/*
		// adds 'profile' field to help understand which this log from.
		if (req.profile as MemberProfile)
			this.logger.log(`${req.id}; ${req.method} ${req.originalUrl}; member [${req.profile.id}/${req.profile.mail}]`);
		else if (req.profile as CaretakerProfile)
			this.logger.log(`${req.id}; ${req.method} ${req.originalUrl}; caretaker [${req.profile.id}/${req.profile.mail}]`);
		else
		*/
		this.logger.log(`${req.id}; ${req.method} ${req.originalUrl}`);

		const now = Date.now();
		return next.handle().pipe(
			tap(() => this.logger.verbose(`${req.id}; success, time ${Date.now() - now} ms`))
		);
	}

}
