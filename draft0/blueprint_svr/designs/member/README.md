# Member

Membership managing.


## Features

* sign-up by email, [code](../../src/member/member_signup.controller.ts) or [diagram](./member_signup.txt)
* reset password if member forgotten his/her password, [code](../../src/member/member_reset.controller.ts) or [diagram](./member_reset.txt)
* authentication with email and password, [code](../../src/member/member_authen.controller.ts) or [diagram](./member_authen.txt)
* authentication with Google or Line, [Google code](../../src/member/member_authenx_google.controller.ts) or [Line code](../../src/member/member_authenx_line.controller.ts)
* member profile manage, [code](../../src/member/member_profile.controller.ts) or [diagram](./member_profile.txt)
  + change email
  + change password
  + save settings
  + upload, view and delete icon
  + resigned T_T
* member's address, [code](../../src/member/member_address.controller.ts)


## Installation

* required 'Shared' and 'Precinct' as prerequisite
* copy folder ['src/helpers'](../../src/helpers) to target
* copy folder ['src/member'](../../src/member) to target
* copy folder ['src/mail_templates/member_*'](../../src/mail_templates) to target
* see ['src/test_member'](../../src/test_member) as samples

