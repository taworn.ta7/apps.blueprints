# Caretaker

Caretakership managing.


## Features

* assign by root, [code](../../src/caretaker/caretaker_assign.controller.ts) or [diagram](./caretaker_assign.txt)
* reset password if caretaker forgotten his/her password, [code](../../src/caretaker/caretaker_reset.controller.ts) or [diagram](./caretaker_reset.txt)
* authentication with email and password, [code](../../src/caretaker/caretaker_authen.controller.ts) or [diagram](./caretaker_authen.txt)
* caretaker profile manage, [code](../../src/caretaker/caretaker_profile.controller.ts) or [diagram](./caretaker_profile.txt)
  + change email
  + change password
  + save settings
  + upload, view and delete icon
  + resigned T_T
* caretaker's address, [code](../../src/caretaker/caretaker_address.controller.ts)


## Installation

* required 'Shared' and 'Precinct' as prerequisite
* copy folder ['src/helpers'](../../src/helpers) to target
* copy folder ['src/caretaker'](../../src/caretaker) to target
* copy folder ['src/mail_templates/caretaker_*'](../../src/mail_templates) to target
* see ['src/test_caretaker'](../../src/test_caretaker) as samples

