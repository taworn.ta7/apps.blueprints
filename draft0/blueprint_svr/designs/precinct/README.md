# Precinct

Query by canton, district, province, sector or ZIP.


## Installation

* copy folder ['src/helpers'](../../src/helpers) to target
* copy folder ['src/precinct'](../../src/precinct) to target
* see ['src/test_precinct'](../../src/test_precinct) as samples

