# blueprint_svr

Blueprint code for Nest 10.


## Modules

* [Shared](./designs/shared/README.md)
* [Precinct](./designs/precinct/README.md)
* [Member](./designs/member/README.md)
* [Caretaker](./designs/caretaker/README.md)


## Credits

We use [SequenceDiagram.org](https://sequencediagram.org) to generate diagrams.  Thank you for text drawing.


## Last

Sorry, but I'm not good at English. T_T

