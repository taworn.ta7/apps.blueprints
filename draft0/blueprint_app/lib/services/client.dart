import 'dart:convert';
import 'package:logging/logging.dart';
import 'package:http/http.dart' as http;
import './rest_result.dart';

/// HTTP methods.
enum MethodType {
  delete,
  get,
  head,
  patch,
  post,
  put,
}

/// Client connection.
class Client {
  static final log = Logger('Client');

  /// JSON formatter.
  static const _formatter = JsonEncoder.withIndent('  ');

  /// Base URL.
  String _baseUrl = '';
  String get baseUrl => _baseUrl;

  /// Constructor.
  Client(String baseUrl) {
    _baseUrl = baseUrl;
    if (!_baseUrl.endsWith('/')) _baseUrl += '/';
  }

  // ----------------------------------------------------------------------

  /// Formats JSON to string.
  String formatJson(
    Object data, {
    bool outputLog = false,
  }) {
    final json = _formatter.convert(data);
    if (outputLog) log.finer("JSON: $json");
    return json;
  }

  /// Concats base URL with address, optional with trailed query string.
  Uri url(String address, [Map<String, String>? query]) {
    var params = Uri(queryParameters: query).query;
    if (params != '') params = '?$params';
    final url = Uri.parse('$baseUrl$address$params');
    return url;
  }

  // ----------------------------------------------------------------------

  /// Calls HTTP.
  /// Returns RestResult.
  Future<RestResult> call(
    MethodType method,
    Uri url, {
    Object? body,
    Map<String, String>? customHeaders,
  }) async {
    try {
      Map<String, String>? headers =
          customHeaders ?? {'Content-Type': 'application/json;charset=utf-8'};

      late http.Response res;
      switch (method) {
        case MethodType.delete:
          log.info("DELETE $url...");
          res = await http.delete(url, headers: headers, body: body);
          break;
        case MethodType.get:
          log.info("GET $url...");
          res = await http.get(url, headers: headers);
          break;
        case MethodType.head:
          log.info("HEAD $url...");
          res = await http.head(url, headers: headers);
          break;
        case MethodType.patch:
          log.info("PATCH $url...");
          res = await http.patch(url, headers: headers, body: body);
          break;
        case MethodType.post:
          log.info("POST $url...");
          res = await http.post(url, headers: headers, body: body);
          break;
        case MethodType.put:
          log.info("PUT $url...");
          res = await http.put(url, headers: headers, body: body);
          break;
      }

      return RestResult.ok(res);
    } catch (ex) {
      return RestResult.error(ex.toString());
    }
  }

  /// Uploads file via HTTP.
  /// Returns RestResult.
  Future<RestResult> upload(
    MethodType method,
    Uri url, {
    required http.MultipartFile file,
    Map<String, String>? customHeaders,
  }) async {
    try {
      Map<String, String>? headers =
          customHeaders ?? {'Content-Type': 'multipart/form-data'};

      late http.MultipartRequest req;
      switch (method) {
        case MethodType.put:
          log.info("PUT $url...");
          req = http.MultipartRequest('PUT', url);
          break;
        case MethodType.post:
        default:
          log.info("POST $url...");
          req = http.MultipartRequest('POST', url);
          break;
      }

      req.headers.addAll(headers);
      req.files.add(file);
      final res = await req.send();
      return RestResult.ok(await http.Response.fromStream(res));
    } catch (ex) {
      return RestResult.error(ex.toString());
    }
  }
}
