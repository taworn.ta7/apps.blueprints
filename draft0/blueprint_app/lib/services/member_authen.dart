part of 'member.dart';

/// Sign-in with mail and password.
/// If mail and password corrected, it will returns member profile with sign-in token.
Future<RestResult> memberAuthenSignIn(
  Client client, {
  required String mail,
  required String password,
  Map<String, String>? customHeaders,
  bool outputJsonLog = false,
}) async {
  return await client.call(
    MethodType.put,
    client.url('member/authen/signin'),
    body: client.formatJson({
      'signIn': {
        'mail': mail,
        'password': password,
      },
    }, outputLog: outputJsonLog),
    customHeaders: customHeaders,
  );
}

/// Signs off from the current session.  The sign-in token will be invalid.
Future<RestResult> memberAuthenSignOut(
  Client client, {
  Map<String, String>? customHeaders,
  bool outputJsonLog = false,
}) async {
  return await client.call(
    MethodType.put,
    client.url('member/authen/signout'),
    body: client.formatJson({}, outputLog: outputJsonLog),
    customHeaders: customHeaders,
  );
}
