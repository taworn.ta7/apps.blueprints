part of 'member.dart';

/// Saves reset password form into database.
Future<RestResult> memberReset(
  Client client, {
  required String mail,
  Map<String, String>? customHeaders,
  bool outputJsonLog = false,
}) async {
  return await client.call(
    MethodType.post,
    client.url('member/reset'),
    body: client.formatJson({
      'reset': {
        'mail': mail,
      },
    }, outputLog: outputJsonLog),
    customHeaders: customHeaders,
  );
}

/// Confirms the reset password form.  This will generate new password and send mail with new password.
Future<RestResult> memberResetConfirm(
  Client client, {
  required String code,
  Map<String, String>? customHeaders,
  bool outputJsonLog = false,
}) async {
  return await client.call(
    MethodType.get,
    client.url('member/reset/confirm?code=$code'),
    body: client.formatJson({}, outputLog: outputJsonLog),
    customHeaders: customHeaders,
  );
}
