/// Query paging, page size, searching, data in trash and ordering.
class QueryPage {
  int page;
  int size;
  String search;
  bool trash;
  String order;

  QueryPage({
    this.page = 0,
    this.size = 1,
    this.search = '',
    this.trash = false,
    this.order = '',
  });

  Map<String, String> toMap() {
    Map<String, String> map = {};
    if (page >= 0) map.addAll({'page': page.toString()});
    if (page >= 1) map.addAll({'size': size.toString()});
    if (search.trim().isNotEmpty) map.addAll({'search': search});
    if (trash) map.addAll({'trash': '1'});
    if (order.trim().isNotEmpty) map.addAll({'order': order});
    return map;
  }
}
