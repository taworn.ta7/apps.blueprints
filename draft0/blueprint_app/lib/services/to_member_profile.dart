import 'package:logging/logging.dart';
import '../models/member_profile.dart';

/// Converts to member profile.
MemberProfile toMemberProfile(
  Map<String, dynamic> entry, {
  Logger? log,
}) {
  final item = MemberProfile.fromJson(entry);
  if (log != null) log.finer("member profile: $item");
  return item;
}

/// Converts to member profile list.
List<MemberProfile> toMemberProfileList(
  List<dynamic> list, {
  Logger? log,
}) {
  final items = <MemberProfile>[];
  if (log != null) log.finer("member profile, find count: ${list.length}");
  for (var i = 0; i < list.length; i++) {
    final item = MemberProfile.fromJson(list[i]);
    if (log != null) log.finer("- $i: $item");
    items.add(item);
  }
  return items;
}
