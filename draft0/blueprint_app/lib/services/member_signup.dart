part of 'member.dart';

/// Saves sign-up form into database.
Future<RestResult> memberSignUp(
  Client client, {
  required String mail,
  required String password,
  required String confirmPassword,
  Map<String, String>? customHeaders,
  bool outputJsonLog = false,
}) async {
  return await client.call(
    MethodType.post,
    client.url('member/signup'),
    body: client.formatJson({
      'signUp': {
        'mail': mail,
        'password': password,
        'confirmPassword': confirmPassword,
      },
    }, outputLog: outputJsonLog),
    customHeaders: customHeaders,
  );
}

/// Confirms the sign-up form, then, copies sign-up data into new 'MemberProfile' entity.
Future<RestResult> memberSignUpConfirm(
  Client client, {
  required String code,
  Map<String, String>? customHeaders,
  bool outputJsonLog = false,
}) async {
  return await client.call(
    MethodType.get,
    client.url('member/signup/confirm?code=$code'),
    body: client.formatJson({}, outputLog: outputJsonLog),
    customHeaders: customHeaders,
  );
}
