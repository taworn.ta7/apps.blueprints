part of 'member.dart';

/// Finds all of member's addresses.
Future<RestResult> memberAddress(
  Client client, {
  Map<String, String>? customHeaders,
  bool outputJsonLog = false,
}) async {
  return await client.call(
    MethodType.get,
    client.url('member/address'),
    body: client.formatJson({}, outputLog: outputJsonLog),
    customHeaders: customHeaders,
  );
}

/// Saves new address.
Future<RestResult> memberAddressSave(
  Client client, {
  Map<String, String>? customHeaders,
  bool outputJsonLog = false,
}) async {
  return await client.call(
    MethodType.post,
    client.url('member/address'),
    body: client.formatJson({}, outputLog: outputJsonLog),
    customHeaders: customHeaders,
  );
}

/// Replaces existing address.
Future<RestResult> memberAddressReplace(
  Client client, {
  required String id,
  Map<String, String>? customHeaders,
  bool outputJsonLog = false,
}) async {
  return await client.call(
    MethodType.put,
    client.url('member/address/$id'),
    body: client.formatJson({}, outputLog: outputJsonLog),
    customHeaders: customHeaders,
  );
}

/// Deletes existing address.
Future<RestResult> memberAddressDelete(
  Client client, {
  required String id,
  Map<String, String>? customHeaders,
  bool outputJsonLog = false,
}) async {
  return await client.call(
    MethodType.delete,
    client.url('member/address/$id'),
    body: client.formatJson({}, outputLog: outputJsonLog),
    customHeaders: customHeaders,
  );
}
