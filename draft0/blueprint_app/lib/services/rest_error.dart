/// REST results error.
class RestError {
  final int statusCode;
  final int? extraCode;
  final Map<String, dynamic>? locales;
  final String? path;
  final DateTime? timestamp;
  final String? requestId;
  final String? profileId;
  final String? profileMail;

  RestError.fromJson(Map<String, dynamic> json)
      : statusCode = json['statusCode'],
        extraCode = json['extraCode'] as int?,
        locales = json['locales'] as Map<String, dynamic>?,
        path = json['path'] as String?,
        timestamp = json['timestamp'] == null
            ? null
            : DateTime.parse(json['timestamp'] as String),
        requestId = json['requestId'] as String?,
        profileId = json['profileId'] as String?,
        profileMail = json['profileMail'] as String?;

  Map<String, dynamic> toJson() => {
        'statusCode': statusCode,
        'extraCode': extraCode,
        'locales': locales,
        'path': path,
        'timestamp': timestamp,
        'requestId': requestId,
        'profileId': profileId,
        'profileMail': profileMail,
      };

  @override
  String toString() =>
      "$statusCode${locales != null ? ' ${locales!['en']}' : ''}; path=$path, timestamp=$timestamp, requestId=$requestId, profileId=$profileId";
}
