part of 'member.dart';

/// Retrives full profile
Future<RestResult> memberProfileMe(
  Client client, {
  Map<String, String>? customHeaders,
  bool outputJsonLog = false,
}) async {
  return await client.call(
    MethodType.get,
    client.url('member/profile/me'),
    body: client.formatJson({}, outputLog: outputJsonLog),
    customHeaders: customHeaders,
  );
}

// ----------------------------------------------------------------------

/// Saves change mail form into database.
Future<RestResult> memberProfileRequestChangeMail(
  Client client, {
  required String newMail,
  required String password,
  Map<String, String>? customHeaders,
  bool outputJsonLog = false,
}) async {
  return await client.call(
    MethodType.post,
    client.url('member/profile/mail'),
    body: client.formatJson({
      'mail': {
        'newMail': newMail,
        'password': password,
      },
    }, outputLog: outputJsonLog),
    customHeaders: customHeaders,
  );
}

/// Confirms the change mail form.  This will set profile mail to the new one.
Future<RestResult> memberProfileConfirmChangeMail(
  Client client, {
  required String code,
  Map<String, String>? customHeaders,
  bool outputJsonLog = false,
}) async {
  return await client.call(
    MethodType.get,
    client.url('member/profile/mail'),
    body: client.formatJson({}, outputLog: outputJsonLog),
    customHeaders: customHeaders,
  );
}

// ----------------------------------------------------------------------

/// This function updates password.
Future<RestResult> memberProfilePassword(
  Client client, {
  required String currentPassword,
  required String newPassword,
  Map<String, String>? customHeaders,
  bool outputJsonLog = false,
}) async {
  return await client.call(
    MethodType.put,
    client.url('member/profile/password'),
    body: client.formatJson({
      'password': {
        'currentPassword': currentPassword,
        'newPassword': newPassword,
      },
    }, outputLog: outputJsonLog),
    customHeaders: customHeaders,
  );
}

// ----------------------------------------------------------------------

/// This function saves settings, in JSON, into database.
/// The DTO is in 'member_settings.dto'.
Future<RestResult> memberProfileSettings(
  Client client, {
  required int dummy,
  Map<String, String>? customHeaders,
  bool outputJsonLog = false,
}) async {
  return await client.call(
    MethodType.put,
    client.url('member/profile/settings'),
    body: client.formatJson({
      'settings': {
        'dummy': dummy,
      },
    }, outputLog: outputJsonLog),
    customHeaders: customHeaders,
  );
}

// ----------------------------------------------------------------------

/// This function saves icon with MIME into database.
Future<RestResult> memberProfileChangeIcon(
  Client client, {
  Map<String, String>? customHeaders,
  bool outputJsonLog = false,
}) async {
  return await client.call(
    MethodType.post,
    client.url('member/profile/icon'),
    body: client.formatJson({}, outputLog: outputJsonLog),
    customHeaders: customHeaders,
  );
}

/// This function returns profile icon, or default icon if they not set.
Future<RestResult> memberProfileGetIcon(
  Client client, {
  Map<String, String>? customHeaders,
  bool outputJsonLog = false,
}) async {
  return await client.call(
    MethodType.get,
    client.url('member/profile/icon'),
    body: client.formatJson({}, outputLog: outputJsonLog),
    customHeaders: customHeaders,
  );
}

/// This function deletes profile icon and reset it will default profile icon.
Future<RestResult> memberProfileDeleteIcon(
  Client client, {
  Map<String, String>? customHeaders,
  bool outputJsonLog = false,
}) async {
  return await client.call(
    MethodType.delete,
    client.url('member/profile/icon'),
    body: client.formatJson({}, outputLog: outputJsonLog),
    customHeaders: customHeaders,
  );
}

// ----------------------------------------------------------------------

/// This function resign current member.
Future<RestResult> memberProfileResign(
  Client client, {
  required String currentPassword,
  Map<String, String>? customHeaders,
  bool outputJsonLog = false,
}) async {
  return await client.call(
    MethodType.put,
    client.url('member/profile/resign'),
    body: client.formatJson({
      'resign': {
        'currentPassword': currentPassword,
      },
    }, outputLog: outputJsonLog),
    customHeaders: customHeaders,
  );
}
