import './rest_result.dart';
import './client.dart';

part 'member_signup.dart';
part 'member_reset.dart';
part 'member_authen.dart';
part 'member_profile.dart';
part 'member_address.dart';

/// Finds member profile by Id.
/// The 'MemberProfile' if founds, otherwise, exception 'E404MemberNotFound' raises.
Future<RestResult> memberFindOne(
  Client client, {
  required String mail,
  Map<String, String>? customHeaders,
}) async {
  return await client.call(
    MethodType.get,
    client.url('member/find/$mail'),
    customHeaders: customHeaders,
  );
}

/// Finds members with conditions, ordering and trashing.
/// The array of 'MemberProfile' if founds, otherwise, is empty array.
Future<RestResult> memberFind(
  Client client, {
  Map<String, String>? queryPageMap,
  Map<String, String>? customHeaders,
}) async {
  return await client.call(
    MethodType.get,
    client.url('member/find', queryPageMap),
    customHeaders: customHeaders,
  );
}
