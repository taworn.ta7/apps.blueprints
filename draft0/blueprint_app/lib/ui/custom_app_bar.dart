//import 'package:logging/logging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../i18n/translations.g.dart';
import '../localization.dart';
import '../theme_manager.dart';

enum ProfileMenu {
  localeEn,
  localeTh,
  modeDefault,
  modeLight,
  modeDark,
}

/// A customized AppBar.
class CustomAppBar extends StatefulWidget implements PreferredSizeWidget {
  const CustomAppBar({
    Key? key,
    required this.title,
  }) : super(key: key);

  final String title;

  @override
  State<CustomAppBar> createState() => _CustomAppBarState();

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}

// ----------------------------------------------------------------------

class _CustomAppBarState extends State<CustomAppBar> {
  //static final log = Logger('CustomAppBar');

  // ----------------------------------------------------------------------

  /// Build widget tree.
  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Theme.of(context).colorScheme.inversePrimary,
      title: Text(widget.title),
      actions: _buildActionList(context),
    );
  }

  List<Widget> _buildActionList(BuildContext context) {
    final actions = <Widget>[];
    actions.add(_buildPopupMenu(context));
    return actions;
  }

  Widget _buildPopupMenu(BuildContext context) {
    final t = Translations.of(context).strings;

    return PopupMenuButton(
      // item list
      itemBuilder: (context) {
        List<PopupMenuEntry<ProfileMenu>> list = [];

        // locale: en, th
        list.add(_buildMenuItem(ProfileMenu.localeEn, t.locale.en,
            leading: _buildSvg('assets/locales/en.svg'),
            tap: () => Localization.instance().change(context, 'en')));
        list.add(_buildMenuItem(ProfileMenu.localeTh, t.locale.th,
            leading: _buildSvg('assets/locales/th.svg'),
            tap: () => Localization.instance().change(context, 'th')));

        // mode: default, light and dark
        list.add(const PopupMenuDivider());
        list.add(_buildMenuItem(ProfileMenu.modeDefault, t.mode.default_,
            leading: _buildIcon(Icons.favorite, Colors.grey)));
        list.add(_buildMenuItem(ProfileMenu.modeLight, t.mode.light,
            leading: _buildIcon(Icons.favorite, Colors.white)));
        list.add(_buildMenuItem(ProfileMenu.modeDark, t.mode.dark,
            leading: _buildIcon(Icons.favorite, Colors.black)));

        return list;
      },

      // events
      onSelected: (value) async {
        // changes mode to ...
        if (value == ProfileMenu.modeDefault) {
          // default
          ThemeManager.instance().changeMode(context, ThemeMode.system);
        } else if (value == ProfileMenu.modeLight) {
          // light
          ThemeManager.instance().changeMode(context, ThemeMode.light);
        } else if (value == ProfileMenu.modeDark) {
          // dark
          ThemeManager.instance().changeMode(context, ThemeMode.dark);
        }
      },
    );
  }

  PopupMenuItem<ProfileMenu> _buildMenuItem(
    ProfileMenu menu,
    String text, {
    Widget? leading,
    Widget? trailing,
    void Function()? tap,
  }) {
    return PopupMenuItem(
      value: menu,
      child: ListTile(
        title: Text(text),
        leading: leading,
        trailing: trailing,
        onTap: tap,
      ),
    );
  }

  Widget _buildSvg(String asset) {
    return SvgPicture.asset(
      asset,
      width: 24,
      height: 24,
    );
  }

  Widget _buildIcon(IconData icon, Color color) {
    return Icon(icon, color: color);
  }
}
