import 'package:json_annotation/json_annotation.dart';
import 'member_profile.dart';

part 'member_credential.g.dart';

/// Member's credential data.
@JsonSerializable()
class MemberCredential {
  String? id;

  /// Link to [MemberProfile]{@link MemberProfile.html}.
  MemberProfile? profile;

  // ----------------------------------------------------------------------

  // Password

  String? salt;

  String? hash;

  /// Sign-in token.
  String? token;

  /// Last sign-in date/time.
  DateTime? begin;

  /// Date/time to update expiry time.
  DateTime? refresh;

  /// Session expiry date/time.
  DateTime? expire;

  /// Last sign-out date/time.
  DateTime? end;

  // ----------------------------------------------------------------------

  DateTime? created;

  DateTime? updated;

  // ----------------------------------------------------------------------

  /// Constructor.
  MemberCredential();

  /// From JSON.
  factory MemberCredential.fromJson(Map<String, dynamic> json) =>
      _$MemberCredentialFromJson(json);

  /// To JSON.
  Map<String, dynamic> toJson() => _$MemberCredentialToJson(this);

  // ----------------------------------------------------------------------

  @override
  String toString() {
    if (profile == null) {
      return "[$id]";
    } else {
      return "[owner $profile.id/$profile.mail, $id]";
    }
  }
}
