// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'member_profile.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MemberProfile _$MemberProfileFromJson(Map<String, dynamic> json) =>
    MemberProfile()
      ..id = json['id'] as String?
      ..mail = json['mail'] as String?
      ..begin =
          json['begin'] == null ? null : DateTime.parse(json['begin'] as String)
      ..disabled = json['disabled'] == null
          ? null
          : DateTime.parse(json['disabled'] as String)
      ..resigned = json['resigned'] == null
          ? null
          : DateTime.parse(json['resigned'] as String)
      ..googleMail = json['googleMail'] as String?
      ..googleSignIn = json['googleSignIn'] == null
          ? null
          : DateTime.parse(json['googleSignIn'] as String)
      ..lineMail = json['lineMail'] as String?
      ..lineSignIn = json['lineSignIn'] == null
          ? null
          : DateTime.parse(json['lineSignIn'] as String)
      ..extra = json['extra'] == null
          ? null
          : MemberProfileExtra.fromJson(json['extra'] as Map<String, dynamic>)
      ..credential = json['credential'] == null
          ? null
          : MemberCredential.fromJson(
              json['credential'] as Map<String, dynamic>)
      ..created = json['created'] == null
          ? null
          : DateTime.parse(json['created'] as String)
      ..updated = json['updated'] == null
          ? null
          : DateTime.parse(json['updated'] as String);

Map<String, dynamic> _$MemberProfileToJson(MemberProfile instance) =>
    <String, dynamic>{
      'id': instance.id,
      'mail': instance.mail,
      'begin': instance.begin?.toIso8601String(),
      'disabled': instance.disabled?.toIso8601String(),
      'resigned': instance.resigned?.toIso8601String(),
      'googleMail': instance.googleMail,
      'googleSignIn': instance.googleSignIn?.toIso8601String(),
      'lineMail': instance.lineMail,
      'lineSignIn': instance.lineSignIn?.toIso8601String(),
      'extra': instance.extra,
      'credential': instance.credential,
      'created': instance.created?.toIso8601String(),
      'updated': instance.updated?.toIso8601String(),
    };
