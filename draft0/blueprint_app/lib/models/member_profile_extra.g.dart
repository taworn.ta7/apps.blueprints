// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'member_profile_extra.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MemberProfileExtra _$MemberProfileExtraFromJson(Map<String, dynamic> json) =>
    MemberProfileExtra()
      ..id = json['id'] as String?
      ..profile = json['profile'] == null
          ? null
          : MemberProfile.fromJson(json['profile'] as Map<String, dynamic>)
      ..mime = json['mime'] as String?
      ..settings = json['settings'] as String?
      ..created = json['created'] == null
          ? null
          : DateTime.parse(json['created'] as String)
      ..updated = json['updated'] == null
          ? null
          : DateTime.parse(json['updated'] as String);

Map<String, dynamic> _$MemberProfileExtraToJson(MemberProfileExtra instance) =>
    <String, dynamic>{
      'id': instance.id,
      'profile': instance.profile,
      'mime': instance.mime,
      'settings': instance.settings,
      'created': instance.created?.toIso8601String(),
      'updated': instance.updated?.toIso8601String(),
    };
