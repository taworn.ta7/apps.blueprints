/// Member's entities constants.
class MemberEntityConsts {
  static const passwordMin = 4;
  static const passwordMax = 20;

  static const salt = 255;
  static const hash = 1024;
  static const confirmToken = 64;
  static const token = 128;

  static const mail = 250;

  static const abode = 200;
}
