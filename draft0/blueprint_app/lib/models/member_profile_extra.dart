//import 'dart:typed_data';
import 'package:json_annotation/json_annotation.dart';
import 'member_profile.dart';

part 'member_profile_extra.g.dart';

/// Member profile extra entity.
@JsonSerializable()
class MemberProfileExtra {
  String? id;

  /// Link to [MemberProfile]{@link MemberProfile.html}.
  MemberProfile? profile;

  // ----------------------------------------------------------------------

  /// Icon MIME.
  String? mime;

  /// Icon stored.
  //Uint8List? icon;

  /// Settings in JSON form.
  String? settings;

  // ----------------------------------------------------------------------

  DateTime? created;

  DateTime? updated;

  // ----------------------------------------------------------------------

  /// Constructor.
  MemberProfileExtra();

  /// From JSON.
  factory MemberProfileExtra.fromJson(Map<String, dynamic> json) =>
      _$MemberProfileExtraFromJson(json);

  /// To JSON.
  Map<String, dynamic> toJson() => _$MemberProfileExtraToJson(this);

  // ----------------------------------------------------------------------

  @override
  String toString() {
    if (profile == null) {
      return "[$id]";
    } else {
      return "[owner $profile.id/$profile.mail, $id]";
    }
  }
}
