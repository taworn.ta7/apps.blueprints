// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'member_credential.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MemberCredential _$MemberCredentialFromJson(Map<String, dynamic> json) =>
    MemberCredential()
      ..id = json['id'] as String?
      ..profile = json['profile'] == null
          ? null
          : MemberProfile.fromJson(json['profile'] as Map<String, dynamic>)
      ..salt = json['salt'] as String?
      ..hash = json['hash'] as String?
      ..token = json['token'] as String?
      ..begin =
          json['begin'] == null ? null : DateTime.parse(json['begin'] as String)
      ..refresh = json['refresh'] == null
          ? null
          : DateTime.parse(json['refresh'] as String)
      ..expire = json['expire'] == null
          ? null
          : DateTime.parse(json['expire'] as String)
      ..end = json['end'] == null ? null : DateTime.parse(json['end'] as String)
      ..created = json['created'] == null
          ? null
          : DateTime.parse(json['created'] as String)
      ..updated = json['updated'] == null
          ? null
          : DateTime.parse(json['updated'] as String);

Map<String, dynamic> _$MemberCredentialToJson(MemberCredential instance) =>
    <String, dynamic>{
      'id': instance.id,
      'profile': instance.profile,
      'salt': instance.salt,
      'hash': instance.hash,
      'token': instance.token,
      'begin': instance.begin?.toIso8601String(),
      'refresh': instance.refresh?.toIso8601String(),
      'expire': instance.expire?.toIso8601String(),
      'end': instance.end?.toIso8601String(),
      'created': instance.created?.toIso8601String(),
      'updated': instance.updated?.toIso8601String(),
    };
