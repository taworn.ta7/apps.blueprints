import 'package:json_annotation/json_annotation.dart';
import 'member_profile_extra.dart';
import 'member_credential.dart';

part 'member_profile.g.dart';

/// Member profile entity.
@JsonSerializable()
class MemberProfile {
  String? id;

  // ----------------------------------------------------------------------

  /// Your personal mail.
  String? mail;

  /// Created date/time for sign-up with our site.
  DateTime? begin;

  /// Disabled by admin.
  DateTime? disabled;

  /// Resigned by this member.
  DateTime? resigned;

  // ----------------------------------------------------------------------

  /// Google sign-in mail.
  String? googleMail;

  /// Google sign-in timestamp.
  DateTime? googleSignIn;

  // ----------------------------------------------------------------------

  /// Line sign-in mail.
  String? lineMail;

  /// Line sign-in timestamp.
  DateTime? lineSignIn;

  // ----------------------------------------------------------------------

  /// Link to [MemberProfileExtra]{@link MemberProfileExtra.html}.
  MemberProfileExtra? extra;

  /// Link to [MemberCredential]{@link MemberCredential.html}.
  MemberCredential? credential;

  // ----------------------------------------------------------------------

  DateTime? created;

  DateTime? updated;

  // ----------------------------------------------------------------------

  /// Constructor.
  MemberProfile();

  /// From JSON.
  factory MemberProfile.fromJson(Map<String, dynamic> json) =>
      _$MemberProfileFromJson(json);

  /// To JSON.
  Map<String, dynamic> toJson() => _$MemberProfileToJson(this);

  // ----------------------------------------------------------------------

  @override
  String toString() {
    return "[$id/$mail]";
  }
}
