part of 'translations.g.dart';

// Path: <root>
class _TranslationsTh implements _TranslationsEn {
  /// You can call this constructor and build your own translation instance of this locale.
  /// Constructing via the enum [AppLocale.build] is preferred.
  _TranslationsTh.build(
      {Map<String, Node>? overrides,
      PluralResolver? cardinalResolver,
      PluralResolver? ordinalResolver})
      : assert(overrides == null,
            'Set "translation_overrides: true" in order to enable this feature.'),
        $meta = TranslationMetadata(
          locale: AppLocale.th,
          overrides: overrides ?? {},
          cardinalResolver: cardinalResolver,
          ordinalResolver: ordinalResolver,
        ) {
    $meta.setFlatMapFunction(_flatMapFunction);
  }

  /// Metadata for the translations of <th>.
  @override
  final TranslationMetadata<AppLocale, _TranslationsEn> $meta;

  /// Access flat map
  @override
  dynamic operator [](String key) => $meta.getTranslation(key);

  @override
  late final _TranslationsTh _root = this; // ignore: unused_field

  // Translations
  @override
  late final _TranslationsMessageBoxTh messageBox =
      _TranslationsMessageBoxTh._(_root);
  @override
  late final _TranslationsStringsTh strings = _TranslationsStringsTh._(_root);
  @override
  late final _TranslationsWaitBoxTh waitBox = _TranslationsWaitBoxTh._(_root);
}

// Path: messageBox
class _TranslationsMessageBoxTh implements _TranslationsMessageBoxEn {
  _TranslationsMessageBoxTh._(this._root);

  @override
  final _TranslationsTh _root; // ignore: unused_field

  // Translations
  @override
  String get close => 'ปิด';
  @override
  String get ok => 'ตกลง';
  @override
  String get cancel => 'ยกเลิก';
  @override
  String get yes => 'ใช่';
  @override
  String get no => 'ไม่ใช่';
  @override
  String get retry => 'ลองใหม่';
  @override
  String get info => 'ข้อมูลข่าวสาร';
  @override
  String get warning => 'แจ้งเตือน';
  @override
  String get error => 'เกิดข้อผิดพลาด';
  @override
  String get question => 'คำถาม';
}

// Path: strings
class _TranslationsStringsTh implements _TranslationsStringsEn {
  _TranslationsStringsTh._(this._root);

  @override
  final _TranslationsTh _root; // ignore: unused_field

  // Translations
  @override
  String get app => 'พิมพ์เขียว';
  @override
  late final _TranslationsStringsLocaleTh locale =
      _TranslationsStringsLocaleTh._(_root);
  @override
  late final _TranslationsStringsModeTh mode =
      _TranslationsStringsModeTh._(_root);
  @override
  late final _TranslationsStringsPagesTh pages =
      _TranslationsStringsPagesTh._(_root);
  @override
  late final _TranslationsStringsHomePageTh homePage =
      _TranslationsStringsHomePageTh._(_root);
  @override
  late final _TranslationsStringsTestPageTh testPage =
      _TranslationsStringsTestPageTh._(_root);
  @override
  late final _TranslationsStringsSettingsPageTh settingsPage =
      _TranslationsStringsSettingsPageTh._(_root);
}

// Path: waitBox
class _TranslationsWaitBoxTh implements _TranslationsWaitBoxEn {
  _TranslationsWaitBoxTh._(this._root);

  @override
  final _TranslationsTh _root; // ignore: unused_field

  // Translations
  @override
  String get message => 'โปรดรอสักครู่...';
}

// Path: strings.locale
class _TranslationsStringsLocaleTh implements _TranslationsStringsLocaleEn {
  _TranslationsStringsLocaleTh._(this._root);

  @override
  final _TranslationsTh _root; // ignore: unused_field

  // Translations
  @override
  String get en => 'อังกฤษ';
  @override
  String get th => 'ไทย';
}

// Path: strings.mode
class _TranslationsStringsModeTh implements _TranslationsStringsModeEn {
  _TranslationsStringsModeTh._(this._root);

  @override
  final _TranslationsTh _root; // ignore: unused_field

  // Translations
  @override
  String get default_ => 'ตามเครื่อง';
  @override
  String get light => 'สว่าง';
  @override
  String get dark => 'มืด';
}

// Path: strings.pages
class _TranslationsStringsPagesTh implements _TranslationsStringsPagesEn {
  _TranslationsStringsPagesTh._(this._root);

  @override
  final _TranslationsTh _root; // ignore: unused_field

  // Translations
  @override
  String get home => 'หน้าแรก';
  @override
  String get test => 'ทดสอบ';
  @override
  String get settings => 'การตั้งค่า';
}

// Path: strings.homePage
class _TranslationsStringsHomePageTh implements _TranslationsStringsHomePageEn {
  _TranslationsStringsHomePageTh._(this._root);

  @override
  final _TranslationsTh _root; // ignore: unused_field

  // Translations
  @override
  String get title => 'พิมพ์เขียว';
  @override
  String get hello => 'อรุณสวัสดิ์ ^_^';
}

// Path: strings.testPage
class _TranslationsStringsTestPageTh implements _TranslationsStringsTestPageEn {
  _TranslationsStringsTestPageTh._(this._root);

  @override
  final _TranslationsTh _root; // ignore: unused_field

  // Translations
  @override
  String get title => 'ทดสอบ';
  @override
  String get messageBox => 'กล่องข้อความ';
  @override
  String get close => 'ปิด';
  @override
  String get ok => 'ตกลง';
  @override
  String get okCancel => 'ตกลง/ยกเลิก';
  @override
  String get yesNo => 'ใช่/ไม่ใช่';
  @override
  String get retryCancel => 'ลองใหม่/ยกเลิก';
  @override
  String get info => 'ข้อมูลข่าวสาร';
  @override
  String get warning => 'แจ้งเตือน';
  @override
  String get error => 'เกิดข้อผิดพลาด';
  @override
  String get question => 'คำถาม';
  @override
  String get content =>
      'ใช่ไม่ใช่ ใช่ไม่ใช่ ใช่ไม่ใช่ ใช่ไม่ใช่ ใช่ไม่ใช่ ใช่ไม่ใช่ ใช่ไม่ใช่ ใช่ไม่ใช่ ใช่ไม่ใช่ ใช่ไม่ใช่ ใช่ไม่ใช่ ใช่ไม่ใช่ ใช่ไม่ใช่ ใช่ไม่ใช่ ใช่ไม่ใช่ ใช่ไม่ใช่ ใช่ไม่ใช่ ใช่ไม่ใช่\n';
  @override
  String get waitBox => 'กล่องรอ';
  @override
  String get wait => 'กรุณารอ...';
}

// Path: strings.settingsPage
class _TranslationsStringsSettingsPageTh
    implements _TranslationsStringsSettingsPageEn {
  _TranslationsStringsSettingsPageTh._(this._root);

  @override
  final _TranslationsTh _root; // ignore: unused_field

  // Translations
  @override
  String get title => 'การตั้งค่า';
  @override
  String get theme => 'เลือกธีมสี';
  @override
  List<String> get colors => [
        'คราม',
        'ฟ้าอ่อน',
        'น้ำเงินนกเป็ดน้ำ',
        'เขียว',
        'อำพัน',
        'ส้มเข้ม',
        'แดง',
        'ชมพู',
        'ม่วง',
        'ม่วงเข้ม',
      ];
}
