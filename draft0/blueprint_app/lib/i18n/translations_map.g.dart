part of 'translations.g.dart';

/// Flat map(s) containing all translations.
/// Only for edge cases! For simple maps, use the map function of this library.

extension on _TranslationsEn {
  dynamic _flatMapFunction(String path) {
    switch (path) {
      case 'messageBox.close':
        return 'Close';
      case 'messageBox.ok':
        return 'OK';
      case 'messageBox.cancel':
        return 'Cancel';
      case 'messageBox.yes':
        return 'Yes';
      case 'messageBox.no':
        return 'No';
      case 'messageBox.retry':
        return 'Retry';
      case 'messageBox.info':
        return 'Information';
      case 'messageBox.warning':
        return 'Warning';
      case 'messageBox.error':
        return 'Error';
      case 'messageBox.question':
        return 'Question';
      case 'strings.app':
        return 'Blueprint App';
      case 'strings.locale.en':
        return 'English';
      case 'strings.locale.th':
        return 'Thai';
      case 'strings.mode.default_':
        return 'Default';
      case 'strings.mode.light':
        return 'Light';
      case 'strings.mode.dark':
        return 'Dark';
      case 'strings.pages.home':
        return 'Home';
      case 'strings.pages.test':
        return 'Test';
      case 'strings.pages.settings':
        return 'Settings';
      case 'strings.homePage.title':
        return 'Blueprint App';
      case 'strings.homePage.hello':
        return 'Hello, world ^-^';
      case 'strings.testPage.title':
        return 'Testing';
      case 'strings.testPage.messageBox':
        return 'Message Box';
      case 'strings.testPage.close':
        return 'Close';
      case 'strings.testPage.ok':
        return 'OK';
      case 'strings.testPage.okCancel':
        return 'OK/Cancel';
      case 'strings.testPage.yesNo':
        return 'Yes/No';
      case 'strings.testPage.retryCancel':
        return 'Retry/Cancel';
      case 'strings.testPage.info':
        return 'Information';
      case 'strings.testPage.warning':
        return 'Warning';
      case 'strings.testPage.error':
        return 'Error';
      case 'strings.testPage.question':
        return 'Question';
      case 'strings.testPage.content':
        return 'Aaaaaa Bbbbbb Cccccc Dddddd Eeeeee Ffffff Aaaaaa Bbbbbb Cccccc Dddddd Eeeeee Ffffff Aaaaaa Bbbbbb Cccccc Dddddd Eeeeee Ffffff.\n';
      case 'strings.testPage.waitBox':
        return 'Wait Box';
      case 'strings.testPage.wait':
        return 'Please wait...';
      case 'strings.settingsPage.title':
        return 'Settings';
      case 'strings.settingsPage.theme':
        return 'Theme Selection';
      case 'strings.settingsPage.colors.0':
        return 'Indigo';
      case 'strings.settingsPage.colors.1':
        return 'Light Blue';
      case 'strings.settingsPage.colors.2':
        return 'Teal';
      case 'strings.settingsPage.colors.3':
        return 'Green';
      case 'strings.settingsPage.colors.4':
        return 'Amber';
      case 'strings.settingsPage.colors.5':
        return 'Deep Orange';
      case 'strings.settingsPage.colors.6':
        return 'Red';
      case 'strings.settingsPage.colors.7':
        return 'Pink';
      case 'strings.settingsPage.colors.8':
        return 'Purple';
      case 'strings.settingsPage.colors.9':
        return 'Deep Purple';
      case 'waitBox.message':
        return 'Please wait...';
      default:
        return null;
    }
  }
}

extension on _TranslationsTh {
  dynamic _flatMapFunction(String path) {
    switch (path) {
      case 'messageBox.close':
        return 'ปิด';
      case 'messageBox.ok':
        return 'ตกลง';
      case 'messageBox.cancel':
        return 'ยกเลิก';
      case 'messageBox.yes':
        return 'ใช่';
      case 'messageBox.no':
        return 'ไม่ใช่';
      case 'messageBox.retry':
        return 'ลองใหม่';
      case 'messageBox.info':
        return 'ข้อมูลข่าวสาร';
      case 'messageBox.warning':
        return 'แจ้งเตือน';
      case 'messageBox.error':
        return 'เกิดข้อผิดพลาด';
      case 'messageBox.question':
        return 'คำถาม';
      case 'strings.app':
        return 'พิมพ์เขียว';
      case 'strings.locale.en':
        return 'อังกฤษ';
      case 'strings.locale.th':
        return 'ไทย';
      case 'strings.mode.default_':
        return 'ตามเครื่อง';
      case 'strings.mode.light':
        return 'สว่าง';
      case 'strings.mode.dark':
        return 'มืด';
      case 'strings.pages.home':
        return 'หน้าแรก';
      case 'strings.pages.test':
        return 'ทดสอบ';
      case 'strings.pages.settings':
        return 'การตั้งค่า';
      case 'strings.homePage.title':
        return 'พิมพ์เขียว';
      case 'strings.homePage.hello':
        return 'อรุณสวัสดิ์ ^_^';
      case 'strings.testPage.title':
        return 'ทดสอบ';
      case 'strings.testPage.messageBox':
        return 'กล่องข้อความ';
      case 'strings.testPage.close':
        return 'ปิด';
      case 'strings.testPage.ok':
        return 'ตกลง';
      case 'strings.testPage.okCancel':
        return 'ตกลง/ยกเลิก';
      case 'strings.testPage.yesNo':
        return 'ใช่/ไม่ใช่';
      case 'strings.testPage.retryCancel':
        return 'ลองใหม่/ยกเลิก';
      case 'strings.testPage.info':
        return 'ข้อมูลข่าวสาร';
      case 'strings.testPage.warning':
        return 'แจ้งเตือน';
      case 'strings.testPage.error':
        return 'เกิดข้อผิดพลาด';
      case 'strings.testPage.question':
        return 'คำถาม';
      case 'strings.testPage.content':
        return 'ใช่ไม่ใช่ ใช่ไม่ใช่ ใช่ไม่ใช่ ใช่ไม่ใช่ ใช่ไม่ใช่ ใช่ไม่ใช่ ใช่ไม่ใช่ ใช่ไม่ใช่ ใช่ไม่ใช่ ใช่ไม่ใช่ ใช่ไม่ใช่ ใช่ไม่ใช่ ใช่ไม่ใช่ ใช่ไม่ใช่ ใช่ไม่ใช่ ใช่ไม่ใช่ ใช่ไม่ใช่ ใช่ไม่ใช่\n';
      case 'strings.testPage.waitBox':
        return 'กล่องรอ';
      case 'strings.testPage.wait':
        return 'กรุณารอ...';
      case 'strings.settingsPage.title':
        return 'การตั้งค่า';
      case 'strings.settingsPage.theme':
        return 'เลือกธีมสี';
      case 'strings.settingsPage.colors.0':
        return 'คราม';
      case 'strings.settingsPage.colors.1':
        return 'ฟ้าอ่อน';
      case 'strings.settingsPage.colors.2':
        return 'น้ำเงินนกเป็ดน้ำ';
      case 'strings.settingsPage.colors.3':
        return 'เขียว';
      case 'strings.settingsPage.colors.4':
        return 'อำพัน';
      case 'strings.settingsPage.colors.5':
        return 'ส้มเข้ม';
      case 'strings.settingsPage.colors.6':
        return 'แดง';
      case 'strings.settingsPage.colors.7':
        return 'ชมพู';
      case 'strings.settingsPage.colors.8':
        return 'ม่วง';
      case 'strings.settingsPage.colors.9':
        return 'ม่วงเข้ม';
      case 'waitBox.message':
        return 'โปรดรอสักครู่...';
      default:
        return null;
    }
  }
}
