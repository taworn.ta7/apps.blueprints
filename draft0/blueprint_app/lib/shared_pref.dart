import 'package:logging/logging.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import './localization.dart';
import './theme_manager.dart';

/// SharedPref service singleton class.
class SharedPref {
  static SharedPref? _instance;

  static SharedPref instance() {
    _instance ??= SharedPref();
    return _instance!;
  }

  // ----------------------------------------------------------------------

  static final log = Logger('SharedPref');

  /// Constructor.
  SharedPref();

  // ----------------------------------------------------------------------

  Future<void> load() async {
    final prefs = await SharedPreferences.getInstance();

    // loads locale
    var locale = prefs.getString('locale') ?? '';

    // loads mode
    var i = prefs.getInt('mode') ?? 0;
    var mode = ThemeMode.system;
    if (i > 0) {
      mode = ThemeMode.light;
    } else if (i < 0) {
      mode = ThemeMode.dark;
    }

    // loads theme
    var theme = prefs.getInt('theme') ?? 0;
    Localization.instance().load(locale);
    ThemeManager.instance().load(mode, theme);
  }

  // ----------------------------------------------------------------------

  void saveLocale(String locale) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString('locale', locale);
  }

  void saveMode(ThemeMode mode) async {
    final prefs = await SharedPreferences.getInstance();
    var i = 0; // mode == ThemeMode.system
    if (mode == ThemeMode.light) {
      i = 1;
    } else if (mode == ThemeMode.dark) {
      i = -1;
    }
    await prefs.setInt('mode', i);
  }

  void saveTheme(int theme) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setInt('theme', theme);
  }
}
