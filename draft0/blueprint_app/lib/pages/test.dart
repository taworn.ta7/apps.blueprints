import 'package:logging/logging.dart';
import 'package:flutter/material.dart';
import '../i18n/translations.g.dart';
import '../widgets/message_box.dart';
import '../widgets/wait_box.dart';
import '../ui/custom_app_bar.dart';
import '../ui/custom_drawer.dart';

/// TestPage class.
class TestPage extends StatefulWidget {
  const TestPage({super.key});

  @override
  State<TestPage> createState() => _TestPageState();
}

// ----------------------------------------------------------------------

class _TestPageState extends State<TestPage> with RestorationMixin {
  static final log = Logger('TestPage');

  @override
  void initState() {
    super.initState();
    log.fine("$this initState()");
  }

  @override
  void dispose() {
    log.fine("$this dispose()");
    super.dispose();
  }

  @override
  String? get restorationId => 'TestPage';

  @override
  void restoreState(RestorationBucket? oldBucket, bool initialRestore) {}

  // ----------------------------------------------------------------------

  /// Build widget tree.
  @override
  Widget build(BuildContext context) {
    final t = Translations.of(context).strings;

    return Scaffold(
      // AppBar
      appBar: CustomAppBar(
        title: t.testPage.title,
      ),

      // Drawer
      drawer: const CustomDrawer(),

      // body
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(32),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              // message box header
              Text(
                t.testPage.messageBox,
                style:
                    const TextStyle(fontSize: 28, fontWeight: FontWeight.bold),
              ),
              const Padding(padding: EdgeInsets.only(bottom: 16)),

              // close
              _buildButton(
                text: t.testPage.close,
                onPressed: () async => _returnFromMessageBox(
                  await MessageBox.show(
                    context: context,
                    message: t.testPage.content,
                    caption: t.testPage.close,
                    options: MessageBoxOptions(
                      barrierDismissible: true,
                      type: MessageBoxType.close,
                    ),
                  ),
                ),
              ),
              const Padding(padding: EdgeInsets.only(bottom: 16)),

              // ok
              _buildButton(
                text: t.testPage.ok,
                onPressed: () async => _returnFromMessageBox(
                  await MessageBox.show(
                    context: context,
                    message: t.testPage.content,
                    caption: t.testPage.ok,
                    options: MessageBoxOptions(
                      barrierDismissible: true,
                      type: MessageBoxType.ok,
                    ),
                  ),
                ),
              ),
              const Padding(padding: EdgeInsets.only(bottom: 16)),

              // ok, cancel
              _buildButton(
                text: t.testPage.okCancel,
                onPressed: () async => _returnFromMessageBox(
                  await MessageBox.show(
                    context: context,
                    message: t.testPage.content,
                    caption: t.testPage.okCancel,
                    options: MessageBoxOptions(
                      barrierDismissible: false,
                      type: MessageBoxType.okCancel,
                      button1Negative: true,
                    ),
                  ),
                ),
              ),
              const Padding(padding: EdgeInsets.only(bottom: 16)),

              // yes, no
              _buildButton(
                text: t.testPage.yesNo,
                onPressed: () async => _returnFromMessageBox(
                  await MessageBox.show(
                    context: context,
                    message: t.testPage.content,
                    caption: t.testPage.yesNo,
                    options: MessageBoxOptions(
                      barrierDismissible: false,
                      type: MessageBoxType.yesNo,
                      button0Negative: true,
                    ),
                  ),
                ),
              ),
              const Padding(padding: EdgeInsets.only(bottom: 16)),

              // retry, cancel
              _buildButton(
                text: t.testPage.retryCancel,
                onPressed: () async => _returnFromMessageBox(
                  await MessageBox.show(
                    context: context,
                    message: t.testPage.content,
                    caption: t.testPage.retryCancel,
                    options: MessageBoxOptions(
                      barrierDismissible: false,
                      type: MessageBoxType.retryCancel,
                      button1Negative: true,
                    ),
                  ),
                ),
              ),
              const Padding(padding: EdgeInsets.only(bottom: 16)),

              // info
              _buildButton(
                text: t.testPage.info,
                onPressed: () async => _returnFromMessageBox(
                  await MessageBox.info(
                    context,
                    t.testPage.content,
                  ),
                ),
              ),
              const Padding(padding: EdgeInsets.only(bottom: 16)),

              // warning
              _buildButton(
                text: t.testPage.warning,
                onPressed: () async => _returnFromMessageBox(
                  await MessageBox.warning(
                    context,
                    t.testPage.content,
                  ),
                ),
              ),
              const Padding(padding: EdgeInsets.only(bottom: 16)),

              // error
              _buildButton(
                text: t.testPage.error,
                onPressed: () async => _returnFromMessageBox(
                  await MessageBox.error(
                    context,
                    t.testPage.content,
                    button0Negative: true,
                  ),
                ),
              ),
              const Padding(padding: EdgeInsets.only(bottom: 16)),

              // question
              _buildButton(
                text: t.testPage.question,
                onPressed: () async => _returnFromMessageBox(
                  await MessageBox.question(
                    context,
                    t.testPage.content,
                    button1Negative: true,
                  ),
                ),
              ),
              const Padding(padding: EdgeInsets.only(bottom: 16)),

              // wait box header
              Text(
                t.testPage.waitBox,
                style:
                    const TextStyle(fontSize: 28, fontWeight: FontWeight.bold),
              ),
              const Padding(padding: EdgeInsets.only(bottom: 16)),

              // wait
              _buildButton(
                text: t.testPage.wait,
                onPressed: () async => _returnFromWaitBox(
                  await WaitBox.show<bool>(
                    context,
                    () async {
                      await Future.delayed(const Duration(seconds: 3));
                      return true;
                    },
                  ),
                ),
              ),
              const Padding(padding: EdgeInsets.only(bottom: 16)),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildButton({
    required String text,
    required void Function() onPressed,
  }) {
    return ElevatedButton(
      onPressed: onPressed,
      child: Container(
        padding: const EdgeInsets.all(12),
        child: Text(text),
      ),
    );
  }

  void _returnFromMessageBox(bool? result) {
    log.finer("message box closed, return $result");
  }

  void _returnFromWaitBox(bool? result) {
    log.finer("wait box closed, return $result");
  }
}
