export default defineI18nConfig(() => ({
	legacy: false,  // you must set to false, if you want to use Composition API
	locale: 'en',
	fallbackLocale: 'en',
	fallbackWarn: false,
	missingWarn: false,
	globalInjection: true,
	messages: {
		en: {
			'app': "Blueprint",
			'path': {
				'home': "Home",
				'test-boxes': "Test Boxes",
				'test-boxes-shared': "Test Boxes Shared",
			},
		},
		th: {
			'app': "พิมพ์เขียว",
			'path': {
				'home': "หน้าแรก",
				'test-boxes': "ทดสอบกล่อง",
				'test-boxes-shared': "ทดสอบกล่องใช้ร่วมกัน",
			},
		},
	},
}))
