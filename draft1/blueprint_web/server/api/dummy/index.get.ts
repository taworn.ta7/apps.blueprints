//import { logger } from '../../utils/logger'

export default defineEventHandler(async (event) => {
	throw createError({
		status: 500,
		statusMessage: "Internal Server Error",
		message: "Not implemented!",
		data: {
			path: event.path,
			requestId: event.context.requestId,
			startTime: new Date(event.context.startTime),
		},
	})
})
