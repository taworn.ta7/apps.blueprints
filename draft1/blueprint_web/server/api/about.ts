//import { logger } from '../utils/logger'

export default defineEventHandler(async (event) => {
	logger.silly(`let's go`)
	logger.debug(`go on`)
	logger.verbose(`on and on`)
	logger.http(`with HTTP protocol`)
	logger.info(`with information`)
	logger.warn(`alive and kicking`)
	logger.error(`I'm dieing t_t`)
	return {
		app: process.env.npm_package_name,
		version: process.env.npm_package_version,
	}
})
