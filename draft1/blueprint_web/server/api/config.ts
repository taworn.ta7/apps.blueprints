//import { logger } from '../utils/logger'

export default defineEventHandler(async (event) => {
	/*
	const runtimeConfig = useRuntimeConfig()
	logger.debug(`runtimeConfig: ${JSON.stringify(runtimeConfig, null, 2)}\n`)
	const appConfig = useAppConfig()
	logger.http(`appConfig: ${JSON.stringify(appConfig, null, 2)}\n`)
	*/
	const env = process.env
	return {
		config: {
			// logging folder
			LOG_DIR: env.LOG_DIR,

			// logging outputs
			LOG_TO_CONSOLE: env.LOG_TO_CONSOLE,
			LOG_TO_FILE: env.LOG_TO_FILE,

			// storage folder
			STORAGE_DIR: env.STORAGE_DIR,

			// upload folder
			UPLOAD_DIR: env.UPLOAD_DIR,

			// database
			DB_USE: env.DB_USE,
			DB_HOST: env.DB_HOST,
			DB_PORT: env.DB_PORT,
			DB_USER: env.DB_USER,
			DB_PASS: '****',
			DB_NAME: env.DB_NAME,
			DB_FILE: env.DB_FILE,

			// test database
			TESTDB_USE: env.TESTDB_USE,
			TESTDB_HOST: env.TESTDB_HOST,
			TESTDB_PORT: env.TESTDB_PORT,
			TESTDB_USER: env.TESTDB_USER,
			TESTDB_PASS: '****',
			TESTDB_NAME: env.TESTDB_NAME,
			TESTDB_FILE: env.TESTDB_FILE,

			// mail
			MAIL_HOST: env.MAIL_HOST,
			MAIL_PORT: env.MAIL_PORT,
			MAIL_USER: env.MAIL_USER,
			MAIL_PASS: '****',
			MAIL_ADMIN: env.MAIL_ADMIN,

			// HTTP port
			HTTP_PORT: env.HTTP_PORT,

			// days to keep before deleting old data?
			DAYS_TO_KEEP_LOGS: env.DAYS_TO_KEEP_LOGS,
			DAYS_TO_KEEP_DBLOGS: env.DAYS_TO_KEEP_DBLOGS,
		},
	}
})
