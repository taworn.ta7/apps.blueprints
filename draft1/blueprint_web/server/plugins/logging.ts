import { H3Error } from 'h3'
//import { logger } from '../utils/logger'

export default defineNitroPlugin((nitroApp) => {

	let requestId = 0

	function generateRequestId(): string {
		const result = requestId++
		if (requestId >= 1000000000)
			requestId = 0
		return result.toString().padStart(9, '0')
	}

	nitroApp.hooks.hook('request', (event) => {
		event.context.requestId = generateRequestId()
		event.context.startTime = Date.now()
		logger.http(`${event.context.requestId}; [${event.method}] ${event.path}`)
	})

	nitroApp.hooks.hook('afterResponse', (event/*, { body }*/) => {
		logger.http(`${event.context.requestId}; success, time used ${Date.now() - event.context.startTime} ms`)
	})

	nitroApp.hooks.hook('error', (error, { event }) => {
		if (event) {
			const e = error as H3Error
			if (e) {
				if (e.statusMessage !== e.message)
					logger.warn(`${event.context.requestId}; ${e.statusCode} ${e.statusMessage}, ${e.message}, data=${JSON.stringify(e.data, null, 2)}`)
				else
					logger.warn(`${event.context.requestId}; ${e.statusCode} ${e.statusMessage}, data=${JSON.stringify(e.data, null, 2)}`)
			}
			else
				logger.warn(`${event.context.requestId}; ${error}`)
		}
		else
			logger.error(`${error}`)
	})

})
