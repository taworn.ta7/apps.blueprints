/*import dotenv from 'dotenv'
dotenv.config({
	path: [
		'.env.override',
		'.env',
	],
})
*/
import * as path from 'node:path'
import * as url from 'node:url'
//import { consola } from 'consola'
//import { logger } from '../utils/logger'
import 'reflect-metadata'
import { DataSource } from 'typeorm'
import clearLogs from '../tasks/clear_logs'

const delayTime = 1
const intervalTime = 8 * 60 * 60 * 1000

export default defineNitroPlugin(async (nitroApp) => {
	const __filename = url.fileURLToPath(import.meta.url)
	const __dirname = path.dirname(__filename)
	const env = <any>process.env

	// initializing tasks
	const logDir = path.resolve(path.join(__dirname, '..', '..', env.LOG_DIR))
	logger.log('verbose', `logs folder: ${logDir}`)
	logger.verbose(`logs to console: ${!!+env.LOG_TO_CONSOLE}`)
	logger.verbose(`logs to file: ${!!+env.LOG_TO_FILE}`)
	logger.debug(`days to keep logs: ${+env.DAYS_TO_KEEP_LOGS} day(s)`)
	logger.debug(`days to keep dummy data: ${+env.DAYS_TO_KEEP_DUMMY} day(s)`)

	// initializing database
	{
		const type = <'sqlite' | 'mysql'>env.DB_USE;
		let db: string;
		let host: string;
		let port: number;
		let username: string;
		let password: string;
		if (env.TESTDB_USE) {
			if (type === 'sqlite')
				db = path.join(__dirname, '..', '..', env.STORAGE_DIR, env.TESTDB_FILE);
			else
				db = env.TESTDB_NAME;
			host = env.TESTDB_HOST;
			port = +env.TESTDB_PORT;
			username = env.TESTDB_USER;
			password = env.TESTDB_PASS;
		}
		else {
			if (type === 'sqlite')
				db = path.join(__dirname, '..', '..', env.STORAGE_DIR, env.DB_FILE);
			else
				db = env.DB_NAME;
			host = env.DB_HOST;
			port = +env.DB_PORT;
			username = env.DB_USER;
			password = env.DB_PASS;
		}
		logger.debug(`connection type: ${type}`)
		if (type === 'sqlite')
			logger.debug(`connection type: ${db}`)
		else
			logger.debug(`connect to: ${host}:${port}, database=${db}, username=${username}`)
		const res = new DataSource({
			type,
			host,
			port,
			username,
			password,
			database: db,
			entities: [
				'dist/**/*.entity.{ts,js}',
			],
			synchronize: true,  // <-- change this to 'false' when release
			//retryAttempts: 0,
			multipleStatements: true,
			//logging: true,
		})
		await res.initialize()
	}

	// clearing function
	const callback = async () => {
		try {
			await clearLogs(+env.DAYS_TO_KEEP_LOGS, logDir)
			//await clearDummy(+env.DAYS_TO_KEEP_DUMMY)
			//
			// clearing more out of used data
			//
		}
		catch (e) {
			logger.error(`uncaught exception: ${e}`)
		}
		setTimeout(callback, intervalTime)
	}
	setTimeout(callback, delayTime)
})
