import { describe, expect, test } from '@jest/globals';
import * as crypto_ex from './crypto_ex';

describe("crypto, customized for this project", () => {

	test("setPassword('xyzw')", () => {
		const pass = crypto_ex.setPassword('xyzw');
		expect(pass.salt.length).toBe(32);
		expect(pass.hash.length).toBe(1024);
	});

	test("setPassword('_1_2_3_4')", () => {
		const pass = crypto_ex.setPassword('_1_2_3_4');
		expect(pass.salt.length).toBe(32);
		expect(pass.hash.length).toBe(1024);
	});

	test("validatePassword('xyzw', salt, hash)", () => {
		const pass = crypto_ex.setPassword('xyzw');
		expect(crypto_ex.validatePassword('xyzw', pass.salt, pass.hash)).toBeTruthy();
		expect(crypto_ex.validatePassword('wxyz', pass.salt, pass.hash)).toBeFalsy();
	});

	test("validatePassword('_1_2_3_4', salt, hash)", () => {
		const pass = crypto_ex.setPassword('_1_2_3_4');
		expect(crypto_ex.validatePassword('_1_2_3_4', pass.salt, pass.hash)).toBeTruthy();
		expect(crypto_ex.validatePassword('_1_2_3_', pass.salt, pass.hash)).toBeFalsy();
	});

	test("generateToken(n)", () => {
		const token = crypto_ex.generateToken(10);
		expect(token.length).toBe(10);
		const again = crypto_ex.generateToken(20);
		expect(again.length).toBe(20);
	});

});
