import {
	BadRequestException,
	ForbiddenException,
	NotFoundException,
	UnauthorizedException,
} from '@nestjs/common';


/**
 * Exception classes packed with error messages and constants to known more reason.
 */
export class Errors {

	/////////////////////////////////////////////////////////////////////////
	// Generic 400 Bad Request
	/////////////////////////////////////////////////////////////////////////

	static readonly uploadFail = 4000101;
	static readonly uploadIsNotType = 4000111;
	static readonly uploadIsNotTypeImage = 4000112;
	static readonly uploadIsTooBig = 4000121;

	static E400UploadFail() {
		return new BadRequestException({
			locales: {
				en: `Uploaded file is failed!`,
				th: `ไฟล์ที่ Upload ขึ้นมา ไม่สำเร็จ!`,
			},
			code: Errors.uploadFail,
		});
	}

	static E400UploadIsNotType() {
		return new BadRequestException({
			locales: {
				en: `Uploaded file is not a specify type!`,
				th: `ไฟล์ที่ Upload ขึ้นมา ไม่ใช่ไฟล์ชนิดที่ต้องการ!`,
			},
			code: Errors.uploadIsNotType,
		});
	}

	static E400UploadIsNotTypeImage() {
		return new BadRequestException({
			locales: {
				en: `Uploaded file is not an image file!`,
				th: `ไฟล์ที่ Upload ขึ้นมา ไม่ใช่ไฟล์รูปภาพ!`,
			},
			code: Errors.uploadIsNotTypeImage,
		});
	}

	static E400UploadIsTooBig(limit: string | number) {
		return new BadRequestException({
			locales: {
				en: `Uploaded file size is too big! (Limit ${limit})`,
				th: `ไฟล์ที่ Upload ขึ้นมา มีขนาดใหญ่เกินไป! (ขนาดใหญ่สุด ${limit})`,
			},
			code: Errors.uploadIsTooBig,
		});
	}


	/////////////////////////////////////////////////////////////////////////
	// Generic 403 Forbidden
	/////////////////////////////////////////////////////////////////////////

	static readonly dataExists = 4030001;
	static readonly dataInvalid = 4030002;
	static readonly dataExpire = 4030003;
	static readonly mailExists = 4030011;
	static readonly addressExists = 4030012;

	static E403DataExists() {
		return new ForbiddenException({
			locales: {
				en: `Data already exists!`,
				th: `ข้อมูลนี้ มีในระบบแล้ว!`,
			},
			code: Errors.dataExists,
		});
	}

	static E403DataInvalid() {
		return new ForbiddenException({
			locales: {
				en: `Data is invalid!`,
				th: `ข้อมูลนี้ ไม่ถูกต้อง!`,
			},
			code: Errors.dataInvalid,
		});
	}

	static E403DataExpire() {
		return new ForbiddenException({
			locales: {
				en: `Data expired!`,
				th: `ข้อมูลนี้ หมดเวลาใช้แล้ว!`,
			},
			code: Errors.dataExpire,
		});
	}

	static E403MailExists() {
		return new ForbiddenException({
			locales: {
				en: `E-mail already exists!`,
				th: `อีเมลนี้ มีในระบบแล้ว!`,
			},
			code: Errors.mailExists,
		});
	}

	static E403AddressExists() {
		return new ForbiddenException({
			locales: {
				en: `Address already exists!`,
				th: `ที่อยู่นี้ มีในระบบแล้ว!`,
			},
			code: Errors.addressExists,
		});
	}


	/////////////////////////////////////////////////////////////////////////
	// Generic 404 Not Found
	/////////////////////////////////////////////////////////////////////////

	static readonly dataNotFound = 4040001;
	static readonly mailNotFound = 4040011;
	static readonly addressNotFound = 4040012;

	static E404DataNotFound() {
		return new NotFoundException({
			locales: {
				en: `Data is not found!`,
				th: `ไม่พบข้อมูลนี้!`,
			},
			code: Errors.dataNotFound,
		});
	}

	static E404MailNotFound() {
		return new NotFoundException({
			locales: {
				en: `Data is not found!`,
				th: `ไม่พบอีเมลนี้!`,
			},
			code: Errors.mailNotFound,
		});
	}

	static E404AddressNotFound() {
		return new NotFoundException({
			locales: {
				en: `Address is not found!`,
				th: `ไม่พบข้อมูล ที่อยู่นี้!`,
			},
			code: Errors.addressNotFound,
		});
	}


	/////////////////////////////////////////////////////////////////////////
	// Precinct
	/////////////////////////////////////////////////////////////////////////

	static readonly precinctCantonNotFound = 10001;
	static readonly precinctDistrictNotFound = 10002;
	static readonly precinctProvinceNotFound = 10003;
	static readonly precinctSectorNotFound = 10004;
	static readonly precinctZipNotFound = 10005;
	static readonly precinctZipOrCantonInvalid = 10011;

	static E404PrecinctCantonNotFound() {
		return new NotFoundException({
			locales: {
				en: `Canton Id is not found!`,
				th: `ไม่พบข้อมูล รหัสตำบลนี้!`,
			},
			code: Errors.precinctCantonNotFound,
		});
	}

	static E404PrecinctDistrictNotFound() {
		return new NotFoundException({
			locales: {
				en: `District Id is not found!`,
				th: `ไม่พบข้อมูล รหัสอำเภอนี้!`,
			},
			code: Errors.precinctDistrictNotFound,
		});
	}

	static E404PrecinctProvinceNotFound() {
		return new NotFoundException({
			locales: {
				en: `Province Id is not found!`,
				th: `ไม่พบข้อมูล รหัสจังหวัดนี้!`,
			},
			code: Errors.precinctProvinceNotFound,
		});
	}

	static E404PrecinctSectorNotFound() {
		return new NotFoundException({
			locales: {
				en: `Sector Id is not found!`,
				th: `ไม่พบข้อมูล รหัสภาคนี้!`,
			},
			code: Errors.precinctSectorNotFound,
		});
	}

	static E404PrecinctZipNotFound() {
		return new NotFoundException({
			locales: {
				en: `ZIP Id is not found!`,
				th: `ไม่พบข้อมูล รหัสไปรษณีย์นี้!`,
			},
			code: Errors.precinctZipNotFound,
		});
	}

	static E404PrecinctZipOrCantonInvalid() {
		return new NotFoundException({
			locales: {
				en: `ZIP and/or canton is not found!`,
				th: `รหัสไปรษณีย์ หรือ ตำบล ไม่ถูกต้อง!`,
			},
			code: Errors.precinctZipOrCantonInvalid,
		});
	}


	/////////////////////////////////////////////////////////////////////////
	// Member
	/////////////////////////////////////////////////////////////////////////

	static readonly memberNotFound = 20001;
	static readonly memberMailOrPasswordInvalid = 20011;
	static readonly memberIsDisabledByAdmin = 20012;
	static readonly memberIsResigned = 20013;
	static readonly memberNeedSignIn = 20014;
	static readonly memberSignInTimeout = 20015;
	static readonly memberAddressIsLimited = 20021;

	static E404MemberNotFound() {
		return new NotFoundException({
			locales: {
				en: `Member profile is not found!`,
				th: `ไม่พบข้อมูล โปรไฟล์สมาชิกท่านนี้!`,
			},
			code: Errors.memberNotFound,
		});
	}

	static E401MemberMailOrPasswordInvalid() {
		return new UnauthorizedException({
			locales: {
				en: `Your member's e-mail or password is incorrect!`,
				th: `สมาชิกท่านนี้ ใส่อีเมลหรือรหัสผ่านไม่ถูกต้อง!`,
			},
			code: Errors.memberMailOrPasswordInvalid,
		});
	}

	static E403MemberIsDisabledByAdmin() {
		return new ForbiddenException({
			locales: {
				en: `Your membership is disabled!`,
				th: `สมาชิกท่านนี้ ถูกระงับการใช้การ!`,
			},
			code: Errors.memberIsDisabledByAdmin,
		});
	}

	static E403MemberIsResigned() {
		return new ForbiddenException({
			locales: {
				en: `Your membership is resigned!`,
				th: `สมาชิกท่านนี้ ลาออกจากระบบไปแล้ว!`,
			},
			code: Errors.memberIsResigned,
		});
	}

	static E401MemberNeedSignIn() {
		return new UnauthorizedException({
			locales: {
				en: `You member require sign-in!`,
				th: `สมาชิกท่านนี้ ต้อง sign-in เข้าระบบก่อน!`,
			},
			code: Errors.memberNeedSignIn,
		});
	}

	static E401MemberSignInTimeout() {
		return new UnauthorizedException({
			locales: {
				en: `Timeout, you need to sign-in again!`,
				th: `หมดเวลา, คุณต้อง sign-in เข้าระบบอีกครั้ง!`,
			},
			code: Errors.memberSignInTimeout,
		});
	}

	static E403MemberAddressIsLimited(limit: number) {
		return new ForbiddenException({
			locales: {
				en: `Member addresses must have limit to ${limit}!`,
				th: `ที่อยู่สมาชิก สามารถมีได้ ${limit} ที่อยู่ เท่านั้น!`,
			},
			code: Errors.memberAddressIsLimited,
		});
	}


	/////////////////////////////////////////////////////////////////////////
	// Caretaker
	/////////////////////////////////////////////////////////////////////////

	static readonly caretakerNotFound = 30001;
	static readonly caretakerMailOrPasswordInvalid = 30011;
	static readonly caretakerIsDisabledByAdmin = 30012;
	static readonly caretakerIsResigned = 30013;
	static readonly caretakerNeedSignIn = 30014;
	static readonly caretakerSignInTimeout = 30015;
	static readonly caretakerAddressIsLimited = 30021;
	static readonly caretakerRootRequired = 30031;
	static readonly caretakerNeedMorePriviledge = 30032;

	static E404CaretakerNotFound() {
		return new NotFoundException({
			locales: {
				en: `Caretaker profile is not found!`,
				th: `ไม่พบข้อมูล โปรไฟล์ผู้ดูแลท่านนี้!`,
			},
			code: Errors.caretakerNotFound,
		});
	}

	static E401CaretakerMailOrPasswordInvalid() {
		return new UnauthorizedException({
			locales: {
				en: `Your caretaker's e-mail or password is incorrect!`,
				th: `ผู้ดูแลท่านนี้ ใส่อีเมลหรือรหัสผ่านไม่ถูกต้อง!`,
			},
			code: Errors.caretakerMailOrPasswordInvalid,
		});
	}

	static E403CaretakerIsDisabledByAdmin() {
		return new ForbiddenException({
			locales: {
				en: `Your caretakership is disabled!`,
				th: `ผู้ดูแลท่านนี้ ถูกระงับการใช้การ!`,
			},
			code: Errors.caretakerIsDisabledByAdmin,
		});
	}

	static E403CaretakerIsResigned() {
		return new ForbiddenException({
			locales: {
				en: `Your caretakership is resigned!`,
				th: `ผู้ดูแลท่านนี้ ลาออกจากระบบไปแล้ว!`,
			},
			code: Errors.caretakerIsResigned,
		});
	}

	static E401CaretakerNeedSignIn() {
		return new UnauthorizedException({
			locales: {
				en: `Your caretaker require sign-in!`,
				th: `ผู้ดูแลท่านนี้ ต้อง sign-in เข้าระบบก่อน!`,
			},
			code: Errors.caretakerNeedSignIn,
		});
	}

	static E401CaretakerSignInTimeout() {
		return new UnauthorizedException({
			locales: {
				en: `Timeout, you need to sign-in again!`,
				th: `หมดเวลา, ผู้ดูแลท่านนี้ ต้อง sign-in เข้าระบบอีกครั้ง!`,
			},
			code: Errors.caretakerSignInTimeout,
		});
	}

	static E403CaretakerAddressIsLimited(limit: number) {
		return new ForbiddenException({
			locales: {
				en: `Caretaker addresses must have limit to ${limit}!`,
				th: `ที่อยู่ผู้ดูแล สามารถมีได้ ${limit} ที่อยู่ เท่านั้น!`,
			},
			code: Errors.caretakerAddressIsLimited,
		});
	}

	static E403CaretakerRootRequired() {
		return new ForbiddenException({
			locales: {
				en: `Must have caretaker 'root' privilege!`,
				th: `คุณต้องมี ผู้ดูแล root privilege!`,
			},
			code: Errors.caretakerRootRequired,
		});
	}

	static E403CaretakerNeedMorePriviledge() {
		return new ForbiddenException({
			locales: {
				en: `Require more privilege!`,
				th: `ต้องการ privilege สูงกว่านี้!`,
			},
			code: Errors.caretakerNeedMorePriviledge,
		});
	}

}
