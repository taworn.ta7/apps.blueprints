/**
 * Query string to convert to boolean.
 * 
 * This function to test with trimmed string to be '', 0, 0..0, or false to become false.
 */
export function queryStringToBool(str: string | null): boolean {
	if (!str)
		return false;

	const s = str.trim();
	if (s === '')
		return false;
	if (/0+/.test(s))
		return false;
	if (s.toLowerCase() === 'false')
		return false;

	return true;
}


/**
 * Regular expression of detecting mail or not.
 */
export const regexMail = /^[ \t]*(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})[ \t]*$/i;
