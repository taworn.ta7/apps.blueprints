import { describe, expect, test } from '@jest/globals';
import * as convert from './convert';

describe("convert utility", () => {

	test("convertBytes()", () => {
		expect(convert.convertBytes(0)).toBe("0 bytes");
		expect(convert.convertBytes(1)).toBe("1 bytes");
		expect(convert.convertBytes(1024)).toBe("1.0 KB");
		expect(convert.convertBytes(2047)).toBe("2.0 KB");
		expect(convert.convertBytes(2048)).toBe("2.0 KB");
		expect(convert.convertBytes(1024 * 1024)).toBe("1.0 MB");
		expect(convert.convertBytes(1024 * 1024 * 1024)).toBe("1.0 GB");
		expect(convert.convertBytes(1024 * 1024 * 1024 * 1024)).toBe("1.0 TB");
		expect(convert.convertBytes(1024 * 1024 * 1024 * 1024 * 1024)).toBe("1.0 PB");
	});

});
