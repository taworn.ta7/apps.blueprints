import { LoggerService } from '@nestjs/common';
import * as path from 'node:path';
import * as fs from 'node:fs';
import { Logger, createLogger, format, transports } from 'winston';
import 'winston-daily-rotate-file';


/**
 * A backend to real output log.
 */
export class CustomLogger implements LoggerService {

	logDir: string;
	logger: Logger;

	constructor() {
		this.logDir = path.resolve(path.join(__dirname, '..', process.env.LOG_DIR));
		if (!fs.existsSync(this.logDir)) {
			fs.mkdirSync(this.logDir);
		}

		let xports = [];
		if (+process.env.LOG_TO_CONSOLE) {
			xports.push(new transports.Console({
				format: format.combine(
					format.colorize({
						all: true,
						colors: {
							error: 'brightWhite bgRed',
							warn: 'brightRed',
							info: 'brightWhite',
							http: 'brightCyan',
							verbose: 'brightBlue',
							debug: 'brightGreen',
							silly: 'white',
						},
					}),
				),
			}));
		}
		if (+process.env.LOG_TO_FILE) {
			xports.push(new transports.DailyRotateFile({
				filename: `${this.logDir}/%DATE%.log`,
				datePattern: 'YYYYMMDD',
			}));
		}

		this.logger = createLogger({
			level: process.env.NODE_ENV === 'production' ? 'info' : 'silly',
			format: format.combine(
				format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss.SSS' }),
				format.printf(info => `${info.timestamp} ${info.level.substring(0, 3).toUpperCase()} ${info.message}`),
			),
			transports: xports,
		});
		this.logger.silly("Testing silly message.");
		this.logger.debug("Testing debug message.");
		this.logger.verbose("Testing verbose message.");
		this.logger.http("Testing http message.");
		this.logger.info("Testing info message.");
		this.logger.warn("Testing warn message.");
		this.logger.error("Testing error message.");
	}

	// ----------------------------------------------------------------------

	log(message: any, ...optionalParams: any[]) {
		this.logger.info(this.format(message, optionalParams.length > 0 ? optionalParams[0] : ''));
	}

	verbose(message: any, ...optionalParams: any[]) {
		this.logger.verbose(this.format(message, optionalParams ? (optionalParams.length > 0 ? optionalParams[0] : '') : ''));
	}

	debug(message: any, ...optionalParams: any[]) {
		this.logger.debug(this.format(message, optionalParams ? (optionalParams.length > 0 ? optionalParams[0] : '') : ''));
	}

	warn(message: any, ...optionalParams: any[]) {
		this.logger.warn(this.format(message, optionalParams ? (optionalParams.length > 0 ? optionalParams[0] : '') : ''));
	}

	error(message: any, ...optionalParams: any[]) {
		// NOTE:
		// Why this just optionalParams[1], every others has optionalParams[0]?
		this.logger.error(this.format(message, optionalParams ? (optionalParams.length > 0 ? optionalParams[1] : '') : ''));
	}

	fatal(message: any, ...optionalParams: any[]) {
		this.logger.error(this.format(message, optionalParams ? (optionalParams.length > 0 ? optionalParams[0] : '') : ''));
	}

	// ----------------------------------------------------------------------

	format(message: any, name: string): string {
		const width = 20;
		const prefix = name ? name.substring(0, width).padEnd(width) : ''.padEnd(width);
		return `${prefix} | ${message}`;
	}

}
