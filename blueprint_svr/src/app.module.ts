import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ServeStaticModule } from '@nestjs/serve-static';
import { ScheduleModule } from '@nestjs/schedule';
import { MulterModule } from '@nestjs/platform-express';
import { MailerModule } from '@nestjs-modules/mailer';
import { EjsAdapter } from '@nestjs-modules/mailer/dist/adapters/ejs.adapter';
import { TypeOrmModule, TypeOrmModuleOptions } from '@nestjs/typeorm';
import { join } from 'path';
import { TasksModule } from './tasks/tasks.module';
/*
import { SharedModule } from './shared/shared.module';
import { TestSharedModule } from './test_shared/test_shared.module';
import { PrecinctModule } from './precinct/precinct.module';
import { TestPrecinctModule } from './test_precinct/test_precinct.module';
import { MemberModule } from './member/member.module';
import { TestMemberModule } from './test_member/test_member.module';
import { CaretakerModule } from './caretaker/caretaker.module';
import { TestCaretakerModule } from './test_caretaker/test_caretaker.module';
import { SetupModule } from './setup/setup.module';
*/
import { AppService } from './app.service';
import { AppController } from './app.controller';


const env = <any>process.env;


@Module({
	imports: [
		// config module
		ConfigModule.forRoot({
			envFilePath: [
				'.env.override',
				'.env',
			],
		}),

		// serve static module
		ServeStaticModule.forRoot({
			rootPath: join(__dirname, '..', env.UPLOAD_DIR),
			serveRoot: `/${process.env.UPLOAD_DIR}`,
		}),

		// schedule task module
		ScheduleModule.forRoot(),

		// multer module
		MulterModule.registerAsync({
			useFactory: () => ({
				dest: join(__dirname, '..', env.UPLOAD_DIR),
			}),
		}),

		// mailer module
		MailerModule.forRootAsync({
			useFactory: () => ({
				transport: {
					host: env.MAIL_HOST,
					port: +env.MAIL_PORT,
					auth: {
						user: env.MAIL_USER,
						pass: env.MAIL_PASS,
					}
				},
				defaults: {
					from: env.MAIL_ADMIN,
				},
				template: {
					dir: `${__dirname}/mail_templates`,
					adapter: new EjsAdapter(),
					options: {
						strict: false,
					},
				},
			}),
		}),

		// database typeorm module
		TypeOrmModule.forRootAsync({
			useFactory: () => {
				const type = <'sqlite' | 'mysql'>env.DB_USE;
				let db: string;
				let host: string;
				let port: number;
				let username: string;
				let password: string;

				if (env.TESTDB_USE) {
					if (type === 'sqlite')
						db = join(__dirname, '..', env.STORAGE_DIR, env.TESTDB_FILE);
					else
						db = env.TESTDB_NAME;
					host = env.TESTDB_HOST;
					port = +env.TESTDB_PORT;
					username = env.TESTDB_USER;
					password = env.TESTDB_PASS;
				}
				else {
					if (type === 'sqlite')
						db = join(__dirname, '..', env.STORAGE_DIR, env.DB_FILE);
					else
						db = env.DB_NAME;
					host = env.DB_HOST;
					port = +env.DB_PORT;
					username = env.DB_USER;
					password = env.DB_PASS;
				}

				const options: TypeOrmModuleOptions = {
					type,
					host,
					port,
					username,
					password,
					database: db,
					entities: [
						'dist/**/*.entity.{ts,js}',
					],
					synchronize: true,  // <-- change this to 'false' when release
					retryAttempts: 0,
					multipleStatements: true,
					//logging: true,
				};
				return options;
			},
		}),

		// tasks schedule
		TasksModule,

		/*
		// shared module and testing
		SharedModule,
		TestSharedModule,

		// precinct module and testing
		PrecinctModule,
		TestPrecinctModule,

		// member module and testing
		MemberModule,
		TestMemberModule,

		// caretaker module and testing
		CaretakerModule,
		TestCaretakerModule,

		// setup module
		SetupModule,
		*/
	],
	providers: [
		AppService,
	],
	controllers: [
		AppController,
	],
})
export class AppModule { }
